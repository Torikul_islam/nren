
@extends('Admin.layouts.app')


@section('content')
<!-- main content start -->
<div class="main-content">

    <!-- content -->
    <div class="container-fluid content-top-gap">

        <!-- breadcrumbs -->
        <nav aria-label="breadcrumb" class="mb-4">
            <ol class="breadcrumb my-breadcrumb">
                <li class="breadcrumb-item"><a href="">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Maturity Table</li>
            </ol>
        </nav>
        <!-- //breadcrumbs -->
        <!-- forms -->
        <section class="forms">
            <!-- forms 1 -->
            <div class="card card_border py-2 mb-4">
               <!--  <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="cards__heading col-sm-8">
                        <h3>Bangladesh Research and Education Network (BdREN) <span></span></h3>
                    </div>
                    <div class="col-sm-1"></div>
                </div> -->

                <div class="cards__heading">
                     <h3 style="text-align:center;">NREN Maturity Model Calculator</h3>
                    <h3 style="text-align:center;">Pending Users</h3>
                </div>

                <div class="card-body">

                    <div style="overflow-x:auto;">
                        <table id="myFileTable" class="table table-striped table-bordered" width="100%" style="text-align: center;">
                            <thead class="text-capitalize " style="text-align: center;" >
                                <tr>
                                    <th >Organization Name</th>
                                    <th >Email</th>
                                    <th >Verifired</th>
                                </tr>
                    
                            </thead>
                            <tbody>
                                @foreach ($data as $value)
                                    <tr>
                                        <td>{{$value->name}}</td>
                                        <td>{{$value->email}}</td>
                                        <td>
                                            <form method="post" action="{{ url('/admin/toggleVerify') }}">
                                            @csrf
                                            <input type="hidden" name="user_id" value="<?php echo $value->id ?>">
                                            <label class="switch">
                                              <input type="checkbox" onclick="this.form.submit()">
                                              <span class="slider round"></span>
                                            </label>
                                          </form>
                                        </td>

                                    </tr>
                                @endforeach
                            </tbody>
                        
                        </table>
                    </div>
                </div>
            </div>
            <!-- //forms 1 -->
     

    </div>
    <!-- //content -->

</div>
<!-- main content end-->
@endsection


