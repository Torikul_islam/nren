  <!-- sidebar menu start -->
  <div class="sidebar-menu sticky-sidebar-menu">

    <!-- logo start -->
<!--     <div class="logo">
      <h1><a href="index.html">Collective</a></h1>
    </div> -->

  <!-- if logo is image enable this -->
  
    <div class="logo">
      <a href="{{ url('/admin/dashboard') }}">
        <img src="{{ URL::asset('assets/images/L.png') }}" alt="Your logo" title="BDREN" class="img-fluid" style="height:60px;" />
      </a>
    </div>


    

    <div class="logo-icon text-center">
      <a href="{{ url('/admin/dashboard') }}" title="BDREN"><img src="{{ URL::asset('assets/images/L.png') }}" alt="logo-icon"> </a>
    </div>
    <!-- //logo end -->

    <div class="sidebar-menu-inner">

      <!-- sidebar nav start -->
      <ul class="nav nav-pills nav-stacked custom-nav">
       
      
        <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-file-text"></i> <span>Home</span></a></li>

       <li><a href="{{route('users.index')}}"><i class="fas fa-users"></i> <span>Pending User</span></a></li>
       <li><a href="{{route('download.index')}}"><i class="fas fa-users"></i> <span>Response Download</span></a></li>

      </ul>
      <!-- //sidebar nav end -->
      <!-- toggle button start -->
      <a class="toggle-btn">
        <i class="fa fa-angle-double-left menu-collapsed__left"><span>Collapse Sidebar</span></i>
        <i class="fa fa-angle-double-right menu-collapsed__right"></i>
      </a>
      <!-- //toggle button end -->
    </div>
  </div>
  <!-- //sidebar menu end -->