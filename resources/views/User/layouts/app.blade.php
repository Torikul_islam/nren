
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>{{config('app.name')}} | {{Auth::user()->name}}</title>

  <!-- Template CSS -->


  <link rel="stylesheet" href="{{ URL::asset('assets/css/style-starter.css') }}">


  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

   

  <style>
  .sidebar-menu-collapsed .logo-icon {
  height: 60px;
  margin-top: 0;
  display: block !important;
  background: var(--white);
  line-height: 60px; }

  .logo {
  background: var(--white);
  text-align: center;
  height: 60px; }

  
</style>

  <!-- google fonts -->
  <link href="//fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900&display=swap" rel="stylesheet">


</head>

<body class="sidebar-menu-collapsed">
  <div class="se-pre-con"></div>
  <section>

    <!-- sidebar menu area start -->
        @include('User.layouts.sidebar')
    <!-- sidebar menu area end -->

    <!-- header-starts -->
        @include('User.layouts.header')
    <!-- //header-ends -->

   @yield('content')
</section>
  <!--footer section start-->
<footer class="dashboard">
  <p>All Rights Reserved | Designed by <a href="https://www.bdren.net.bd/" target="_blank"
      class="text-primary">BdREN</a> & <a href="https://www.ac.lk/" target="_blank"
      class="text-primary">LEARN</a></p>
</footer>
<!--footer section end-->
<!-- move top -->
<button onclick="topFunction()" id="movetop" class="bg-primary" title="Go to top">
  <span class="fa fa-angle-up"></span>
</button>
<script>
  // When the user scrolls down 20px from the top of the document, show the button
  window.onscroll = function () {
    scrollFunction()
  };

  function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      document.getElementById("movetop").style.display = "block";
    } else {
      document.getElementById("movetop").style.display = "none";
    }
  }

  // When the user clicks on the button, scroll to the top of the document
  function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }
</script>
<!-- /move top -->



<script src="{{ URL::asset('assets/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/jquery-1.10.2.min.js') }}"></script>

<!-- chart js -->
<!-- <script src="{{ URL::asset('assets/js/Chart.min.js') }}"></script> -->
<!-- <script src="{{ URL::asset('assets/js/utils.js') }}"></script> -->
<!-- //chart js -->

<!-- Different scripts of charts.  Ex.Barchart, Linechart -->
<script src="{{ URL::asset('assets/js/bar.js') }}"></script>
<script src="{{ URL::asset('assets/js/linechart.js') }}"></script>
<!-- //Different scripts of charts.  Ex.Barchart, Linechart -->


<script src="{{ URL::asset('assets/js/jquery.nicescroll.js') }}"></script>
<script src="{{ URL::asset('assets/js/scripts.js') }}"></script>

<!-- close script -->
<script>
  var closebtns = document.getElementsByClassName("close-grid");
  var i;

  for (i = 0; i < closebtns.length; i++) {
    closebtns[i].addEventListener("click", function () {
      this.parentElement.style.display = 'none';
    });
  }
</script>
<!-- //close script -->

<!-- disable body scroll when navbar is in active -->
<script>
  $(function () {
    $('.sidebar-menu-collapsed').click(function () {
      $('body').toggleClass('noscroll');
    })
  });
</script>
<!-- disable body scroll when navbar is in active -->

 <!-- loading-gif Js -->
 <script src="assets/js/modernizr.js"></script>
 <script>
     $(window).load(function () {
         // Animate loader off screen
         $(".se-pre-con").fadeOut("slow");;
     });
 </script>
 <!--// loading-gif Js -->

<!-- Bootstrap Core JavaScript -->
<script src="{{ URL::asset('assets/js/bootstrap.min.js') }}"></script>

<script type="text/javascript">

      $(document).ready(function() {

        $("#YES").click(function(){
            $('#altUserEmail').show();
          });
        $("#NO").click(function(){
            $('#altUserEmail').hide();
            $('#email').prop("required", false);
          });
    });

      $("#RENOthers").change(function(){

            var selValue = $(this).val();
            $(".link").attr('href','/edit/'+selValue);
        
    });

      $('#startWithNewdata').click(function(){
        $('#coverage_total_university,#coverage_university').prop('disabled', false);

        $('input[type=radio][name=Lifetime]').prop('disabled', false);

        $('#connected_institute_university,#connected_institute_research,#connected_institute_govt,#connected_institute_school,#connected_institute_other').prop('disabled', false);
       

         $('input[type=radio][name=Maximum_BW_U]').prop('disabled', false);
         $('input[type=radio][name=Maximum_BW_RI]').prop('disabled', false);

         $('#Commodity,#Res_Edu').prop('disabled', false);

         
        $('input[type=radio][name=RandS]').prop('disabled', false);
        $('input[type=radio][name=Trans_Network]').prop('disabled', false);
        $('input[type=radio][name=CandV]').prop('disabled', false);
        $('input[type=radio][name=VideoColla]').prop('disabled', false);
        $('input[type=radio][name=BackboneFibre]').prop('disabled', false);
        $('input[type=radio][name=CivilPI]').prop('disabled', false);
        $('input[type=radio][name=LMF]').prop('disabled', false);

         $('input[type=radio][name=Organogram]').prop('disabled', false);
         $('input[type=radio][name=Policy_Makers]').prop('disabled', false);
         $('input[type=radio][name=Recruitment_Regulations]').prop('disabled', false);
         $('input[type=radio][name=Promotion_Policy]').prop('disabled', false);
         $('input[type=radio][name=Annual_Appraisal]').prop('disabled', false);
         $('input[type=radio][name=Gratuity]').prop('disabled', false);
         $('input[type=radio][name=Provident_Fund]').prop('disabled', false);
         $('input[type=radio][name=Welfare_Fund]').prop('disabled', false);
         $('input[type=radio][name=Group_Insurance]').prop('disabled', false);
         $('input[type=radio][name=Pension_Benefits]').prop('disabled', false);
        
         $('#Permanent,#Contractual,#Outsourced,#Turnover_Rate,#Awareness_programs,#Awareness_Programs_conducted,#human_resouces_technical,#human_resouces_Nontechnical,#human_expert_admin,#human_intermediate_admin,#human_beginner_admin,#human_expert_tech,#human_intermediate_tech,#human_beginner_tech').prop('disabled', false);

         $('input[type=radio][name=Filled_Position]').prop('disabled', false);
         $('input[type=radio][name=PV_effort_REN]').prop('disabled', false);
         $('input[type=radio][name=budget_promotional]').prop('disabled', false);
         $('input[type=radio][name=Development_personnel]').prop('disabled', false);

         $('input[type=radio][name=collaboration_NREN]').prop('disabled', false);
         $('input[type=radio][name=collaboration_UR]').prop('disabled', false);
         $('input[type=radio][name=collaboration_vendor]').prop('disabled', false);
         $('input[type=radio][name=collaboration_policy_RM]').prop('disabled', false);
         $('input[type=radio][name=collaboration_policy_HEC]').prop('disabled', false);
         $('input[type=radio][name=SystemOperationRandS]').prop('disabled', false);
         $('input[type=radio][name=SystemOperationTN]').prop('disabled', false);
         $('input[type=radio][name=SystemOperationCV]').prop('disabled', false);
         $('input[type=radio][name=SystemOperationCollaboration]').prop('disabled', false);
         $('input[type=radio][name=SystemOperationAS]').prop('disabled', false);
         $('input[type=radio][name=SystemOperationOther]').prop('disabled', false);
         $('input[type=radio][name=financial_type]').prop('disabled', false);
         $('input[type=radio][name=FS_Revenue_Coverage]').prop('disabled', false);
         $('input[type=radio][name=FS_compliance_lts]').prop('disabled', false);
         $('input[type=radio][name=financial_constraint]').prop('disabled', false);
         $('input[type=radio][name=LTC]').prop('disabled', false);
         $('input[type=radio][name=Highly_Struggling]').prop('disabled', false);
         $('input[type=radio][name=alterSendScoreUser]').prop('disabled', false);

        $(function() {
            enable_cb();
            $("#startWithNewdata").click(enable_cb);
        });

        function enable_cb() {
            $("input.policies,input.network_service_SO,input.identity_service_SO,input.security_service_SO,input.multimedia_service_SO,input.collaboration_service_SO,input.storage_service_SO,input.Critical_Deviation,input.professional_service_SO").prop('disabled', false);
        }
});


</script>

</body>

</html>
  