 <!-- header-starts -->
  <div class="header sticky-header">

    <!-- notification menu start -->
    <div class="menu-right">
      <div class="navbar user-panel-top">
       <!--  <div class="search-box">
          <form action="#search-results.html" method="get">
            <input class="search-input" placeholder="Search Here..." type="search" id="search">
            <button class="search-submit" value=""><span class="fa fa-search"></span></button>
          </form>
        </div> -->
        <div class="user-dropdown-details d-flex">
        <!--   <div class="profile_details_left">
            <ul class="nofitications-dropdown">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i
                    class="fa fa-bell-o"></i><span class="badge blue">3</span></a>
                <ul class="dropdown-menu">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i
                    class="fa fa-comment-o"></i><span class="badge blue">4</span></a>
                <ul class="dropdown-menu">
                    <li>
                      <div class="notification_header">
                        <h3>You have 4 new messages</h3>
                      </div>
                    </li>

                    <li><a href="#" class="grid">
                        <div class="user_img"><img src="assets/images/avatar1.jpg" alt=""></div>
                        <div class="notification_desc">
                          <p>Johnson purchased template</p>
                          <span>Just Now</span>
                        </div>
                      </a></li>
                    <li class="odd"><a href="#" class="grid">
                        <div class="user_img"><img src="assets/images/avatar2.jpg" alt=""></div>
                        <div class="notification_desc">
                          <p>New customer registered </p>
                          <span>1 hour ago</span>
                        </div>
                      </a></li>
                    <li><a href="#" class="grid">
                        <div class="user_img"><img src="assets/images/avatar3.jpg" alt=""></div>
                        <div class="notification_desc">
                          <p>Lorem ipsum dolor sit amet </p>
                          <span>2 hours ago</span>
                        </div>
                      </a></li>
                    <li><a href="#" class="grid">
                        <div class="user_img"><img src="assets/images/avatar1.jpg" alt=""></div>
                        <div class="notification_desc">
                          <p>Johnson purchased template</p>
                          <span>Just Now</span>
                        </div>
                      </a></li>
                    <li>
                      <div class="notification_bottom">
                        <a href="#all" class="bg-primary">See all messages</a>
                      </div>
                    </li>
                  </ul>
                </li>
            </ul>
          </div> -->
          <div class="profile_details">
            <ul>
              <li class="dropdown profile_details_drop">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="dropdownMenu3" aria-haspopup="true"
                  aria-expanded="false">
                  <div class="profile_img">
                    <img src="{{ URL::asset('assets/images/profile.png') }}" class="rounded-circle" alt="" />
                    <div class="user-active">
                      <span></span>
                    </div>
                  </div>
                </a>
                <ul class="dropdown-menu drp-mnu" aria-labelledby="dropdownMenu3">
                  <li class="user-info">
                    <h5 class="user-name">{{Auth::user()->name}}</h5>
                    <span class="status ml-2">Available</span>
                  </li>

                  <li class="logout"> <a href="{{ url('/change-password') }}">
                    <i class="fa fa-shield"></i> Change Password</a> </li>
                  
                  <li class="logout"> <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-power-off"></i> Logout</a> </li>
                  <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!--notification menu end -->
  </div>
  <!-- //header-ends -->