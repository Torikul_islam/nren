  <!-- sidebar menu start -->
  <div class="sidebar-menu sticky-sidebar-menu">

    <!-- logo start -->
<!--     <div class="logo">
      <h1><a href="index.html">Collective</a></h1>
    </div> -->

  <!-- if logo is image enable this -->
  
    <div class="logo">
      <a href="{{ route('form') }}">
        <img src="{{ URL::asset('assets/images/L.png') }}" alt="Your logo" title="BDREN" class="img-fluid" style="height:60px;" />
      </a>
    </div>


    

    <div class="logo-icon text-center">
      <a href="{{ route('form') }}" title="BDREN"><img src="{{ URL::asset('assets/images/L.png') }}" alt="logo-icon"> </a>
    </div>
    <!-- //logo end -->

    <div class="sidebar-menu-inner">

      <!-- sidebar nav start -->
      <ul class="nav nav-pills nav-stacked custom-nav">
       
      
        <li><a href="{{ route('form') }}"><i class="fa fa-file-text"></i> <span> Question Forms</span></a></li>

       <li><a href="{{ route('maturity') }}"><i class="fas fa-table"></i> <span> Maturity Table</span></a></li>

      </ul>
      <!-- //sidebar nav end -->
      <!-- toggle button start -->
      <a class="toggle-btn">
        <i class="fa fa-angle-double-left menu-collapsed__left"><span>Collapse Sidebar</span></i>
        <i class="fa fa-angle-double-right menu-collapsed__right"></i>
      </a>
      <!-- //toggle button end -->
    </div>
  </div>
  <!-- //sidebar menu end -->