
@extends('layouts.app')

@section('content')
  <!-- main content start -->
<div class="main-content">

  <!-- content -->
  <div class="container-fluid content-top-gap">

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb my-breadcrumb">
        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
      </ol>
    </nav>
    <div class="welcome-msg pt-3 pb-4">
      <h1>Hi Welcome To BdREN</h1>
    </div>

    <!-- statistics data -->
    <div class="statistics">
      <div class="row">
        <div class="col-xl-6 pr-xl-2">
          <div class="row">
            <div class="col-sm-6 pr-sm-2 statistics-grid">
              <div class="card card_border border-primary-top p-4">
                <a href="{{ route('form') }}"><i class="fab fa-wpforms" style="font-size:40px;color:blue;" ></i></a>
                <h3 class="text-primary number"><a href="{{ route('form') }}">Question Form</a></h3>
              </div>
            </div>
            <div class="col-sm-6 pl-sm-2 statistics-grid">
              <div class="card card_border border-primary-top p-4">
                <a href="{{ route('maturity_table') }}"><i class="fas fa-table" style="font-size:40px;color:blue;" ></i></a>
                <h3 class="text-primary number"><a href="{{ route('maturity_table') }}">Maturity Table</a></h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- //statistics data -->

   

  </div>
  <!-- //content -->
</div>
<!-- main content end-->
@endsection