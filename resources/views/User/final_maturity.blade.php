
@extends('User.layouts.app')

@section('content')
  <!-- main content start -->
<div class="main-content">

  <!-- content -->
  <div class="container-fluid content-top-gap">

    
    @foreach ($matu_data as $data)
    <div class="welcome-msg pt-3 pb-4">
      <h1 style="text-align:center;">NREN Maturity Calculator Outcome</h1>
    </div>

  
   <!-- statistics data -->
    <div class="statistics">
      <div class="row">
        <div class="col-xl-12 pr-xl-2">
          <div class="row">
            <div class="col-sm-4 pr-sm-2 statistics-grid">
            </div>
            <div class="col-sm-4 pr-sm-2 statistics-grid">
              <div class="card card_border border-primary-top p-4">
                <i class="fas fa-bookmark" style="font-size:35px;color:blue;"></i>
                <p class="text-primary stat-text"style="font-size: 25px;">Maturity Score of {{$data->getRENDATA->name}}</p>
                <h3 class="text-primary number">{{$data->maturity_level}} /100</h3>
                <p class="text-primary stat-text" style="font-size: 17px;">Placement in {{$data->placement_tier}}</p>
              </div>
            </div>
            <div class="col-sm-4 pr-sm-2 statistics-grid">
            </div>

          </div>
        </div>
      </div>
    </div>
    <!-- //statistics data -->

    <!-- statistics data -->
    <div class="statistics">
      <div class="row">
        <div class="col-xl-6 pr-xl-2">
          <div class="row">
            <div class="col-sm-6 pr-sm-2 statistics-grid">
              <div class="card card_border border-primary-top p-4">
                <i class="fas fa-university" style="font-size:25px;color:blue;"></i>
                <h3 class="text-secondary number">{{$data->getCoverageScore->score}} /10</h3>
                <p class="text-primary stat-text"style="font-size: 17px;">Coverage</p> 
              </div>
            </div>
            <div class="col-sm-6 pl-sm-2 statistics-grid">
              <div class="card card_border border-primary-top p-4">
                <i class="fas fa-hourglass-start" style="font-size:25px;color:blue;"></i>
                <h3 class="text-secondary number">{{$data->getLifetimevalue->lifetimeValue}}/10</h3>
                <p class="text-primary stat-text"style="font-size: 17px;">Lifetime</p> 
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-6 pl-xl-2">
          <div class="row">
            <div class="col-sm-6 pr-sm-2 statistics-grid">
              <div class="card card_border border-primary-top p-4">
                <i class="fas fa-university" style="font-size:25px;color:blue;"></i>
                <h3 class="text-secondary number">{{$data->getConnectedInstitutionlue->connected_institute_typescore}}/10</h3>
               <p class="text-primary stat-text"style="font-size: 17px;">Connected Institution Type</p>
              </div>
            </div>
            <div class="col-sm-6 pl-sm-2 statistics-grid">
              <div class="card card_border border-primary-top p-4">
                <i class="fas fa-university" style="font-size:25px;color:blue;"></i>
                <h3 class="text-secondary number">{{$data->getConnectedInstitutionlue->connected_institute_coverage_score}}/10</h3>
                <p class="text-primary stat-text"style="font-size: 17px;">Connected Institution Coverage</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- //statistics data -->
    <!-- statistics data -->
    <div class="statistics">
      <div class="row">
        <div class="col-xl-6 pr-xl-2">
          <div class="row">
            <div class="col-sm-6 pr-sm-2 statistics-grid">
              <div class="card card_border border-primary-top p-4">
                <i class="fas fa-wifi" style="font-size:25px;color:blue;"></i>
                <h3 class="text-secondary number">{{$data->getMaximumBW->Maximum_BW_U_score}}/10</h3>
                <p class="text-primary stat-text"style="font-size: 17px;">Maximum BW University</p>
              </div>
            </div>
            <div class="col-sm-6 pl-sm-2 statistics-grid">
              <div class="card card_border border-primary-top p-4">
                <i class="fas fa-wifi" style="font-size:25px;color:blue;"></i>
                <h3 class="text-secondary number">{{$data->getMaximumBW->Maximum_BW_RI_score}}/10</h3>
                <p class="text-primary stat-text"style="font-size: 17px;">Maximum BW Research Institute</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-6 pl-xl-2">
          <div class="row">
            <div class="col-sm-6 pr-sm-2 statistics-grid">
              <div class="card card_border border-primary-top p-4">
                <i class="fas fa-industry" style="font-size:25px;color:blue;"></i>
                <h3 class="text-secondary number">{{$data->getAggregateBW->Commodity_score}}/10</h3>
                <p class="text-primary stat-text"style="font-size: 17px;">Aggregate Up BW Commodity</p>
              </div>
            </div>
            <div class="col-sm-6 pl-sm-2 statistics-grid">
              <div class="card card_border border-primary-top p-4">
                <i class="fas fa-industry" style="font-size:25px;color:blue;"></i>
                <h3 class="text-secondary number">{{$data->getAggregateBW->Res_Edu_score}}/10</h3>
                <p class="text-primary stat-text"style="font-size: 17px;">Aggregate Up BW R&E</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- //statistics data -->
    <!-- statistics data -->
    <div class="statistics">
      <div class="row">
        <div class="col-xl-6 pr-xl-2">
          <div class="row">
            <div class="col-sm-6 pr-sm-2 statistics-grid">
              <div class="card card_border border-primary-top p-4">
                <i class="fas fa-laptop-code" style="font-size:25px;color:blue;"></i>
                <h3 class="text-secondary number">{{$data->getOwwnerShipData->ownership_score}}/10</h3>
                <p class="text-primary stat-text"style="font-size: 17px;">Device of Ownership</p> 
              </div>
            </div>
            <div class="col-sm-6 pl-sm-2 statistics-grid">
              <div class="card card_border border-primary-top p-4">
                <i class="fas fa-poll-h" style="font-size:25px;color:blue;"></i>
                <h3 class="text-secondary number">{{$data->getServiceOfferData->offered_service_final_score}}/10</h3>
                <p class="text-primary stat-text"style="font-size: 17px;">Service Offer</p> 
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-6 pl-xl-2">
          <div class="row">
            <div class="col-sm-6 pr-sm-2 statistics-grid">
              <div class="card card_border border-primary-top p-4">
                <i class="fas fa-donate" style="font-size:25px;color:blue;"></i>
                <h3 class="text-secondary number">{{$data->getFSData->financitial_stablity_grading}}/10</h3>
                <p class="text-primary stat-text"style="font-size: 17px;">Financial Stablity</p> 
              </div>
            </div>
            <div class="col-sm-6 pl-sm-2 statistics-grid">
              <div class="card card_border border-primary-top p-4">
                <i class="fas fa-user-alt" style="font-size:25px;color:blue;"></i>
                <h3 class="text-secondary number">{{$data->getHRData->human_resouce_technical_final_score}}/10</h3>
                <p class="text-primary stat-text"style="font-size: 17px;">Human Resouces Tech</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- //statistics data -->
    <!-- statistics data -->
    <div class="statistics">
      <div class="row">
        <div class="col-xl-6 pr-xl-2">
          <div class="row">
            <div class="col-sm-6 pr-sm-2 statistics-grid">
              <div class="card card_border border-primary-top p-4">
                <i class="fas fa-user-alt" style="font-size:25px;color:blue;"></i>
                <h3 class="text-secondary number">{{$data->getHRData->human_resouce_nontechnical_final_score}}/10</h3>
                <p class="text-primary stat-text"style="font-size: 17px;">Human Resouces Admin</p>
              </div>
            </div>
            <div class="col-sm-6 pl-sm-2 statistics-grid">
              <div class="card card_border border-primary-top p-4">
                <i class="fas fa-sitemap" style="font-size:25px;color:blue;"></i>
                <h3 class="text-secondary number">{{$data->getOMGData->omg_aggregate_score}}/10</h3>
                <p class="text-primary stat-text"style="font-size: 17px;">Org, Management & Governance</p> 
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-6 pl-xl-2">
          <div class="row">
            <div class="col-sm-6 pr-sm-2 statistics-grid">
              <div class="card card_border border-primary-top p-4">
                <i class="fas fa-sort-amount-up-alt" style="font-size:25px;color:blue;"></i>
                <h3 class="text-secondary number">{{$data->getPVData->PV_aggregate_score}}/10</h3>
                <p class="text-primary stat-text"style="font-size: 17px;">Promotion & Visibility</p> 
              </div>
            </div>
            <div class="col-sm-6 pl-sm-2 statistics-grid">
              <div class="card card_border border-primary-top p-4">
                <i class="fas fa-people-carry" style="font-size:25px;color:blue;"></i>
                <h3 class="text-secondary number">{{$data->getcollaborationData->aggregate_collaborate_score}}/10</h3>
               <p class="text-primary stat-text"style="font-size: 17px;">Collaboration  </p> 
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- //statistics data -->
    <!-- statistics data -->
    <div class="statistics">
      <div class="row">
        <div class="col-xl-6 pr-xl-2">
          <div class="row">
            <div class="col-sm-6 pr-sm-2 statistics-grid">
              <div class="card card_border border-primary-top p-4">
                <i class="fas fa-user-cog" style="font-size:25px;color:blue;"></i>
                <h3 class="text-secondary number">{{$data->getHEData->aggreagte_tech_score}}/10</h3>
                <p class="text-primary stat-text"style="font-size: 17px;">Human Expert Tech</p>
              </div>
            </div>
            <div class="col-sm-6 pl-sm-2 statistics-grid">
              <div class="card card_border border-primary-top p-4">
                <i class="fas fa-user-shield" style="font-size:25px;color:blue;"></i>
                <h3 class="text-secondary number">{{$data->getHEData->aggreagte_admin_score}}/10</h3>
                <p class="text-primary stat-text" style="font-size: 17px;">Human Expert Admin</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- //statistics data -->
 @endforeach
  </div>
  <!-- //content -->
</div>
<!-- main content end-->
@endsection