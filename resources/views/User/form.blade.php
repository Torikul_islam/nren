
@extends('User.layouts.app')

@section('content')
<!-- main content start -->
<div class="main-content">

    <!-- content -->
    <div class="container-fluid content-top-gap">

        <!-- breadcrumbs -->
        <nav aria-label="breadcrumb" class="mb-4">
            <ol class="breadcrumb my-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('form') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Forms</li>
            </ol>
        </nav>
        <!-- //breadcrumbs -->
        <!-- forms -->
        <section class="forms">
            <!-- forms 1 -->
            <div class="card card_border py-2 mb-4">
             <!--    <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="cards__heading col-sm-8">
                        <h3>Bangladesh Research and Education Network (BdREN) <span></span></h3>
                        <h3 style="text-align:center;"> NNA Maturity Model Calculator<span></span></h3>
                    </div>
                    <div class="col-sm-1"></div>
                </div> -->

                <div class="card-body">
                    <form method="POST" action="{{route('coverage.store')}}" enctype="multipart/form-data">
                         @csrf


                        <div class="cards__heading">
                            <h3 style="text-align:center; font-size: 40px;">NREN Maturity Calculator</h3>

                            <div class="text-center">
                              <a href="https://www.bdren.net.bd/" title="BDREN"><img src="{{ URL::asset('assets/images/L.png') }}" alt="logo-icon" style="height:80px;"> </a>
                              <a href="https://www.ac.lk/" title="LEARN"><img src="{{ URL::asset('assets/images/download.png') }}" alt="logo-icon" style="height:80px;"> </a>

                            </div>

                        </div>

                       
                             @if (session('success'))
                                <div class="alert alert-success"><strong>{{ session('success') }}</strong></div>
                             @endif

                             @if (session('error'))
                                <div class="alert alert-danger"><strong>{{ session('error') }}</strong></div>
                            @endif

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">1. Which Organization / NREN are you from ?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Organization:</label>
                                        <select class="form-control form-control-sm" name="ren_type_id" id="RENOthers">
                                            
                                            <option value="{{ Auth::user()->id}}">{{ Auth::user()->name }}</option>
                                           
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <div class="row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                <a class="btn btn-secondary link" href="{{route('mvalues.index')}}" >Load Previous Data<i class="fas fa-cloud-upload-alt" style="font-size:30px;color:white;" title="Upload Previous Data"></i></a>

                                <a class="btn btn-secondary" id="startWithNewdata">Start With New Data<i class="fas fa-fast-forward" style="font-size:30px;color:white;" title="Start With New Data"></i></a>
                            </div>
                            
                        </div>

                        <div class="cards__heading">
                            <h3>Coverage<span></span></h3>
                        </div>
                        <div class="form-group row">

                            <label class="col-sm-4 col-form-label input__label"> 2. How many universities are there in Total in your country ?</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control input-style" name="coverage_total_university" id="coverage_total_university"
                                    placeholder="Total Universities" disabled="disabled">
                                <small class="form-text text-muted">* Please Provide Numeric Value</small>
                            </div>
                        </div>
                        <div class="form-group row">

                            <label class="col-sm-4 col-form-label input__label"> 3. How many universities Were Connected To Your Network ?</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control input-style" name="coverage_university" id="coverage_university"
                                    placeholder="Connected Universities" disabled="disabled">
                                <small class="form-text text-muted">* Please Provide Numeric Value</small>
                            </div>
                        </div>


                        <div class="cards__heading">
                            <h3> Lifetime<span></span></h3>
                        </div>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">4. How long have you been in Business ?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                    
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Lifetime"
                                            value="1-3 Years" disabled="disabled" id="Lifetime1">
                                        <label class="form-check-label" >
                                            1-3 Years
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Lifetime"
                                            value="4-5 Years" disabled="disabled" id="Lifetime2">
                                        <label class="form-check-label">
                                            4-5 Years
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Lifetime" 
                                            value="6-10 Years" disabled="disabled" id="Lifetime3">
                                        <label class="form-check-label">
                                            6-10 Years
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Lifetime" 
                                            value="> 10 Years" disabled="disabled" id="Lifetime4">
                                        <label class="form-check-label">
                                            > 10 Years
                                        </label>
                                    </div>        
                                </div>
                            </div>
                        </fieldset>

                        <div class="cards__heading">
                            <h3> Connected Institutions<span></span></h3>
                        </div>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">5. How many Institutions/members were connected to your network??</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="connected_institute_university" class="form-control input-style"
                                        placeholder="Universities" disabled="disabled" id="connected_institute_university">
                                    <small  class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Research Institutes: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="connected_institute_research" class="form-control input-style"
                                        placeholder="Research Institutes" disabled="disabled" id="connected_institute_research">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Government Organizations: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="connected_institute_govt" class="form-control input-style"
                                        placeholder="Government Organizations" disabled="disabled" id="connected_institute_govt">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Schools and Colleges: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="connected_institute_school" class="form-control input-style" 
                                        placeholder="Schools and Colleges" disabled="disabled" id="connected_institute_school">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-4 col-form-label input__label">Others: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="connected_institute_other"class="form-control input-style"
                                        placeholder="Others" disabled="disabled" id="connected_institute_other">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <div class="cards__heading">
                            <h3> Maximum BW<span></span></h3>
                        </div>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-6">
                                    <div class="form-group ">
                                    <label class="input__label">6. The Maximum Bandwith that is deliverable among the universities ?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Maximum_BW_U" 
                                            value="None" disabled="disabled" id="Maximum_BW_U1">
                                        <label class="form-check-label" >
                                           None
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Maximum_BW_U"
                                            value="< 100 Mbps" disabled="disabled" id="Maximum_BW_U2">
                                        <label class="form-check-label" >
                                            < 100 Mbps
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Maximum_BW_U" 
                                            value="100 Mbps" disabled="disabled" id="Maximum_BW_U3">
                                        <label class="form-check-label">
                                            100 Mbps
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Maximum_BW_U" 
                                            value="1 Gbps" disabled="disabled" id="Maximum_BW_U4">
                                        <label class="form-check-label" >
                                             1 Gbps
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Maximum_BW_U" 
                                            value="10 Gbps" disabled="disabled" id="Maximum_BW_U5">
                                        <label class="form-check-label">
                                           10 Gbps
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Maximum_BW_U" 
                                            value="100 Gbps" disabled="disabled" id="Maximum_BW_U6">
                                        <label class="form-check-label" >
                                            100 Gbps
                                        </label>
                                    </div>       
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-6">
                                    <div class="form-group ">
                                    <label class="input__label">7.  The Maximum Bandwith that is deliverable among the Research Instituates ?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Maximum_BW_RI" 
                                            value="None" disabled="disabled" id="Maximum_BW_RI1">
                                        <label class="form-check-label" >
                                            None
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Maximum_BW_RI" 
                                            value="< 100 Mbps" disabled="disabled" id="Maximum_BW_RI2">
                                        <label class="form-check-label" >
                                            < 100 Mbps
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Maximum_BW_RI" 
                                            value="100 Mbps" disabled="disabled" id="Maximum_BW_RI3">
                                        <label class="form-check-label" >
                                           100 Mbps
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Maximum_BW_RI" 
                                            value="1 Gbps" disabled="disabled" id="Maximum_BW_RI4">
                                        <label class="form-check-label" >
                                            1 Gbps
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Maximum_BW_RI" 
                                            value="10 Gbps" disabled="disabled" id="Maximum_BW_RI5">
                                        <label class="form-check-label" >
                                            10 Gbps
                                        </label>
                                    </div>  
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Maximum_BW_RI" 
                                            value="100 Gbps" disabled="disabled" id="Maximum_BW_RI6">
                                        <label class="form-check-label" >
                                            100 Gbps
                                        </label>
                                    </div>      
                                </div>
                            </div>
                        </fieldset>

                        <div class="cards__heading">
                            <h3> Aggregate Upstream Bandwidth <span></span></h3>
                        </div>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">8. Your Total Upstream Traffic Capacity, Mbps</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Commodity : </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Commodity" class="form-control input-style"
                                        placeholder="Commodity" disabled="disabled" id="Commodity">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Research and Education: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Res_Edu" class="form-control input-style"
                                        placeholder="Research and Education" disabled="disabled" id="Res_Edu">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <div class="cards__heading">
                            <h3> Device Ownership <span></span></h3>
                        </div>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">9. What is the "Ownership Model" of the key components of your Network?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Routing and Switching: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="RandS"
                                            value="Fully Owned" disabled="disabled" id="RandS1">
                                        <label class="form-check-label" >
                                            Fully Owned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="RandS" 
                                            value="Long Term Leased" disabled="disabled" id="RandS2">
                                        <label class="form-check-label" >
                                            Long Term Leased
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="RandS" 
                                            value="Hybrid" disabled="disabled" id="RandS3">
                                        <label class="form-check-label" >
                                           Hybrid
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="RandS" 
                                            value="Rented" disabled="disabled" id="RandS4">
                                        <label class="form-check-label" >
                                           Rented
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="RandS" 
                                            value="Not Applicable" disabled="disabled" id="RandS5">
                                        <label class="form-check-label" >
                                           Not Applicable
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-4 col-form-label input__label"> Transmission Network: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Trans_Network" 
                                            value="Fully Owned" disabled="disabled" id="Trans_Network1">
                                        <label class="form-check-label" >
                                            Fully Owned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Trans_Network" 
                                            value="Long Term Leased" disabled="disabled" id="Trans_Network2">
                                        <label class="form-check-label" >
                                            Long Term Leased
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Trans_Network" 
                                            value="Hybrid" disabled="disabled" id="Trans_Network3">
                                        <label class="form-check-label" >
                                           Hybrid
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Trans_Network" 
                                            value="Rented" disabled="disabled" id="Trans_Network4">
                                        <label class="form-check-label" >
                                           Rented
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Trans_Network" 
                                            value="Not Applicable" disabled="disabled" id="Trans_Network5">
                                        <label class="form-check-label" >
                                           Not Applicable
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Compute and Virtualization: </label>
                                 <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="CandV" 
                                            value="Fully Owned" disabled="disabled" id="CandV1">
                                        <label class="form-check-label" >
                                            Fully Owned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="CandV" 
                                            value="Long Term Leased"disabled="disabled" id="CandV2">
                                        <label class="form-check-label" >
                                            Long Term Leased
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="CandV" 
                                            value="Hybrid"disabled="disabled" id="CandV3">
                                        <label class="form-check-label" >
                                           Hybrid
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="CandV" 
                                            value="Rented" disabled="disabled" id="CandV4">
                                        <label class="form-check-label" >
                                           Rented
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="CandV" 
                                            value="Not Applicable" disabled="disabled" id="CandV5">
                                        <label class="form-check-label" >
                                           Not Applicable
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Video Collaboration: </label>
                                 <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="VideoColla" 
                                            value="Fully Owned" disabled="disabled" id="VideoColla1">
                                        <label class="form-check-label" >
                                            Fully Owned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="VideoColla" 
                                            value="Long Term Leased"disabled="disabled" id="VideoColla2">
                                        <label class="form-check-label" >
                                            Long Term Leased
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="VideoColla" 
                                            value="Hybrid" disabled="disabled" id="VideoColla3">
                                        <label class="form-check-label" >
                                           Hybrid
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="VideoColla" 
                                            value="Rented"disabled="disabled" id="VideoColla4">
                                        <label class="form-check-label" >
                                           Rented
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="VideoColla" 
                                            value="Not Applicable"disabled="disabled" id="VideoColla5">
                                        <label class="form-check-label" >
                                           Not Applicable
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-4 col-form-label input__label"> Backbone Fibre: </label>
                                 <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="BackboneFibre" 
                                            value="Fully Owned" disabled="disabled" id="BackboneFibre1">
                                        <label class="form-check-label" >
                                            Fully Owned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="BackboneFibre" 
                                            value="Long Term Leased"disabled="disabled" id="BackboneFibre2">
                                        <label class="form-check-label" >
                                            Long Term Leased
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="BackboneFibre" 
                                            value="Hybrid" disabled="disabled" id="BackboneFibre3">
                                        <label class="form-check-label" >
                                           Hybrid
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="BackboneFibre" 
                                            value="Rented" disabled="disabled" id="BackboneFibre4">
                                        <label class="form-check-label" >
                                           Rented
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="BackboneFibre" 
                                            value="Not Applicable" disabled="disabled" id="BackboneFibre5">
                                        <label class="form-check-label" >
                                           Not Applicable
                                        </label>
                                    </div>
                                </div>
                            </div>
                             <div class="form-group row">
                                <label  class="col-sm-4 col-form-label input__label"> Last mile Fibre: </label>
                                 <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="LMF" 
                                            value="Fully Owned" disabled="disabled" id="LMF1">
                                        <label class="form-check-label" >
                                            Fully Owned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="LMF" i
                                            value="Long Term Leased" disabled="disabled" id="LMF2">
                                        <label class="form-check-label" >
                                            Long Term Leased
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="LMF" 
                                            value="Hybrid" disabled="disabled" id="LMF3">
                                        <label class="form-check-label" >
                                           Hybrid
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="LMF" 
                                            value="Rented"disabled="disabled" id="LMF4">
                                        <label class="form-check-label" >
                                           Rented
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="LMF" 
                                            value="Not Applicable"disabled="disabled" id="LMF5">
                                        <label class="form-check-label" >
                                           Not Applicable
                                        </label>
                                    </div>
                                </div>
                            </div>
                             <div class="form-group row">
                                <label  class="col-sm-4 col-form-label input__label"> Civil and Power Infrastructure at PoPs: </label>
                                 <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="CivilPI" 
                                            value="Fully Owned" disabled="disabled" id="CivilPI1">
                                        <label class="form-check-label" >
                                            Fully Owned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="CivilPI" 
                                            value="Long Term Leased"disabled="disabled" id="CivilPI2">
                                        <label class="form-check-label" >
                                            Long Term Leased
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="CivilPI" 
                                            value="Hybrid"disabled="disabled" id="CivilPI3">
                                        <label class="form-check-label" >
                                           Hybrid
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="CivilPI" 
                                            value="Rented"disabled="disabled" id="CivilPI4">
                                        <label class="form-check-label" >
                                           Rented
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="CivilPI" 
                                            value="Not Applicable"disabled="disabled" id="CivilPI5">
                                        <label class="form-check-label" >
                                           Not Applicable
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <div class="cards__heading">
                            <h3> Organization, Management and Governance (OMG): <span></span></h3>
                        </div>
                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">10. Please Select the policies that are available in your organization?</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Human Resource Policy" name="policies[]" type="checkbox" disabled="disabled" >
                                    <label class="form-check-label">
                                       Human Resource Policy
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Procurement Policy and Rules" name="policies[]"type="checkbox" disabled="disabled" >
                                    <label class="form-check-label" >
                                       Procurement Policy and Rules
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Service Level Agreements" name="policies[]"type="checkbox" disabled="disabled" >
                                    <label class="form-check-label" >
                                        Service Level Agreements
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Finance Policy" name="policies[]"type="checkbox" disabled="disabled" >
                                    <label class="form-check-label" >
                                        Finance Policy
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Membership Policy" name="policies[]"type="checkbox" disabled="disabled" >
                                    <label class="form-check-label" >
                                       Membership Policy
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Service Usage Policy" name="policies[]"type="checkbox" disabled="disabled" >
                                    <label class="form-check-label" >
                                       Service Usage Policy
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value=" Tariff Policy" name="policies[]"type="checkbox" disabled="disabled" >
                                    <label class="form-check-label" >
                                       Tariff Policy
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value=" Vehicle Policy" name="policies[]"type="checkbox" disabled="disabled" >
                                    <label class="form-check-label" >
                                        Vehicle Policy
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Security Policy" name="policies[]"type="checkbox" disabled="disabled" >
                                    <label class="form-check-label" >
                                       Security Policy
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Access Usage Policy" name="policies[]"type="checkbox" disabled="disabled" >
                                    <label class="form-check-label" >
                                        Access Usage Policy
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Operations Policy" name="policies[]"type="checkbox" disabled="disabled" >
                                    <label class="form-check-label" >
                                       Operations Policy
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">11. Please mention the availability of the following in your Organization. </label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Organogram: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Organogram" 
                                            value="YES" disabled="disabled" >
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Organogram" 
                                            value="NO" disabled="disabled" >
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Policy Makers: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Policy_Makers" 
                                            value="YES" disabled="disabled" >
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Policy_Makers" 
                                            value="NO"disabled="disabled" >
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Recruitment Regulations: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Recruitment_Regulations" 
                                            value="YES" disabled="disabled" >
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Recruitment_Regulations" 
                                            value="NO"disabled="disabled" >
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Promotion Policy: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Promotion_Policy" 
                                            value="YES" disabled="disabled" >
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Promotion_Policy" 
                                            value="NO"disabled="disabled" >
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Annual Appraisal: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Annual_Appraisal" 
                                            value="YES" disabled="disabled" >
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Annual_Appraisal" 
                                            value="NO"disabled="disabled" >
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Gratuity: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Gratuity" 
                                            value="YES" disabled="disabled" >
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Gratuity" 
                                            value="NO"disabled="disabled" >
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Provident Fund: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Provident_Fund" 
                                            value="YES" disabled="disabled" >
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Provident_Fund" 
                                            value="NO"disabled="disabled" >
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Welfare Fund: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Welfare_Fund" 
                                            value="YES" disabled="disabled" >
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Welfare_Fund" 
                                            value="NO"disabled="disabled" >
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Group Insurance: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Group_Insurance" 
                                            value="YES" disabled="disabled" >
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Group_Insurance" 
                                            value="NO"disabled="disabled" >
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Pension Benefits: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Pension_Benefits" 
                                            value="YES" disabled="disabled" >
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Pension_Benefits" 
                                            value="NO" disabled="disabled">
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">12. How many Employees were working in your Organization on January 31, 2021 under the following distinct categories?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Permanent/Full Time : </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="Permanent"
                                        placeholder="Permanent/Full Time" id="Permanent" disabled="disabled">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Contractual/ Part-time </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="Contractual" 
                                        placeholder="Contractual/ Part-time" id="Contractual" disabled="disabled">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Outsourced/ Honorary: </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="Outsourced"
                                        placeholder="Outsourced/ Honorary" id="Outsourced" disabled="disabled">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">13. What is the turnover rate [in %] of the employees in last 5 years? </label>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="Turnover_Rate"
                                        placeholder="Turnover Rate" id="Turnover_Rate" disabled="disabled">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                                <label class="col-sm-4 col-form-label input__label"> %</label>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-6">
                                    <div class="form-group ">
                                    <label class="input__label">14. How many positions of your Organogram are filled at the moment [in %]??</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                    
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Filled_Position"
                                            value="<50%" disabled="disabled">
                                        <label class="form-check-label" >
                                           <50%
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Filled_Position"
                                            value="50%-80%" disabled="disabled">
                                        <label class="form-check-label">
                                            50%-80%
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Filled_Position" 
                                            value=">80%" disabled="disabled">
                                        <label class="form-check-label">
                                            >80%
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Filled_Position" 
                                            value="Not Applicable" disabled="disabled">
                                        <label class="form-check-label">
                                            Not Applicable,we don't have any organogram
                                        </label>
                                    </div>        
                                </div>
                            </div>
                        </fieldset>
                        <div class="cards__heading">
                            <h3> Promotion and Visiblity <span></span></h3>
                        </div>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-8">
                                    <div class="form-group ">
                                    <label class="input__label">15. Do you think that more efforts are required by the regulatory bodies to ensure smooth functioning of the NREN?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                    
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="PV_effort_REN" 
                                            value="YES" disabled="disabled">
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="PV_effort_REN" 
                                            value="NO" disabled="disabled">
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>      
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">16. Awareness programs attended by Regulatory Bodies in last 3 years? </label>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="Awareness_programs" 
                                        placeholder="Awareness Programs" disabled="disabled" id="Awareness_programs">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-8">
                                    <div class="form-group ">
                                    <label class="input__label">17. What is the percent (%) of budget allocated for promotional activities with respect to your total budget?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                    
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="budget_promotional"
                                            value="0%" disabled="disabled">
                                        <label class="form-check-label" >
                                           0%
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="budget_promotional"
                                            value=">0%" disabled="disabled">
                                        <label class="form-check-label">
                                            >0%
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="budget_promotional" 
                                            value=">1%" disabled="disabled">
                                        <label class="form-check-label">
                                            >1%
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="budget_promotional" 
                                            value=">5%" disabled="disabled">
                                        <label class="form-check-label">
                                            >5%
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="budget_promotional" 
                                            value=">10%" disabled="disabled">
                                        <label class="form-check-label">
                                            >10%
                                        </label>
                                    </div>          
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label"> 18. How many Awareness Programs [Seminar/Workshop/Training] have been conducted in last 3 years? </label>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="Awareness_Programs_conducted"
                                        placeholder="Awareness Programs" disabled="disabled" id="Awareness_Programs_conducted">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-8">
                                    <div class="form-group ">
                                    <label class="input__label">19. Promotion/Market Development personnel Presence?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                    
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Development_personnel" 
                                            value="YES" disabled="disabled">
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Development_personnel" 
                                            value="NO" disabled="disabled">
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>      
                                </div>
                            </div>
                        </fieldset>

                         <div class="cards__heading">
                            <h3> Collaboration: <span></span></h3>
                        </div>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">20. Rate your collaboration with NRENs, Users, Vendors and Policy Makers</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> NRENs: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_NREN" 
                                            value="Totally Unsuccessful" disabled="disabled">
                                        <label class="form-check-label" >
                                            Totally Unsuccessful
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_NREN" 
                                            value="Unsuccessful" disabled="disabled">
                                        <label class="form-check-label" >
                                            Unsuccessful
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_NREN" 
                                            value="Successful" disabled="disabled">
                                        <label class="form-check-label" >
                                           Successful
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_NREN" 
                                            value="Very Successful" disabled="disabled">
                                        <label class="form-check-label" >
                                           Very Successful
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_NREN" 
                                            value="Neutral" disabled="disabled">
                                        <label class="form-check-label" >
                                           Neutral
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Universities/Research Institutions: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_UR" 
                                            value="Totally Unsuccessful" disabled="disabled">
                                        <label class="form-check-label" >
                                            Totally Unsuccessful
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_UR" 
                                            value="Unsuccessful" disabled="disabled">
                                        <label class="form-check-label" >
                                            Unsuccessful
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_UR" 
                                            value="Successful" disabled="disabled">
                                        <label class="form-check-label" >
                                           Successful
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_UR" 
                                            value="Very Successful" disabled="disabled">
                                        <label class="form-check-label" >
                                           Very Successful
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_UR" 
                                            value="Neutral" disabled="disabled">
                                        <label class="form-check-label" >
                                           Neutral
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Vendors: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_vendor" 
                                            value="Totally Unsuccessful" disabled="disabled">
                                        <label class="form-check-label" >
                                            Totally Unsuccessful
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_vendor" 
                                            value="Unsuccessful" disabled="disabled">
                                        <label class="form-check-label" >
                                            Unsuccessful
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_vendor" 
                                            value="Successful" disabled="disabled">
                                        <label class="form-check-label" >
                                           Successful
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_vendor" 
                                            value="Very Successful" disabled="disabled">
                                        <label class="form-check-label" >
                                           Very Successful
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_vendor" 
                                            value="Neutral" disabled="disabled">
                                        <label class="form-check-label" >
                                           Neutral
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Policy Makers (Regulators/Ministries): </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_policy_RM" 
                                            value="Totally Unsuccessful" disabled="disabled">
                                        <label class="form-check-label" >
                                            Totally Unsuccessful
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_policy_RM" 
                                            value="Unsuccessful" disabled="disabled">
                                        <label class="form-check-label" >
                                            Unsuccessful
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_policy_RM" 
                                            value="Successful" disabled="disabled">
                                        <label class="form-check-label" >
                                           Successful
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_policy_RM" 
                                            value="Very Successful" disabled="disabled">
                                        <label class="form-check-label" >
                                           Very Successful
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_policy_RM" 
                                            value="Neutral" disabled="disabled">
                                        <label class="form-check-label" >
                                           Neutral
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-4 col-form-label input__label"> Policy Makers (Higher Education Commission): </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_policy_HEC" 
                                            value="Totally Unsuccessful" disabled="disabled">
                                        <label class="form-check-label" >
                                            Totally Unsuccessful
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_policy_HEC" 
                                            value="Unsuccessful" disabled="disabled">
                                        <label class="form-check-label" >
                                            Unsuccessful
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_policy_HEC" 
                                            value="Successful" disabled="disabled">
                                        <label class="form-check-label" >
                                           Successful
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_policy_HEC" 
                                            value="Very Successful" disabled="disabled">
                                        <label class="form-check-label" >
                                           Very Successful
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_policy_HEC" 
                                            value="Neutral" disabled="disabled">
                                        <label class="form-check-label" >
                                           Neutral
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <div class="cards__heading">
                            <h3> Human Resources</h3>
                        </div>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">21. How do you perform your "System Operations"?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Routing and Switching: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationRandS" 
                                            value="Fully Self" disabled="disabled">
                                        <label class="form-check-label" >
                                            Fully Self
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationRandS" 
                                            value="Fully Outsourced" disabled="disabled">
                                        <label class="form-check-label" >
                                            Fully Outsourced
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationRandS" 
                                            value="Hybrid" disabled="disabled">
                                        <label class="form-check-label" >
                                           Hybrid [Self+Outsourced]
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationRandS" 
                                            value="Not Applicable" disabled="disabled">
                                        <label class="form-check-label" >
                                           Not Applicable
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Transmission Network: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationTN" 
                                            value="Fully Self" disabled="disabled">
                                        <label class="form-check-label" >
                                            Fully Self
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationTN" 
                                            value="Fully Outsourced" disabled="disabled">
                                        <label class="form-check-label" >
                                            Fully Outsourced
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationTN" 
                                            value="Hybrid" disabled="disabled">
                                        <label class="form-check-label" >
                                           Hybrid [Self+Outsourced]
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationTN" 
                                            value="Not Applicable" disabled="disabled">
                                        <label class="form-check-label" >
                                           Not Applicable
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Compute and Virtualization: </label>
                                 <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationCV" 
                                            value="Fully Self" disabled="disabled">
                                        <label class="form-check-label" >
                                            Fully Self
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationCV" 
                                            value="Fully Outsourced" disabled="disabled">
                                        <label class="form-check-label" >
                                            Fully Outsourced
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationCV" 
                                            value="Hybrid" disabled="disabled">
                                        <label class="form-check-label" >
                                           Hybrid [Self+Outsourced]
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationCV" 
                                            value="Not Applicable" disabled="disabled">
                                        <label class="form-check-label" >
                                           Not Applicable
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Video Collaboration: </label>
                                 <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationCollaboration" 
                                            value="Fully Self" disabled="disabled">
                                        <label class="form-check-label" >
                                            Fully Self
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationCollaboration" 
                                            value="Fully Outsourced" disabled="disabled">
                                        <label class="form-check-label" >
                                            Fully Outsourced
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationCollaboration" 
                                            value="Hybrid" disabled="disabled">
                                        <label class="form-check-label" >
                                           Hybrid [Self+Outsourced]
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationCollaboration" 
                                            value="Not Applicable" disabled="disabled">
                                        <label class="form-check-label" >
                                           Not Applicable
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Application Services: </label>
                                 <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationAS" 
                                            value="Fully Self" disabled="disabled">
                                        <label class="form-check-label" >
                                            Fully Self
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationAS" 
                                            value="Fully Outsourced" disabled="disabled">
                                        <label class="form-check-label" >
                                            Fully Outsourced
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationAS" 
                                            value="Hybrid" disabled="disabled">
                                        <label class="form-check-label" >
                                           Hybrid [Self+Outsourced]
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationAS" 
                                            value="Not Applicable" disabled="disabled">
                                        <label class="form-check-label" >
                                           Not Applicable
                                        </label>
                                    </div>
                                </div>
                            </div>
                             <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others: </label>
                                 <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationOther" 
                                            value="Fully Self" disabled="disabled">
                                        <label class="form-check-label" >
                                            Fully Self
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationOther" 
                                            value="Fully Outsourced" disabled="disabled">
                                        <label class="form-check-label" >
                                            Fully Outsourced
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationOther" 
                                            value="Hybrid" disabled="disabled">
                                        <label class="form-check-label" >
                                           Hybrid [Self+Outsourced]
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationOther" 
                                            value="Not Applicable" disabled="disabled">
                                        <label class="form-check-label" >
                                           Not Applicable
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">22. How many Employees were working in your Organization on January 31, 2021 under the following distinct categories?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Technical : </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="human_resouces_technical"
                                        placeholder="Technical" id="human_resouces_technical" disabled="disabled">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non-Technical : </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="human_resouces_Nontechnical" 
                                        placeholder="Non-Technical" id="human_resouces_Nontechnical" disabled="disabled">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>



                        <div class="cards__heading">
                            <h3> Financial Stability: <span></span></h3>
                        </div>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">23. Type of Financing for your NREN??</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                    
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="financial_type" 
                                            value="Fully Government Financed" disabled="disabled">
                                        <label class="form-check-label" >
                                            Fully Government Financed
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="financial_type" 
                                            value="Hybrid Financing" disabled="disabled">
                                        <label class="form-check-label" >
                                          Hybrid Financing
                                        </label>
                                    </div>  
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="financial_type" 
                                            value="Fully Self-financed" disabled="disabled">
                                        <label class="form-check-label" >
                                          Fully Self-financed
                                        </label>
                                    </div>      
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">24. Revenue Coverage</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">

                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="FS_Revenue_Coverage" 
                                            value="Full CAPEX and Full OPEX" disabled="disabled">
                                        <label class="form-check-label" >
                                           Full CAPEX and Full OPEX
                                        </label>
                                    </div>
                    
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="FS_Revenue_Coverage" 
                                            value="Full OPEX and Part of CAPEX" disabled="disabled">
                                        <label class="form-check-label" >
                                            Full OPEX and Part of CAPEX
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="FS_Revenue_Coverage" 
                                            value="Full CAPEX and Part of OPEX" disabled="disabled">
                                        <label class="form-check-label" >
                                            Full CAPEX and Part of OPEX
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="FS_Revenue_Coverage" 
                                            value="Only OPex" disabled="disabled">
                                        <label class="form-check-label" >
                                            Only OPex
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="FS_Revenue_Coverage" 
                                            value="Only CAPex" disabled="disabled">
                                        <label class="form-check-label" >
                                            Only CAPex
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="FS_Revenue_Coverage" 
                                            value="Neither CAPEX nor OPEX" disabled="disabled">
                                        <label class="form-check-label" >
                                           Neither CAPEX nor OPEX
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="FS_Revenue_Coverage" 
                                            value="Not Applicable" disabled="disabled">
                                        <label class="form-check-label" >
                                            Not Applicable
                                        </label>
                                    </div>  
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">25. Compliance with LTS?</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                    
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="FS_compliance_lts" 
                                            value="YES" disabled="disabled">
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="FS_compliance_lts" 
                                            value="Not Applicable" disabled="disabled">
                                        <label class="form-check-label" >
                                            Not Applicable
                                        </label>
                                    </div>      
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-8">
                                    <div class="form-group ">
                                    <label class="input__label">26. Are there any financial constraints of funding by the government or regulatory bodies and a push to be financially self-sustainable?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                    
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="financial_constraint" 
                                            value="YES"  disabled="disabled">
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="financial_constraint" 
                                            value="NO"  disabled="disabled">
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>      
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-6">
                                    <div class="form-group ">
                                    <label class="input__label">27. LTC: Long Term Commitment from Government ?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="LTC" 
                                            value="YES" disabled="disabled">
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="LTC" 
                                            value="NO" disabled="disabled">
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="LTC" 
                                            value="Not Applicable" disabled="disabled">
                                        <label class="form-check-label" >
                                            Not Applicable
                                        </label>
                                    </div>        
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-6">
                                    <div class="form-group ">
                                    <label class="input__label">28. Do you consider yourself as an NREN who is highly struggling for financial sustainablity ??</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Highly_Struggling" 
                                            value="YES" disabled="disabled">
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Highly_Struggling"
                                            value="NO" disabled="disabled">
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>        
                                </div>
                            </div>
                        </fieldset>

                        <div class="cards__heading">
                            <h3>Human Expertise: <span></span></h3>
                        </div>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">29. Expertise of NREN Employees in percent (%) engaged in Administrative and Management.</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Experienced : </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="human_expert_admin" 
                                        placeholder="Experienced" id="human_expert_admin" disabled="disabled">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                                <label class="col-sm-4 col-form-label input__label">% </label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Intermediate </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="human_intermediate_admin" 
                                        placeholder="Intermediate" id="human_intermediate_admin" disabled="disabled">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                                <label class="col-sm-4 col-form-label input__label">% </label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Beginners: </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="human_beginner_admin" 
                                        placeholder="Beginners" id="human_beginner_admin" disabled="disabled">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                                <label class="col-sm-4 col-form-label input__label">% </label>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label  class="input__label">30. Aproximate % level of expertise among NREN Engineers </label>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-4 col-form-label input__label">Expert : </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="human_expert_tech" 
                                        placeholder="Expert " id="human_expert_tech" disabled="disabled">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                                <label class="col-sm-4 col-form-label input__label">% </label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Intermediate </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="human_intermediate_tech"
                                        placeholder="Intermediate" id="human_intermediate_tech" disabled="disabled">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                                <label class="col-sm-4 col-form-label input__label">% </label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Beginners: </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="human_beginner_tech" 
                                        placeholder="Beginners" id="human_beginner_tech" disabled="disabled">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                                <label class="col-sm-4 col-form-label input__label">% </label>
                            </div>
                        </fieldset>

                        <div class="cards__heading">
                            <h3> Services Offered: <span></span></h3>
                        </div>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">31. Which network services are you currently facilitating to REN Community ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="IPv4 Connectivity" name="network_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        IPv4 Connectivity
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="IPv6 Connectivity" name="network_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        IPv6 Connectivity
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="Network Monitoring" name="network_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Network Monitoring
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="Virtual Circuit/VPN" name="network_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Virtual Circuit/VPN
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="Multicast" name="network_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Multicast
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="Network Troubleshooting" name="network_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Network Troubleshooting
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="NetFlow Tool" name="network_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        NetFlow Tool
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="Optical Wavelength" name="network_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Optical Wavelength
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="Quality of Service" name="network_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Quality of Service
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="Managed Router Services" name="network_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Managed Router Services
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="Remote Access VPN Services" name="network_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Remote Access VPN Services
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="PERT" name="network_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Performance Enhancement Response Team (PERT)
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="SDN" name="network_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        SDN
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="Open Lightpath Exchange" name="network_service_SO[]" type="checkbox"disabled="disabled" >
                                    <label class="form-check-label" >
                                        Open Lightpath Exchange
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="Disaster Recovery Services" name="network_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Disaster Recovery Services
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>
                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">32. Which Identity services have you already deployed ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                <div class="form-check ">
                                    <input class="form-check-input identity_service_SO" value="Eduroam" name="identity_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Eduroam
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input identity_service_SO" value="Federated Services" name="identity_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Federated Services
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input identity_service_SO" value="EduGAIN" name="identity_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label">
                                        EduGAIN
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input identity_service_SO" value="GovRoam Services" name="identity_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        GovRoam Services
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input identity_service_SO" value="Hosted Campus AAI" name="identity_service_SO[]"  type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Hosted Campus AAI
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>
                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">33. Which security services are you offering ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                <div class="form-check">
                                    <input class="form-check-input security_service_SO" value="CERT/CSIRT" name="security_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        CERT/CSIRT
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input security_service_SO" value="DDoS Mitigation" name="security_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        DDoS Mitigation
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input security_service_SO" value="Network Troubleshooting" name="security_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Network Troubleshooting
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input security_service_SO" value="Anti-spam Solution" name="security_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Anti-spam Solution
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input security_service_SO" value="Vulnerability Scanning" name="security_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Vulnerability Scanning
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input security_service_SO" value="Firewall-on-demand" name="security_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                       Firewall-on-demand
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input security_service_SO" value="Intrusion Detection" name="security_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Intrusion Detection
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input security_service_SO" value="Identifier Registry" name="security_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                       Identifier Registry
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input security_service_SO" value="Security Auditing" name="security_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Security Auditing
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input security_service_SO" value="Web Filtering" name="security_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Web Filtering
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input security_service_SO" value="PGP Key Server" name="security_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        PGP Key Server
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input security_service_SO" value="Online Payment" name="security_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Online Payment
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>
                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">34. Do you cover any of the following Multimedia Services to your REN Community ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                <div class="form-check">
                                    <input class="form-check-input multimedia_service_SO" value="Web/Desktop Conferencing" name="multimedia_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                       Web/Desktop Conferencing
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input multimedia_service_SO" value="Video Streaming (E-Lesson)" name="multimedia_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                       Video Streaming (E-Lesson)
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input multimedia_service_SO" value="Event Recording / Streaming" name="multimedia_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Event Recording / Streaming 
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input multimedia_service_SO" value="TV / Radio Streaming" name="multimedia_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        TV / Radio Streaming
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input multimedia_service_SO" value="Media Post Production" name="multimedia_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Media Post Production
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input multimedia_service_SO" value="Multicast" name="multimedia_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Multicast
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>
                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">35. Which Collaboration services are you currently promoting ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="Mailing Lists" name="collaboration_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                       Mailing Lists
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="Email Server Hosting" name="collaboration_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Email Server Hosting
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="Journal Access" name="collaboration_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Journal Access
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="Video Conferencing Services" name="collaboration_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Video Conferencing Services
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="SIS( Students Information Service)" name="collaboration_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        SIS( Students Information Service)
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="Research Gateway" name="collaboration_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                       Research Gateway
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value=" VoIP" name="collaboration_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        VoIP
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="Project Collaboration Tools" name="collaboration_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Project Collaboration Tools
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="CMS Services" name="collaboration_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                       CMS Services
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="Database Services" name="collaboration_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                       Database Services
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="Survey-Polling Tool" name="collaboration_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                       Survey-Polling Tool
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="Instant Messages" name="collaboration_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Instant Messages
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="Scheduling Tools" name="collaboration_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Scheduling Tools
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="SMS Messaging" name="collaboration_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        SMS Messaging
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="Plagiarism" name="collaboration_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Plagiarism
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="Learning Management Services" name="collaboration_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Learning Management Services
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="ePortfolios" name="collaboration_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        ePortfolios
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="Class Registration Services" name="collaboration_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                       Class Registration Services
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>
                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">36. Do you offer the following Storage and Hosting Services ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                <div class="form-check">
                                    <input class="form-check-input storage_service_SO" value="DNS Hosting" name="storage_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                      DNS Hosting
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input storage_service_SO" value="File Sender" name="storage_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        File Sender
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input storage_service_SO" value="Virtual Machines" name="storage_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                       Virtual Machines/IaaS
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input storage_service_SO" value="Cloud Storage" name="storage_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                       Cloud Storage
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input storage_service_SO" value="Housing/Colocation" name="storage_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label">
                                      Housing/Colocation
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input storage_service_SO" value="Web Hosting" name="storage_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                       Web Hosting
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input storage_service_SO" value="SaaS" name="storage_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                       SaaS
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input storage_service_SO" value="Content Delivery" name="storage_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                      Content Delivery
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input storage_service_SO" value="Disaster Recovery" name="storage_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                      Disaster Recovery
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input storage_service_SO" value="Hot-Standby" name="storage_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                       Hot-Standby
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input storage_service_SO" value="Netnews" name="storage_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Netnews
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>
                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">37. Do you have any Professional Services ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                <div class="form-check">
                                    <input class="form-check-input professional_service_SO" value="Hosting of User Conference" name="professional_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                     Hosting of User Conference
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input professional_service_SO" value="Consultancy/Training" name="professional_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Consultancy/Training
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input professional_service_SO" value="User Portals" name="professional_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                       User Portals
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input professional_service_SO" value="Dissemination of Information" name="professional_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                       Dissemination of Information (Messages/News Letter)
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input professional_service_SO" value="Procurement Services" name="professional_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                     Procurement Services
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input professional_service_SO" value="Software Licenses" name="professional_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                       Software Licenses
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input professional_service_SO" value="Web Development" name="professional_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                     Web Development
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input professional_service_SO" value="Software Development" name="professional_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                      Software Development
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input professional_service_SO" value="Finance/Admin System" name="professional_service_SO[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                     Finance/Admin System
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">38. Please Check if you have any of the listed Deviation ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                <div class="form-check">
                                    <input class="form-check-input Critical_Deviation" value="Non-connectivity with Research Institute/Universities" name="Critical_Deviation[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Non-connectivity with Research Institute/Universities
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input Critical_Deviation" value="Non-connectivity with Higher Educational Institute" name="Critical_Deviation[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Non-connectivity with Higher Educational Institute
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input Critical_Deviation" value="Non-connectivity with Global Research Network" name="Critical_Deviation[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label">
                                        Non-connectivity with Global Research Network
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input Critical_Deviation" value="Non-ownership of R&S Device" name="Critical_Deviation[]" type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                        Non-ownership of R&S Device
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input Critical_Deviation" value="No Internal Network Connectivity to Interconnect Institutes/Universities" name="Critical_Deviation[]"  type="checkbox" disabled="disabled">
                                    <label class="form-check-label" >
                                       No Internal Network Connectivity to Interconnect Institutes/Universities
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>

                <div class="card card_border py-2 mb-4">
                    <div class="card-body" style="background-color:#99ccff;">
                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">Do you want an email containing the result associated with the inputs?</label>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                    
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="alterSendScoreUser" id="YES"
                                            value="YES" disabled="disabled">
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="alterSendScoreUser" id="NO"
                                            value="NO" disabled="disabled">
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>      
                                </div>
                                 <div class="col-sm-4" id="altUserEmail"style="display: none;">
                                    <label>Email Address:</label>
                                    <input type="email" class="form-control input-style" name="email"
                                        placeholder="Email Address">
                                    <small id="emailHelp" class="form-text text-muted">* Please Provide your Email Address</small>
                                </div>
                            </div>
                        </fieldset>
                    </div>   
                </div>

                        <div class="row">
                            <div class="col-sm-5"></div>
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-primary btn-style mt-4">Assess Maturity</button>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- //forms 1 -->
     

    </div>
    <!-- //content -->

</div>
<!-- main content end-->
@endsection