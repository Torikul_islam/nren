
@extends('layouts.app')


@section('content')
<!-- main content start -->
<div class="main-content">

    <!-- content -->
    <div class="container-fluid content-top-gap">

        <!-- breadcrumbs -->
        <nav aria-label="breadcrumb" class="mb-4">
            <ol class="breadcrumb my-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Maturity Table</li>
            </ol>
        </nav>
        <!-- //breadcrumbs -->
        <!-- forms -->
        <section class="forms">
            <!-- forms 1 -->
            <div class="card card_border py-2 mb-4">
               <!--  <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="cards__heading col-sm-8">
                        <h3>Bangladesh Research and Education Network (BdREN) <span></span></h3>
                    </div>
                    <div class="col-sm-1"></div>
                </div> -->

                <div class="cards__heading">
                     <h3 style="text-align:center;">Bangladesh Research and Education Network (BdREN)</h3>
                    <h3 style="text-align:center;">Maturity Table</h3>
                </div>

                <div class="card-body">

                    <div style="overflow-x:auto;">
                        <table id="myFileTable" class="table table-striped table-bordered" width="100%" style="text-align: center;">
                            <thead class="text-capitalize " style="text-align: center;" >
                                <tr>
                                    <th rowspan ="2">ID</th>
                                    <th rowspan ="2" style="background-color: blue; color: white;">Organization</th>
                                    <th rowspan ="2">Others Organization Name</th>
                                    <th rowspan ="2">Coverage</th>
                                    <th rowspan ="2">Lifetime</th>
                                    <th colspan="2">Connected Institution</th>
                                    <th colspan="2">Maximum BW</th>
                                    <th colspan="2">Aggregate Up BW</th>
                                    <th rowspan ="2">Depth of Ownership</th>


                                    <th rowspan ="2">Service Offer</th>


                                    <th rowspan ="2">Financial Stablity</th>

                                    <th colspan="2">Human Resouces</th>


                                    <th rowspan ="2">OMG</th>
                                    <th rowspan ="2">Promotion & Visibility</th>
                                    <th rowspan ="2">Collaboration</th>
                                    
                                    
                                    <th colspan="2">Human Expert</th>
                                    <th rowspan ="2" style="background-color: blue; color: white;">Organization</th>
                                    
                                    <th rowspan ="2" style="background-color: blue; color: white;">Maturity Level</th>

                                    <th rowspan ="2">View/Edit</th>

                                </tr>
                                <tr>
                                    <th >Type</th>
                                    <th >Coverage</th>
                                    <th >University</th>
                                    <th >Research Institute</th>
                                    <th >Commodity</th>
                                    <th >R&E</th>
                                    <th >Tech</th>
                                    <th >Admin</th>
                                    <th >Tech</th>
                                    <th >Admin</th>
                                </tr>
                    
                            </thead>
                            <tbody>
                                @foreach ($m_data as $data)
                                    <tr>
                                        <td>{{$data->id}}</td>
                                        <td style="background-color: blue; color: white;">{{$data->getRENDATA->REN_Name}}</td>
                                        <td>{{$data->OtherOrganization}}</td>

                                        <td>{{$data->getCoverageScore->score}}</td>
                                        <td>{{$data->getLifetimevalue->lifetimeValue}}</td>
                                        <td>{{$data->getConnectedInstitutionlue->connected_institute_typescore}}</td>
                                        <td>{{$data->getConnectedInstitutionlue->connected_institute_coverage_score}}</td>

                                        <td>{{$data->getMaximumBW->Maximum_BW_U_score}}</td>
                                        <td>{{$data->getMaximumBW->Maximum_BW_RI_score}}</td>

                                        <td>{{$data->getAggregateBW->Commodity_score}}</td>
                                        <td>{{$data->getAggregateBW->Res_Edu_score}}</td>
                                        <td>{{$data->getOwwnerShipData->ownership_score}}</td>

                                        <td>{{$data->getServiceOfferData->offered_service_final_score}}</td>

                                        <td>{{$data->getFSData->financitial_stablity_grading}}</td>


                                        <td>{{$data->getHRData->human_resouce_technical_final_score}}</td>
                                        <td>{{$data->getHRData->human_resouce_nontechnical_final_score}}</td>



                                        <td>{{$data->getOMGData->omg_aggregate_score}}</td>
                                        <td>{{$data->getPVData->PV_aggregate_score}}</td>
                                        <td>{{$data->getcollaborationData->aggregate_collaborate_score}}</td>

                                        
                                        

                                        <td>{{$data->getHEData->aggreagte_tech_score}}</td>
                                        <td>{{$data->getHEData->aggreagte_admin_score}}</td>

                                        <td style="background-color: blue; color: white;">{{$data->getRENDATA->REN_Name}}</td>

                                        
                                        <td style="background-color: blue; color: white;">@php echo(round($data->maturity_level,1)) @endphp</td>


                                        <td><a href="{!!route('edit',$data->ren_type_id) !!}"><i class="fas fa-eye" style="font-size:25px;color:blue;" title="View Details"></i></a></td>



                                    </tr>
                                   
                                    
                                @endforeach
                                


                            </tbody>
                        
                        </table>
                    </div>
                </div>
            </div>
            <!-- //forms 1 -->
     

    </div>
    <!-- //content -->

</div>
<!-- main content end-->
@endsection


