<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
    <link rel="icon" type="image/png" href="{{ URL::asset('logindesign/images/icons/favicon.ico') }}"/>

<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="logindesign/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="logindesign/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="logindesign/vendor/animate/animate.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="logindesign/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="logindesign/vendor/select2/select2.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="logindesign/css/util.css">
    <link rel="stylesheet" type="text/css" href="logindesign/css/main.css">
<!--===============================================================================================-->
</head>
<body>
    
    <div class="limiter">
        <div class="container-login100" style="background-image: url('{{ URL::asset('assets/bg/calculator.jpg') }}');  background-size: cover; background-repeat:no-repeat;  background-position: center center;  height: 100vh; ">

                    <div style="margin-top: 33px;">
                        <span  style="font-size: 55px; color: #21A014 ; font-weight: bold;font-family: Calibri;">
                        NREN MATURITY CALCULATOR
                        </span>
                    </div>


            <div class="wrap-login100" style="background: #ffffff8a">
                <div class="login100-pic js-tilt" data-tilt>   
                    <img height="70" src="logindesign/images/test.png" alt="IMG">
                    <img src="logindesign/images/L.png" alt="IMG">
                    <br><br>
                      <h5 style=" font-weight: bold;color: black;">A BdREN & LEARN Joint Initiative</h5>     
                </div>
                          
                    
                    <div class="login100-form validate-form">

                    <div class="container-login100-form-btn">
                        <a class="login100-form-btn" href="{{ route('guestform') }}" >Guest Login</a>
                    </div>

                    <div class="container-login100-form-btn">
                        <a class="login100-form-btn" href="{{ route('login') }}" >Login</a>
                    </div>


                    <div class="text-center p-t-136" >
                        <a class="txt2" href="{{ route('register') }}" style=" font-weight: bold;font-size: 20px; color: black;">
                            Create your Account
                            <i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
                        </a>
                    </div> 
                    </div>
                   
              
            </div>
        </div>
    </div>
    
    

    
<!--===============================================================================================-->  
    <script src="logindesign/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
    <script src="logindesign/vendor/bootstrap/js/popper.js"></script>
    <script src="logindesign/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
    <script src="logindesign/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
    <script src="logindesign/vendor/tilt/tilt.jquery.min.js"></script>
    <script >
        $('.js-tilt').tilt({
            scale: 1.1
        })
    </script>
<!--===============================================================================================-->
    <script src="logindesign/js/main.js"></script>

</body>
</html>