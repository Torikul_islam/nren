<!DOCTYPE html>
<html lang="en">
<head>
    <title>Verify</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
    <link rel="icon" type="image/png" href="{{ URL::asset('logindesign/images/icons/favicon.ico') }}"/>

<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="logindesign/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="logindesign/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="logindesign/vendor/animate/animate.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="logindesign/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="logindesign/vendor/select2/select2.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="logindesign/css/util.css">
    <link rel="stylesheet" type="text/css" href="logindesign/css/main.css">
<!--===============================================================================================-->
</head>
<body>
    
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <!-- <div class="login100-pic js-tilt" data-tilt>   
                    <img src="logindesign/images/download.png" alt="IMG">
                    <img src="logindesign/images/L.png" alt="IMG">        
                </div> -->

                <form method="POST" action="{{ url('/verifyNow') }}">
                        @csrf

                        <span class="login100-form-title">
                        NREN Maturity Calculator
                        </span>

                            <center>
                                <h3>Verify your email address</h3>
                                <h5>Please check your email to get the verification code. Please submit your verification code on the bellow box, and press verify now button.
                                <?php if ($email): ?>
                                   If you didn't receive any email please click here: <a href="{{ url('resendCode')}}?email=<?php echo $email ?>">Resend Code</a>
                                 <?php endif ?> </h5>
                            </center>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                      <i class="icon-user"></i>
                                  </span>
                              </div>
                              <input type="email" class="form-control {{ $errors->has('email')?'is-invalid':'' }}" name="email" value="<?php echo $email ?>" placeholder="Email" required <?php if($email){ echo "readonly"; } ?> >
                              @if ($errors->has('email'))
                              <span class="invalid-feedback">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="icon-key"></i>
                              </span>
                          </div>
                          <input type="email_verification_code" class="form-control {{ $errors->has('email_verification_code')?'is-invalid':'' }}" placeholder="Verification code" name="email_verification_code" required>
                          @if ($errors->has('email_verification_code'))
                          <span class="invalid-feedback">
                             <strong>{{ $errors->first('email_verification_code') }}</strong>
                         </span>
                         @endif
                     </div>
                     <div class="row">
                        <div class="col-12">
                            @if (session()->has('error_message'))
                            <div class="alert alert-danger">
                               {{session('error_message')}} 
                           </div>    
                           @endif
                       </div>
                   </div>
                   <div class="row">
                    <div class="col-6">
                        <button class="btn btn-primary px-4" type="submit">Verify Now</button>
                    </div>
                </div>
                    </form>
            </div>
        </div>
    </div>
    
    

    
<!--===============================================================================================-->  
    <script src="logindesign/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
    <script src="logindesign/vendor/bootstrap/js/popper.js"></script>
    <script src="logindesign/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
    <script src="logindesign/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
    <script src="logindesign/vendor/tilt/tilt.jquery.min.js"></script>
    <script >
        $('.js-tilt').tilt({
            scale: 1.1
        })
    </script>
<!--===============================================================================================-->
    <script src="logindesign/js/main.js"></script>

</body>
</html>