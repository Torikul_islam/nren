<!DOCTYPE html>
<html lang="en">
<head>
    <title>Verify</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
    <link rel="icon" type="image/png" href="{{ URL::asset('logindesign/images/icons/favicon.ico') }}"/>

<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="logindesign/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="logindesign/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="logindesign/vendor/animate/animate.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="logindesign/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="logindesign/vendor/select2/select2.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="logindesign/css/util.css">
    <link rel="stylesheet" type="text/css" href="logindesign/css/main.css">
<!--===============================================================================================-->
</head>
<body>
    
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">

                <span class="login100-form-title">
                        NREN Maturity Calculator
                </span>

                   <div class="row justify-content-center">
                    <div class="col-md-8">
                      <div class="card-group">

                        <div class="card text-white bg-success py-5 d-md-down-none" style="width:44%">
                          <div class="card-body text-center">
                            <div>
                              <h2>Thank You For Your Registration</h2>
                              <h4>Your application is under verification through BdREN.Please wait maximum 24 hour for confirmation email.</h4>
                              <p></p>
                              <a class="btn btn-primary active mt-3" href="{{ url('/login') }}">Login</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                
            </div>
        </div>
    </div>
    
    

    
<!--===============================================================================================-->  
    <script src="logindesign/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
    <script src="logindesign/vendor/bootstrap/js/popper.js"></script>
    <script src="logindesign/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
    <script src="logindesign/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
    <script src="logindesign/vendor/tilt/tilt.jquery.min.js"></script>
    <script >
        $('.js-tilt').tilt({
            scale: 1.1
        })
    </script>
<!--===============================================================================================-->
    <script src="logindesign/js/main.js"></script>

</body>
</html>