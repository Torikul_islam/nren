
<!doctype html>
<html lang="en">

<head></head>

<body>


<!-- main content start -->
<div class="main-content">

    <!-- content -->
    <div class="container-fluid content-top-gap">

        <!-- forms -->
        <section class="forms">
            <!-- forms 1 -->
            <div class="card card_border py-2 mb-4">

                <div class="cards__heading">
                            <h3 style="text-align:center; font-size: 40px;">NREN Maturity Calculator</h3>
                </div>

                <div class="card-body">                   

                            
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">1. Which Organization / NREN are you from ?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Organization:</label>
                                        <input type="text" class="form-control input-style" name="email"
                                            placeholder="Email Address" value="{{$Organization}}">
                                    </div>
                                </div>
                                <div class="col-sm-4" id="Othersorganization">
                                        <label>Email Address:</label>
                                        <input type="text" class="form-control input-style" name="email"
                                            placeholder="Email Address" value="{{$email}}">
                                </div>
                            </div>
                       



                        <div class="cards__heading">
                            <h3>Coverage<span></span></h3>
                        </div>
                        <div class="form-group row">

                            <label class="col-sm-4 col-form-label input__label"> 2. How many universities are there in Total in your country ?</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control input-style" name="coverage_total_university" value="{{ $coverage_total_university}}"id="coverage_total_university"
                                    placeholder="Total Universities">
                                
                            </div>
                        </div>
                        <div class="form-group row">

                            <label class="col-sm-4 col-form-label input__label"> 3. How many universities Were Connected To Your Network ?</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control input-style" name="coverage_university" value="{{ $coverage_university}}" id="coverage_university"
                                    placeholder="Connected Universities">
                                
                            </div>
                        </div>


                        <div class="cards__heading">
                            <h3> Lifetime<span></span></h3>
                        </div>

                        
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">4. How long have you been in Business ?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                    
                                    
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Lifetime"
                                            value="1-3 Years" <?php if ($Lifetime == '1-3 Years') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            1-3 Years
                                        </label>
                                    </div>
                                   
                                    <div class="form-check">
                                         
                                        <input class="form-check-input" type="radio" name="Lifetime"
                                            value="4-5 Years" <?php if ($Lifetime == '4-5 Years') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            4-5 Years
                                        </label>
                                         
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Lifetime" 
                                            value="6-10 Years" <?php if ($Lifetime == '6-10 Years') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            6-10 Years
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        
                                        <input class="form-check-input" type="radio" name="Lifetime" 
                                            value="> 10 Years" <?php if ($Lifetime == '> 10 Years') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            > 10 Years
                                        </label>
                                        
                                    </div>        
                                </div>
                            </div>
                       

                        <div class="cards__heading">
                            <h3> Connected Institutions<span></span></h3>
                        </div>

                       
                            <div class="form-group">
                                <label class="input__label">5. How many Institutions/members were connected to your network??</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="connected_institute_university" class="form-control input-style" value="{{$connected_institute_university }}" placeholder="Universities">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Research Institutes: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="connected_institute_research" class="form-control input-style" value="{{ $connected_institute_research }}"
                                        placeholder="Research Institutes">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Government Organizations: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="connected_institute_govt" class="form-control input-style" value="{{ $connected_institute_govt }}"
                                        placeholder="Government Organizations">
                                  
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Schools and Colleges: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="connected_institute_school" class="form-control input-style" value="{{ $connected_institute_school }}"
                                        placeholder="Schools and Colleges">
                                  
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-4 col-form-label input__label">Others: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="connected_institute_other"class="form-control input-style" value="{{ $connected_institute_other }}"
                                        placeholder="Others">
                                    
                                </div>
                            </div>
                   

                        <div class="cards__heading">
                            <h3> Maximum BW<span></span></h3>
                        </div>

                        
                            <div class="row">
                                <div class = "col-sm-6">
                                    <div class="form-group ">
                                    <label class="input__label">6. The Maximum Bandwith that is deliverable among the universities ?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Maximum_BW_U" 
                                            value="None" <?php if ($Maximum_BW_U == 'None') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           None
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Maximum_BW_U"
                                            value="< 100 Mbps" <?php if ($Maximum_BW_U == '< 100 Mbps') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            < 100 Mbps
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Maximum_BW_U" 
                                            value="100 Mbps" <?php if ($Maximum_BW_U == '100 Mbps') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            100 Mbps
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Maximum_BW_U" 
                                            value="1 Gbps" <?php if ($Maximum_BW_U == '1 Gbps') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                             1 Gbps
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Maximum_BW_U" 
                                            value="10 Gbps" <?php if ($Maximum_BW_U == '10 Gbps') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                           10 Gbps
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Maximum_BW_U" 
                                            value="100 Gbps" <?php if ($Maximum_BW_U == '100 Gbps') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            100 Gbps
                                        </label>
                                    </div>       
                                </div>
                            </div>
                     
                       
                            <div class="row">
                                <div class = "col-sm-6">
                                    <div class="form-group ">
                                    <label class="input__label">7.  The Maximum Bandwith that is deliverable among the Research Instituates ?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Maximum_BW_RI" 
                                            value="None" <?php if ($Maximum_BW_RI == 'None') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            None
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Maximum_BW_RI" 
                                            value="< 100 Mbps" <?php if ($Maximum_BW_RI == '< 100 Mbps') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            < 100 Mbps
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Maximum_BW_RI" 
                                            value="100 Mbps" <?php if ($Maximum_BW_RI == '100 Mbps') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           100 Mbps
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Maximum_BW_RI" 
                                            value="1 Gbps" <?php if ($Maximum_BW_RI == '1 Gbps') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            1 Gbps
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Maximum_BW_RI" 
                                            value="10 Gbps" <?php if ($Maximum_BW_RI == '10 Gbps') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            10 Gbps
                                        </label>
                                    </div>  
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Maximum_BW_RI" 
                                            value="100 Gbps" <?php if ($Maximum_BW_RI == '100 Gbps') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            100 Gbps
                                        </label>
                                    </div>      
                                </div>
                            </div>
                        

                        <div class="cards__heading">
                            <h3> Aggregate Upstream Bandwidth <span></span></h3>
                        </div>
                     
                            <div class="form-group">
                                <label class="input__label">8. Your Total Upstream Traffic Capacity, Mbps</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Commodity : </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Commodity" class="form-control input-style" value="{{ $Commodity }}"
                                        placeholder="Commodity">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Research and Education: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Res_Edu" class="form-control input-style" value="{{ $Res_Edu }}"
                                        placeholder="Research and Education">
                                    
                                </div>
                            </div>
                       

                        <div class="cards__heading">
                            <h3> Device Ownership <span></span></h3>
                        </div>
                       
                            <div class="form-group">
                                <label class="input__label">9. What is the "Ownership Model" of the key components of your Network?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Routing and Switching: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="RandS"
                                            value="Fully Owned" <?php if ($RandS == 'Fully Owned') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Fully Owned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="RandS" 
                                            value="Long Term Leased" <?php if ($RandS == 'Long Term Leased') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Long Term Leased
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="RandS" 
                                            value="Hybrid" <?php if ($RandS == 'Hybrid') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Hybrid
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="RandS" 
                                            value="Rented" <?php if ($RandS == 'Rented') echo 'checked="checked"'; ?>> 
                                        <label class="form-check-label" >
                                           Rented
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="RandS" 
                                            value="Not Applicable" <?php if ($RandS == 'Not Applicable') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Not Applicable
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-4 col-form-label input__label"> Transmission Network: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Trans_Network" 
                                            value="Fully Owned" <?php if ($Trans_Network == 'Fully Owned') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Fully Owned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Trans_Network" 
                                            value="Long Term Leased" <?php if ($Trans_Network == 'Long Term Leased') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Long Term Leased
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Trans_Network" 
                                            value="Hybrid" <?php if ($Trans_Network == 'Hybrid') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Hybrid
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Trans_Network" 
                                            value="Rented" <?php if ($Trans_Network == 'Rented') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Rented
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Trans_Network" 
                                            value="Not Applicable" <?php if ($Trans_Network == 'Not Applicable') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Not Applicable
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Compute and Virtualization: </label>
                                 <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="CandV" 
                                            value="Fully Owned" <?php if ($CandV == 'Fully Owned') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Fully Owned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="CandV" 
                                            value="Long Term Leased" <?php if ($CandV == 'Long Term Leased') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Long Term Leased
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="CandV" 
                                            value="Hybrid" <?php if ($CandV == 'Hybrid') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Hybrid
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="CandV" 
                                            value="Rented" <?php if ($CandV == 'Rented') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Rented
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="CandV" 
                                            value="Not Applicable" <?php if ($CandV == 'Not Applicable') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Not Applicable
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Video Collaboration: </label>
                                 <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="VideoColla" 
                                            value="Fully Owned" <?php if ($VideoColla == 'Fully Owned') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Fully Owned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="VideoColla" 
                                            value="Long Term Leased" <?php if ($VideoColla == 'Long Term Leased') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Long Term Leased
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="VideoColla" 
                                            value="Hybrid" <?php if ($VideoColla == 'Hybrid') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Hybrid
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="VideoColla" 
                                            value="Rented" <?php if ($VideoColla == 'Rented') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Rented
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="VideoColla" 
                                            value="Not Applicable" <?php if ($VideoColla == 'Not Applicable') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Not Applicable
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-4 col-form-label input__label"> Backbone Fibre: </label>
                                 <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="BackboneFibre" 
                                            value="Fully Owned" <?php if ($BackboneFibre == 'Fully Owned') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Fully Owned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="BackboneFibre" 
                                            value="Long Term Leased" <?php if ($BackboneFibre == 'Long Term Leased') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Long Term Leased
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="BackboneFibre" 
                                            value="Hybrid" <?php if ($BackboneFibre == 'Hybrid') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Hybrid
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="BackboneFibre" 
                                            value="Rented" <?php if ($BackboneFibre == 'Rented') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Rented
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="BackboneFibre" 
                                            value="Not Applicable" <?php if ($BackboneFibre == 'Not Applicable') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Not Applicable
                                        </label>
                                    </div>
                                </div>
                            </div>
                             <div class="form-group row">
                                <label  class="col-sm-4 col-form-label input__label"> Last mile Fibre: </label>
                                 <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="LMF" 
                                            value="Fully Owned" <?php if ($LMF == 'Fully Owned') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Fully Owned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="LMF" i
                                            value="Long Term Leased" <?php if ($LMF == 'Long Term Leased') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Long Term Leased
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="LMF" 
                                            value="Hybrid" <?php if ($LMF == 'Hybrid') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Hybrid
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="LMF" 
                                            value="Rented" <?php if ($LMF == 'Rented') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Rented
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="LMF" 
                                            value="Not Applicable" <?php if ($LMF == 'Not Applicable') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Not Applicable
                                        </label>
                                    </div>
                                </div>
                            </div>
                             <div class="form-group row">
                                <label  class="col-sm-4 col-form-label input__label"> Civil and Power Infrastructure at PoPs: </label>
                                 <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="CivilPI" 
                                            value="Fully Owned" <?php if ($CivilPI == 'Fully Owned') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Fully Owned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="CivilPI" 
                                            value="Long Term Leased" <?php if ($CivilPI == 'Long Term Leased') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Long Term Leased
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="CivilPI" 
                                            value="Hybrid" <?php if ($CivilPI == 'Hybrid') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Hybrid
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="CivilPI" 
                                            value="Rented" <?php if ($CivilPI == 'Rented') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Rented
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="CivilPI" 
                                            value="Not Applicable" <?php if ($CivilPI == 'Not Applicable') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Not Applicable
                                        </label>
                                    </div>
                                </div>
                            </div>
                       


                        <div class="cards__heading">
                            <h3> Organization, Management and Governance (OMG): <span></span></h3>
                        </div>
                       
                            <div class="form-group">
                                <label class="input__label">10. Please Select the policies that are available in your organization?</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">

                                @php
                                $days = array("Human Resource Policy","Procurement Policy and Rules", "Service Level Agreements","Finance Policy","Membership Policy","Service Usage Policy"," Tariff Policy", " Vehicle Policy","Security Policy","Access Usage Policy","Operations Policy");
                                $policies_data = $policies;

                                 if($policies_data === null){
                                    $policies_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check"> 
                                       @foreach($days as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('policies[]', $value, in_array($value, $policies_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>
                                       
                                       @endforeach
                                 </div>   
                                
                            </div>
                        </div>
                       

                       
                            <div class="form-group">
                                <label class="input__label">11. Please mention the availability of the following in your Organization. </label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Organogram: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Organogram" 
                                            value="YES" <?php if ($Organogram == 'YES') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Organogram" 
                                            value="NO" <?php if ($Organogram == 'NO') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Policy Makers: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Policy_Makers" 
                                            value="YES" <?php if ($Policy_Makers == 'YES') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Policy_Makers" 
                                            value="NO" <?php if ($Policy_Makers == 'NO') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Recruitment Regulations: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Recruitment_Regulations" 
                                            value="YES" <?php if ($Recruitment_Regulations == 'YES') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Recruitment_Regulations" 
                                            value="NO" <?php if ($Recruitment_Regulations == 'NO') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Promotion Policy: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Promotion_Policy" 
                                            value="YES" <?php if ($Promotion_Policy == 'YES') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Promotion_Policy" 
                                            value="NO" <?php if ($Promotion_Policy == 'NO') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Annual Appraisal: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Annual_Appraisal" 
                                            value="YES" <?php if ($Annual_Appraisal == 'YES') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Annual_Appraisal" 
                                            value="NO" <?php if ($Annual_Appraisal == 'NO') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Gratuity: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Gratuity" 
                                            value="YES" <?php if ($Gratuity == 'YES') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Gratuity" 
                                            value="NO" <?php if ($Gratuity == 'NO') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Provident Fund: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Provident_Fund" 
                                            value="YES" <?php if ($Provident_Fund == 'YES') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Provident_Fund" 
                                            value="NO" <?php if ($Provident_Fund == 'NO') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Welfare Fund: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Welfare_Fund" 
                                            value="YES" <?php if ($Welfare_Fund == 'YES') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Welfare_Fund" 
                                            value="NO" <?php if ($Welfare_Fund == 'NO') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Group Insurance: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Group_Insurance" 
                                            value="YES" <?php if ($Group_Insurance == 'YES') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Group_Insurance" 
                                            value="NO" <?php if ($Group_Insurance == 'NO') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Pension Benefits: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Pension_Benefits" 
                                            value="YES" <?php if ($Pension_Benefits == 'YES') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Pension_Benefits" 
                                            value="NO" <?php if ($Pension_Benefits == 'NO') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                      
                        
                            <div class="form-group">
                                <label class="input__label">12. How many Employees were working in your Organization on January 31, 2021 under the following distinct categories?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Permanent/Full Time : </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="Permanent"
                                        placeholder="Permanent/Full Time" value="{{ $Permanent }}">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Contractual/ Part-time </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="Contractual" 
                                        placeholder="Contractual/ Part-time" value="{{ $Contractual }}">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Outsourced/ Honorary: </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="Outsourced"
                                        placeholder="Outsourced/ Honorary" value="{{ $Outsourced }}">
                                   
                                </div>
                            </div>
                       

                        
                            <div class="form-group">
                                <label class="input__label">13. What is the turnover rate [in %] of the employees in last 5 years? </label>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="Turnover_Rate"
                                        placeholder="Turnover Rate" value="{{ $Turnover_Rate }}">
                                    
                                </div>
                                <label class="col-sm-4 col-form-label input__label"> %</label>
                            </div>
                       
                     
                            <div class="row">
                                <div class = "col-sm-6">
                                    <div class="form-group ">
                                    <label class="input__label">14. How many positions of your Organogram are filled at the moment [in %]??</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                    
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Filled_Position"
                                            value="<50%" <?php if ($Filled_Position == '<50%') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           <50%
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Filled_Position"
                                            value="50%-80%" <?php if ($Filled_Position == '50%-80%') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            50%-80%
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Filled_Position" 
                                            value=">80%" <?php if ($Filled_Position == '>80%') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            >80%
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Filled_Position" 
                                            value="Not Applicable" <?php if ($Filled_Position == 'Not Applicable') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            Not Applicable,we don't have any organogram
                                        </label>
                                    </div>        
                                </div>
                            </div>
                       
                        <div class="cards__heading">
                            <h3> Promotion and Visiblity <span></span></h3>
                        </div>

                       
                            <div class="row">
                                <div class = "col-sm-8">
                                    <div class="form-group ">
                                    <label class="input__label">15. Do you think that more efforts are required by the regulatory bodies to ensure smooth functioning of the NREN?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                    
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="PV_effort_REN" 
                                            value="YES" <?php if ($PV_effort_REN == 'YES') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="PV_effort_REN" 
                                            value="NO" <?php if ($PV_effort_REN == 'NO') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>      
                                </div>
                            </div>
                      
                      
                            <div class="form-group">
                                <label class="input__label">16. Awareness programs attended by Regulatory Bodies in last 3 years? </label>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="Awareness_programs" 
                                        placeholder="Awareness Programs" value="{{ $Awareness_programs }}">
                                
                                </div>
                            </div>
                       

                      
                            <div class="row">
                                <div class = "col-sm-8">
                                    <div class="form-group ">
                                    <label class="input__label">17. What is the percent (%) of budget allocated for promotional activities with respect to your total budget?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                    
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="budget_promotional"
                                            value="0%" <?php if ($budget_promotional == '0%') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           0%
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="budget_promotional"
                                            value=">0%" <?php if ($budget_promotional == '>0%') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            >0%
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="budget_promotional" 
                                            value=">1%" <?php if ($budget_promotional == '>1%') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            >1%
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="budget_promotional" 
                                            value=">5%" <?php if ($budget_promotional == '>5%') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            >5%
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="budget_promotional" 
                                            value=">10%" <?php if ($budget_promotional == '>10%') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            >10%
                                        </label>
                                    </div>          
                                </div>
                            </div>
                  
                            <div class="form-group">
                                <label class="input__label"> 18. How many Awareness Programs [Seminar/Workshop/Training] have been conducted in last 3 years? </label>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="Awareness_Programs_conducted"
                                        placeholder="Awareness Programs" value="{{ $Awareness_Programs_conducted }}">
                                    
                                </div>
                            </div>
                      
                            <div class="row">
                                <div class = "col-sm-8">
                                    <div class="form-group ">
                                    <label class="input__label">19. Promotion/Market Development personnel Presence?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                    
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Development_personnel" 
                                            value="YES" <?php if ($Development_personnel == 'YES') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Development_personnel" 
                                            value="NO" <?php if ($Development_personnel == 'NO') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>      
                                </div>
                            </div>
                    

                         <div class="cards__heading">
                            <h3> Collaboration: <span></span></h3>
                        </div>

                            <div class="form-group">
                                <label class="input__label">20. Rate your collaboration with NRENs, Users, Vendors and Policy Makers</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> NRENs: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_NREN" 
                                            value="Totally Unsuccessful" <?php if ($collaboration_NREN == 'Totally Unsuccessful') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Totally Unsuccessful
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_NREN" 
                                            value="Unsuccessful" <?php if ($collaboration_NREN == 'Unsuccessful') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Unsuccessful
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_NREN" 
                                            value="Successful" <?php if ($collaboration_NREN == 'Successful') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Successful
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_NREN" 
                                            value="Very Successful" <?php if ($collaboration_NREN == 'Very Successful') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Very Successful
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_NREN" 
                                            value="Neutral" <?php if ($collaboration_NREN == 'Neutral') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Neutral
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Universities/Research Institutions: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_UR" 
                                            value="Totally Unsuccessful" <?php if ($collaboration_UR == 'Totally Unsuccessful') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Totally Unsuccessful
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_UR" 
                                            value="Unsuccessful" <?php if ($collaboration_UR == 'Unsuccessful') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Unsuccessful
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_UR" 
                                            value="Successful" <?php if ($collaboration_UR == 'Successful') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Successful
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_UR" 
                                            value="Very Successful" <?php if ($collaboration_UR == 'Very Successful') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Very Successful
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_UR" 
                                            value="Neutral" <?php if ($collaboration_UR == 'Neutral') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Neutral
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Vendors: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_vendor" 
                                            value="Totally Unsuccessful" <?php if ($collaboration_vendor == 'Totally Unsuccessful') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Totally Unsuccessful
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_vendor" 
                                            value="Unsuccessful" <?php if ($collaboration_vendor == 'Unsuccessful') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Unsuccessful
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_vendor" 
                                            value="Successful" <?php if ($collaboration_vendor == 'Successful') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Successful
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_vendor" 
                                            value="Very Successful" <?php if ($collaboration_vendor == 'Very Successful') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Very Successful
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_vendor" 
                                            value="Neutral" <?php if ($collaboration_vendor == 'Neutral') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Neutral
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Policy Makers (Regulators/Ministries): </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_policy_RM" 
                                            value="Totally Unsuccessful" <?php if ($collaboration_policy_RM == 'Totally Unsuccessful') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Totally Unsuccessful
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_policy_RM" 
                                            value="Unsuccessful" <?php if ($collaboration_policy_RM == 'Unsuccessful') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Unsuccessful
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_policy_RM" 
                                            value="Successful" <?php if ($collaboration_policy_RM == 'Successful') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Successful
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_policy_RM" 
                                            value="Very Successful" <?php if ($collaboration_policy_RM == 'Very Successful') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Very Successful
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_policy_RM" 
                                            value="Neutral" <?php if ($collaboration_policy_RM == 'Neutral') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Neutral
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-4 col-form-label input__label"> Policy Makers (Higher Education Commission): </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_policy_HEC" 
                                            value="Totally Unsuccessful" <?php if ($collaboration_policy_HEC == 'Totally Unsuccessful') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Totally Unsuccessful
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_policy_HEC" 
                                            value="Unsuccessful" <?php if ($collaboration_policy_HEC == 'Unsuccessful') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Unsuccessful
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_policy_HEC" 
                                            value="Successful" <?php if ($collaboration_policy_HEC == 'Successful') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Successful
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_policy_HEC" 
                                            value="Very Successful" <?php if ($collaboration_policy_HEC == 'Very Successful') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Very Successful
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="collaboration_policy_HEC" 
                                            value="Neutral" <?php if ($collaboration_policy_HEC == 'Neutral') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Neutral
                                        </label>
                                    </div>
                                </div>
                            </div>
                      
                        <div class="cards__heading">
                            <h3> Human Resources</h3>
                        </div>
                      
                            <div class="form-group">
                                <label class="input__label">21. How do you perform your "System Operations"?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Routing and Switching: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationRandS" 
                                            value="Fully Self" <?php if ($SystemOperationRandS == 'Fully Self') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Fully Self
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationRandS" 
                                            value="Fully Outsourced" <?php if ($SystemOperationRandS == 'Fully Outsourced') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Fully Outsourced
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationRandS" 
                                            value="Hybrid" <?php if ($SystemOperationRandS == 'Hybrid') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Hybrid [Self+Outsourced]
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationRandS" 
                                            value="Not Applicable" <?php if ($SystemOperationRandS == 'Not Applicable') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Not Applicable
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Transmission Network: </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationTN" 
                                            value="Fully Self" <?php if ($SystemOperationTN == 'Fully Self') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Fully Self
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationTN" 
                                            value="Fully Outsourced" <?php if ($SystemOperationTN == 'Fully Outsourced') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Fully Outsourced
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationTN" 
                                            value="Hybrid" <?php if ($SystemOperationTN == 'Hybrid') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Hybrid [Self+Outsourced]
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationTN" 
                                            value="Not Applicable" <?php if ($SystemOperationTN == 'Not Applicable') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Not Applicable
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Compute and Virtualization: </label>
                                 <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationCV" 
                                            value="Fully Self" <?php if ($SystemOperationCV == 'Fully Self') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Fully Self
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationCV" 
                                            value="Fully Outsourced" <?php if ($SystemOperationCV == 'Fully Outsourced') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Fully Outsourced
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationCV" 
                                            value="Hybrid" <?php if ($SystemOperationCV == 'Hybrid') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Hybrid [Self+Outsourced]
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationCV" 
                                            value="Not Applicable" <?php if ($SystemOperationCV == 'Not Applicable') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Not Applicable
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Video Collaboration: </label>
                                 <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationCollaboration" 
                                            value="Fully Self" <?php if ($SystemOperationCollaboration == 'Fully Self') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Fully Self
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationCollaboration" 
                                            value="Fully Outsourced" <?php if ($SystemOperationCollaboration == 'Fully Outsourced') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Fully Outsourced
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationCollaboration" 
                                            value="Hybrid" <?php if ($SystemOperationCollaboration == 'Hybrid') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Hybrid [Self+Outsourced]
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationCollaboration" 
                                            value="Not Applicable" <?php if ($SystemOperationCollaboration == 'Not Applicable') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Not Applicable
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Application Services: </label>
                                 <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationAS" 
                                            value="Fully Self" <?php if ($SystemOperationAS == 'Fully Self') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Fully Self
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationAS" 
                                            value="Fully Outsourced" <?php if ($SystemOperationAS == 'Fully Outsourced') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Fully Outsourced
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationAS" 
                                            value="Hybrid" <?php if ($SystemOperationAS == 'Hybrid') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Hybrid [Self+Outsourced]
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationAS" 
                                            value="Not Applicable" <?php if ($SystemOperationAS == 'Not Applicable') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Not Applicable
                                        </label>
                                    </div>
                                </div>
                            </div>
                             <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others: </label>
                                 <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationOther" 
                                            value="Fully Self" <?php if ($SystemOperationOther == 'Fully Self') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Fully Self
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationOther" 
                                            value="Fully Outsourced" <?php if ($SystemOperationOther == 'Fully Outsourced') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Fully Outsourced
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationOther" 
                                            value="Hybrid" <?php if ($SystemOperationOther == 'Hybrid') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Hybrid [Self+Outsourced]
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SystemOperationOther" 
                                            value="Not Applicable" <?php if ($SystemOperationOther == 'Not Applicable') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Not Applicable
                                        </label>
                                    </div>
                                </div>
                            </div>
                       

                      
                            <div class="form-group">
                                <label class="input__label">22. How many Employees were working in your Organization on January 31, 2021 under the following distinct categories?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Technical : </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="human_resouces_technical"
                                        placeholder="Technical" value="{{$human_resouces_technical}}">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non-Technical : </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="human_resouces_Nontechnical" 
                                        placeholder="Non-Technical" value="{{$human_resouces_Nontechnical}}">
                                    
                                </div>
                            </div>
                      



                        <div class="cards__heading">
                            <h3> Financial Stability: <span></span></h3>
                        </div>

                        
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">23. Type of Financing for your NREN??</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                    
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="financial_type" 
                                            value="Fully Government Financed" <?php if ($financial_type == 'Fully Government Financed') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Fully Government Financed
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="financial_type" 
                                            value="Hybrid Financing" <?php if ($financial_type == 'Hybrid Financing') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                          Hybrid Financing
                                        </label>
                                    </div>  
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="financial_type" 
                                            value="Fully Self-financed" <?php if ($financial_type == 'Fully Self-financed') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                          Fully Self-financed
                                        </label>
                                    </div>      
                                </div>
                            </div>
                       

                       
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">24. Revenue Coverage</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">

                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="FS_Revenue_Coverage" 
                                            value="Full CAPEX and Full OPEX" <?php if ($FS_Revenue_Coverage == 'Full CAPEX and Full OPEX') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Full CAPEX and Full OPEX
                                        </label>
                                    </div>
                    
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="FS_Revenue_Coverage" 
                                            value="Full OPEX and Part of CAPEX" <?php if ($FS_Revenue_Coverage == 'Full OPEX and Part of CAPEX') echo 'checked="checked"'; ?> >
                                        <label class="form-check-label" >
                                            Full OPEX and Part of CAPEX
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="FS_Revenue_Coverage" 
                                            value="Full CAPEX and Part of OPEX" <?php if ($FS_Revenue_Coverage == 'Full CAPEX and Part of OPEX') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Full CAPEX and Part of OPEX
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="FS_Revenue_Coverage" 
                                            value="Only OPex" <?php if ($FS_Revenue_Coverage == 'Only OPex') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Only OPex
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="FS_Revenue_Coverage" 
                                            value="Only CAPex" <?php if ($FS_Revenue_Coverage == 'Only CAPex') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Only CAPex
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="FS_Revenue_Coverage" 
                                            value="Neither CAPEX nor OPEX" <?php if ($FS_Revenue_Coverage == 'Neither CAPEX nor OPEX') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           Neither CAPEX nor OPEX
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="FS_Revenue_Coverage" 
                                            value="Not Applicable" <?php if ($FS_Revenue_Coverage == 'Not Applicable') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Not Applicable
                                        </label>
                                    </div>  
                                </div>
                            </div>
                     

                        
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">25. Compliance with LTS?</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                    
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="FS_compliance_lts" 
                                            value="YES" <?php if ($FS_compliance_lts == 'YES') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="FS_compliance_lts" 
                                            value="Not Applicable" <?php if ($FS_compliance_lts == 'Not Applicable') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Not Applicable
                                        </label>
                                    </div>      
                                </div>
                            </div>
                     
                            <div class="row">
                                <div class = "col-sm-8">
                                    <div class="form-group ">
                                    <label class="input__label">26. Are there any financial constraints of funding by the government or regulatory bodies and a push to be financially self-sustainable?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                    
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="financial_constraint" 
                                            value="YES" <?php if ($financial_constraint == 'YES') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="financial_constraint" 
                                            value="NO" <?php if ($financial_constraint == 'NO') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>      
                                </div>
                            </div>
                      
                            <div class="row">
                                <div class = "col-sm-6">
                                    <div class="form-group ">
                                    <label class="input__label">27. LTC: Long Term Commitment from Government ?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="LTC" 
                                            value="YES" <?php if ($LTC == 'YES') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="LTC" 
                                            value="NO" <?php if ($LTC == 'NO') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="LTC" 
                                            value="Not Applicable" <?php if ($LTC == 'Not Applicable') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Not Applicable
                                        </label>
                                    </div>        
                                </div>
                            </div>
                  
                            <div class="row">
                                <div class = "col-sm-6">
                                    <div class="form-group ">
                                    <label class="input__label">28. Do you consider yourself as an NREN who is highly struggling for financial sustainablity ??</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Highly_Struggling" 
                                            value="YES" <?php if ($highly_struggling == 'YES') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Highly_Struggling"
                                            value="NO" <?php if ($highly_struggling == 'NO') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            NO
                                        </label>
                                    </div>        
                                </div>
                            </div>
                    

                        <div class="cards__heading">
                            <h3>Human Expertise: <span></span></h3>
                        </div>

                            <div class="form-group">
                                <label class="input__label">29. Expertise of NREN Employees in percent (%) engaged in Administrative and Management.</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Experienced : </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="human_expert_admin" 
                                        placeholder="Experienced " value="{{$human_expert_admin}}">
                                    
                                </div>
                                <label class="col-sm-4 col-form-label input__label">% </label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Intermediate </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="human_intermediate_admin" 
                                        placeholder="Intermediate" value="{{$human_intermediate_admin}}">
                                    
                                </div>
                                <label class="col-sm-4 col-form-label input__label">% </label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Beginners: </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="human_beginner_admin" 
                                        placeholder="Beginners" value="{{$human_beginner_admin}}">
                                    
                                </div>
                                <label class="col-sm-4 col-form-label input__label">% </label>
                            </div>
                    

                      
                            <div class="form-group">
                                <label  class="input__label">30. Aproximate % level of expertise among NREN Engineers </label>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-4 col-form-label input__label">Expert : </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="human_expert_tech" 
                                        placeholder="Expert " value="{{$human_expert_tech}}">
                                </div>
                                <label class="col-sm-4 col-form-label input__label">% </label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Intermediate </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="human_intermediate_tech"
                                        placeholder="Intermediate" value="{{$human_intermediate_tech}}">
                                </div>
                                <label class="col-sm-4 col-form-label input__label">% </label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Beginners: </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-style" name="human_beginner_tech" 
                                        placeholder="Beginners" value="{{$human_beginner_tech}}">
                                </div>
                                <label class="col-sm-4 col-form-label input__label">% </label>
                            </div>
                     

                          <div class="cards__heading">
                            <h3> Services Offered: <span></span></h3>
                        </div>

                                                    
                            <div class="form-group">
                                <label class="input__label">31. Which network services are you currently facilitating to REN Community ??</label>
                            </div>
                            <div class="form-group row">
                               
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">

                                @php
                                $SO_Network = array("IPv4 Connectivity","IPv6 Connectivity", "Network Monitoring","Virtual Circuit/VPN","Multicast","Network Troubleshooting","NetFlow Tool", "VehiclOptical Wavelength", "Quality of Service", "Managed Router Services", "Remote Access VPN Services", "PERT", "SDN","Open Lightpath Exchange","Disaster Recovery Services");

                                $network_service_SO_data = $network_service_SO;
                                 if($network_service_SO_data === null){
                                    $network_service_SO_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check"> 
                                       @foreach($SO_Network as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('network_service_SO[]', $value, in_array($value, $network_service_SO_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>
                                       
                                       @endforeach
                                 </div> 
                            </div>
                        </div>

                     
                     
                            <div class="form-group">
                                <label class="input__label">32. Which Identity services have you already deployed ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                @php
                                $SO_identity = array("Eduroam","Federated Services", "EduGAIN","GovRoam Services","Hosted Campus AAI");

                                $identity_service_SO_data = $identity_service_SO;  
                                if($identity_service_SO_data === null){
                                    $identity_service_SO_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check"> 
                                       @foreach($SO_identity as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('identity_service_SO[]', $value, in_array($value, $identity_service_SO_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>
                                       
                                       @endforeach
                                 </div> 
                            </div>
                        </div>
                      
                       
                            <div class="form-group">
                                <label class="input__label">33. Which security services are you offering ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                @php
                                $SO_SS = array("CERT/CSIRT","DDoS Mitigation", "Network Troubleshooting","Anti-spam Solution","Vulnerability Scanning","Firewall-on-demand", "Intrusion Detection", "Identifier Registry", "Security Auditing", "Web Filtering", "PGP Key Server", "Online Payment");

                                $security_service_SO_data = $security_service_SO;
                                 if($security_service_SO_data === null){
                                    $security_service_SO_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check"> 
                                       @foreach($SO_SS as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('security_service_SO[]', $value, in_array($value, $security_service_SO_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>
                                       
                                       @endforeach
                                 </div>     
                    
                            </div>
                        </div>
                       
                      
                            <div class="form-group">
                                <label class="input__label">34. Do you cover any of the following Multimedia Services to your REN Community ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                @php
                                $SO_multimedia = array("Web/Desktop Conferencing","Video Streaming (E-Lesson)", "Event Recording / Streaming","TV / Radio Streaming","Media Post Production","Multicast");

                                $multimedia_service_SO_data = $multimedia_service_SO;
                                 if($multimedia_service_SO_data === null){
                                    $multimedia_service_SO_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check"> 
                                       @foreach($SO_multimedia as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('multimedia_service_SO[]', $value, in_array($value, $multimedia_service_SO_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>
                                       
                                       @endforeach
                                 </div>    
                            </div>
                        </div>
                       
                            <div class="form-group">
                                <label class="input__label">35. Which Collaboration services are you currently promoting ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                @php
                                $SO_colla = array("Mailing Lists","Email Server Hosting", "Journal Access","Research Gateway"," VoIP","Project Collaboration Tools","CMS Services","Database Services","Survey-Polling Tool","Instant Messages","Scheduling Tools","SMS Messaging","Plagiarism","Learning Management Services","ePortfolios","Class Registration Services","Video Conferencing Services","SIS( Students Information Service)");

                                $collaboration_service_SO_data = $collaboration_service_SO;
                                 if($collaboration_service_SO_data === null){
                                    $collaboration_service_SO_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check"> 
                                       @foreach($SO_colla as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('collaboration_service_SO[]', $value, in_array($value, $collaboration_service_SO_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>
                                       
                                       @endforeach
                                 </div>   
                            </div>
                        </div>
                      
                            <div class="form-group">
                                <label class="input__label">36. Do you offer the following Storage and Hosting Services ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                @php
                                $SO_storage = array("DNS Hosting","File Sender", "Virtual Machines","Cloud Storage","Housing/Colocation","Web Hosting","SaaS","Content Delivery","Disaster Recovery","Hot-Standby","Netnews");

                                $storage_service_SO_data = $storage_service_SO;
                                 if($storage_service_SO_data === null){
                                    $storage_service_SO_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check"> 
                                       @foreach($SO_storage as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('storage_service_SO[]', $value, in_array($value, $storage_service_SO_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>
                                       
                                       @endforeach
                                 </div>   
                            </div>
                        </div>
                      
                            <div class="form-group">
                                <label class="input__label">37. Do you have any Professional Services ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                @php
                                $SO_professional = array("Hosting of User Conference","Consultancy/Training", "User Portals","Dissemination of Information","Procurement Services","Software Licenses","Web Development","Software Development","Finance/Admin System");

                                $professional_service_SO_data = $professional_service_SO;
                                 if($professional_service_SO_data === null){
                                    $professional_service_SO_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check"> 
                                       @foreach($SO_professional as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('professional_service_SO[]', $value, in_array($value, $professional_service_SO_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>
                                       
                                       @endforeach
                                 </div> 
                            </div>
                        </div>
                       
                            <div class="form-group">
                                <label class="input__label">38. Please Check if you have any of the listed Deviation ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                @php
                                $CD = array("Non-connectivity with Research Institute/Universities","Non-connectivity with Higher Educational Institute", "Non-connectivity with Global Research Network","Non-ownership of R&S Device","No Internal Network Connectivity to Interconnect Institutes/Universities");

                                $Demerits_data = $Critical_Deviation;  
                                if($Demerits_data === null){
                                    $Demerits_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check"> 
                                       @foreach($CD as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('Critical_Deviation[]', $value, in_array($value, $Demerits_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>
                                       
                                       @endforeach
                                </div> 
                            </div>
                        </div>
                      
                </div>
            </div>
            <!-- //forms 1 -->
    </div>
    <!-- //content -->

</div>
<!-- main content end-->


</body>

</html>
  