<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOmgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('omgs', function (Blueprint $table) {
            

             $table->bigIncrements('id');

             $table->unsignedBigInteger('ren_type_id')->nullable();
             $table->foreign('ren_type_id')->references('id')->on('users');

             
             $table->text('policies');
             $table->string('Organogram');
             $table->string('Policy_Makers');
             $table->string('Recruitment_Regulations');
             $table->string('Promotion_Policy');
             $table->string('Annual_Appraisal');
             $table->string('Gratuity');
             $table->string('Provident_Fund');
             $table->string('Welfare_Fund');
             $table->string('Group_Insurance');
             $table->string('Pension_Benefits');
             $table->string('Permanent')->nullable();
             $table->string('Contractual')->nullable();
             $table->string('Outsourced')->nullable();
             $table->string('Turnover_Rate');
             $table->string('Filled_Position');
             $table->string('omg_aggregate_score');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('omgs');
    }
}
