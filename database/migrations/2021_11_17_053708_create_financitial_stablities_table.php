<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinancitialStablitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financitial_stablities', function (Blueprint $table) {
            

             $table->bigIncrements('id');

             $table->unsignedBigInteger('ren_type_id')->nullable();
             $table->foreign('ren_type_id')->references('id')->on('users');

            

             $table->string('financial_type');
             $table->string('FS_Revenue_Coverage');
             $table->string('FS_compliance_lts');
             $table->string('financial_constraint');
             $table->string('finance_stablity_Lifetime');
             $table->string('LTC');
             $table->string('highly_struggling');
             $table->string('financitial_stablity_parameter');
             $table->string('financitial_stablity_grading');
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financitial_stablities');
    }
}
