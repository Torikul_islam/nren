<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollaborationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collaborations', function (Blueprint $table) {
            

             $table->bigIncrements('id');

             $table->unsignedBigInteger('ren_type_id')->nullable();
             $table->foreign('ren_type_id')->references('id')->on('users');


             $table->string('collaboration_NREN');
             $table->string('collaboration_UR');
             $table->string('collaboration_vendor');
             $table->string('collaboration_policy_RM');
             $table->string('collaboration_policy_HEC');
             $table->string('aggregate_collaborate_score');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collaborations');
    }
}
