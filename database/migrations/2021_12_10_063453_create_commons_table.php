<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commons', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('ren_type_id')->nullable();
            $table->foreign('ren_type_id')->references('id')->on('users');


            $table->unsignedBigInteger('lifetime_id')->nullable();
            $table->foreign('lifetime_id')->references('id')->on('lifetimes');

            $table->unsignedBigInteger('coverage_id')->nullable();
            $table->foreign('coverage_id')->references('id')->on('coverages');

            $table->unsignedBigInteger('connected_institution_id')->nullable();
            $table->foreign('connected_institution_id')->references('id')->on('connected_institutions');

            $table->unsignedBigInteger('maximumBW_id')->nullable();
            $table->foreign('maximumBW_id')->references('id')->on('maximum_bws');

            $table->unsignedBigInteger('aggregate_UPBW_id')->nullable();
            $table->foreign('aggregate_UPBW_id')->references('id')->on('aggregate_up_bws');

            $table->unsignedBigInteger('ownership_id')->nullable();
            $table->foreign('ownership_id')->references('id')->on('ownerships');

            $table->unsignedBigInteger('omg_id')->nullable();
            $table->foreign('omg_id')->references('id')->on('omgs');

            $table->unsignedBigInteger('PV_id')->nullable();
            $table->foreign('PV_id')->references('id')->on('promotionandvisiblities');

            $table->unsignedBigInteger('collaboration_id')->nullable();
            $table->foreign('collaboration_id')->references('id')->on('collaborations');

            $table->unsignedBigInteger('FS_id')->nullable();
            $table->foreign('FS_id')->references('id')->on('financitial_stablities');

            $table->unsignedBigInteger('HR_id')->nullable();
            $table->foreign('HR_id')->references('id')->on('human_resouces');

            $table->unsignedBigInteger('HE_id')->nullable();
            $table->foreign('HE_id')->references('id')->on('human_experts');

            $table->unsignedBigInteger('SO_id')->nullable();
            $table->foreign('SO_id')->references('id')->on('offer_services');

            $table->text('Demerits');
            $table->string('maturity_level');
            $table->string('placement_tier');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commons');
    }
}
