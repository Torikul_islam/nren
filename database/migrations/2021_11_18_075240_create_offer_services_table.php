<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_services', function (Blueprint $table) {
            

             $table->bigIncrements('id');
             
             $table->unsignedBigInteger('ren_type_id')->nullable();
             $table->foreign('ren_type_id')->references('id')->on('users');

            

             $table->text('network_service_SO')->nullable(); 
             $table->text('identity_service_SO')->nullable(); 
             $table->text('security_service_SO')->nullable(); 
             $table->text('multimedia_service_SO')->nullable();
             $table->text('collaboration_service_SO')->nullable(); ;
             $table->text('storage_service_SO')->nullable(); 
             $table->text('professional_service_SO')->nullable(); 
             $table->string('offered_service_final_score');
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_services');
    }
}
