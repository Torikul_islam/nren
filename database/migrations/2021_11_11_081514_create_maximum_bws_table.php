<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaximumBwsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maximum_bws', function (Blueprint $table) {
            
             $table->bigIncrements('id');

             $table->unsignedBigInteger('ren_type_id')->nullable();
             $table->foreign('ren_type_id')->references('id')->on('users');


             $table->string('Maximum_BW_U');
             $table->string('Maximum_BW_U_score');
             $table->string('Maximum_BW_RI');
             $table->string('Maximum_BW_RI_score');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maximum_bws');
    }
}
