<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConnectedInstitutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('connected_institutions', function (Blueprint $table) {
             
             $table->bigIncrements('id');

             $table->unsignedBigInteger('ren_type_id')->nullable();
             $table->foreign('ren_type_id')->references('id')->on('users');


             $table->string('connected_institute_university')->nullable(); 
             $table->string('connected_institute_research')->nullable();
             $table->string('connected_institute_govt')->nullable();
             $table->string('connected_institute_school')->nullable();
             $table->string('connected_institute_other')->nullable(); 
             $table->string('total_institute_type');
             $table->string('connected_institute_typescore');
             $table->string('connected_institute_coverage');
             $table->string('connected_institute_coverage_score');
             

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('connected_institutions');
    }
}
