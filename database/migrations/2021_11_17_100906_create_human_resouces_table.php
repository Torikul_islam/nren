<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHumanResoucesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('human_resouces', function (Blueprint $table) {
            

             $table->bigIncrements('id');

             $table->unsignedBigInteger('ren_type_id')->nullable();
             $table->foreign('ren_type_id')->references('id')->on('users');


             
             $table->string('SystemOperationRandS');
             $table->string('SystemOperationTN');
             $table->string('SystemOperationCV');
             $table->string('SystemOperationCollaboration');
             $table->string('SystemOperationAS');
             $table->string('SystemOperationOther');
             $table->string('human_resouces_technical');
             $table->string('human_resouce_technical_final_score');
             $table->string('human_resouces_Nontechnical');
             $table->string('human_resouce_nontechnical_final_score');
            


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('human_resouces');
    }
}
