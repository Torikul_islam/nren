<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromotionandvisiblitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotionandvisiblities', function (Blueprint $table) {
            

            
             $table->bigIncrements('id');

             $table->unsignedBigInteger('ren_type_id')->nullable();
             $table->foreign('ren_type_id')->references('id')->on('users');

             
             $table->string('PV_effort_REN');
             $table->string('Awareness_programs');
             $table->string('budget_promotional');
             $table->string('Awareness_Programs_conducted');
             $table->string('Development_personnel');
             $table->string('PV_aggregate_score');
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotionandvisiblities');
    }
}
