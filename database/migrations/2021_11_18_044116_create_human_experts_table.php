<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHumanExpertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('human_experts', function (Blueprint $table) {
            

             $table->bigIncrements('id');

             $table->unsignedBigInteger('ren_type_id')->nullable();
             $table->foreign('ren_type_id')->references('id')->on('users');

             
             
             $table->string('human_expert_admin')->nullable(); 
             $table->string('human_intermediate_admin')->nullable(); 
             $table->string('human_beginner_admin')->nullable(); 
             $table->string('aggreagte_admin_score');
             $table->string('human_expert_tech')->nullable();
             $table->string('human_intermediate_tech')->nullable(); 
             $table->string('human_beginner_tech')->nullable(); 
             $table->string('aggreagte_tech_score');
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('human_experts');
    }
}
