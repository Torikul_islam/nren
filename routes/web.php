<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\User\HomeController;
use App\Http\Controllers\User\CommonController;
use App\Http\Controllers\AccessDeniedController;
use App\Http\Controllers\ChangePasswordController;
use App\Http\Controllers\Admin\User;

use App\Http\Controllers\Guest\guestHomeController;
use App\Http\Controllers\Guest\guestCalculateController;

use App\Http\Controllers\Auth\EmailVerificationController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});



Auth::routes();

Route::group([], __DIR__ . '/web/registration.php');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/access-denied', [AccessDeniedController::class, 'index']);
    Route::get('/change-password', [ChangePasswordController::class, 'index']);
    Route::post('/change-password', [ChangePasswordController::class, 'store'])->name('change.password');
});

Route::prefix('admin')->middleware('AdminAuth')->group(__DIR__ . '/web/admin.php');

// Route Group for users
Route::group(['prefix' => 'user', 'middleware' => 'UserAuth'], function () {

    Route::get('/form', [HomeController::class, 'create'])->name('form');

    Route::post('coverage/store', [CommonController::class, 'CoverageStore'])->name('coverage.store');

    Route::resource('mvalues', HomeController::class);

    Route::get('/maturity', [HomeController::class, 'maturity'])->name('maturity');
});


Route::get('guestform', [guestHomeController::class, 'index'])->name('guestform');
Route::post('calculate', [guestCalculateController::class, 'guestcalculate'])->name('calculate');
