<?php

Route::get('/dashboard', [App\Http\Controllers\Admin\DashboardController::class, 'index']);

Route::resource('users', App\Http\Controllers\Admin\UserController::class);

Route::resource('download', App\Http\Controllers\Admin\download::class);

Route::post('/toggleVerify', [App\Http\Controllers\Admin\UserController::class, 'ToggleVerify']);
