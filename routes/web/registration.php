<?php 


Route::get('/registration_message', [App\Http\Controllers\Auth\EmailVerificationController::class, 'success_register']);

Route::get('/verifyEmail', [App\Http\Controllers\Auth\EmailVerificationController::class, 'index']);

Route::get('/resendCode', [App\Http\Controllers\Auth\EmailVerificationController::class, 'ResendCode']);

Route::post('/verifyNow', [App\Http\Controllers\Auth\EmailVerificationController::class, 'verifyNow']);