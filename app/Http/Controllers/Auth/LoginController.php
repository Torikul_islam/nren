<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            $role = Auth::user()->role;
            if ($user->isVerified == true) {
                if ($role == "admin") {
                    return redirect('/admin/dashboard');
                } else if ($role == "user") {
                    return redirect('user/form');
                }else {
                    return redirect('/logout');
                } 
            } else {
                //Not varified Account, return to login view
                Auth::logout();
                request()->session()->flash('error_message', 'Your Account is Inactive Now!');
                // return view('auth.login');
                return back()->withInput();
            }
        } else {
            //return to login view
            request()->session()->flash('error_message', 'Wrong Credentials');
            // return view('auth.login');
            return back()->withInput();
        }
    }
}
