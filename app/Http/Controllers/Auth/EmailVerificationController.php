<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\SendEmailVerificationCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class EmailVerificationController extends Controller
{

     public function success_register()
    {
        return view("auth.success_registration_message");
    }
    
    public function index(Request $request)
    {
        $data['email']=$request->input('email');
        return view('auth.verify',$data);
    }

    public function verifyNow(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required',
            'email_verification_code' => 'required',
        ]);
        $email=$request->input('email');
        $email_verification_code=$request->input('email_verification_code');

        $user_count=DB::table('users')
                    ->where('email','=',$email)
                    ->where('email_verification_code','=',$email_verification_code)
                    ->where('isEmailVerified','=',false)
                    ->count();
        if($user_count==1){
            //veify user
            $update_user = DB::table('users')
                        ->where('email','=',$email)
                        ->update(['isEmailVerified' => true, 'email_verified_at' => date('Y-m-d H:i:s')]);
            $already_verifed=DB::table('users')
                            ->where('email','=',$email)
                            ->where('isVerified','=',true)
                            ->count();
            if($already_verifed==1){
                request()->session()->flash('success_message', 'Successfully verify your email address');
                return redirect("/login");
            }else{
                return redirect("/registration_message");
            }
            
        }else{
            request()->session()->flash('error_message', 'Failed to verify email address');
            return redirect("/login");
        }
    }
    public function ResendCode(Request $request)
    {
        $few_min_ago=date("Y-m-d h:i:s", strtotime("-10 minutes"));

        $email=$request->input('email');
        $user=DB::table('users')
                    ->where('email','=',$email)
                    ->where('isEmailVerified','=',false)
                    ->where('isVerified','=',false)
                    ->where('created_at','>=',$few_min_ago)
                    ->first();
        if($user != null)
        {
            $registred_email=$user->email;
            $email_verification_code=$user->email_verification_code;
            
            if (strpos(env("APP_URL"), 'localhost') == true) {
                Mail::to($registred_email)->send(new SendEmailVerificationCode($email_verification_code,$registred_email));
            }
            request()->session()->flash('success_message', 'Code resend successfully');
            return redirect("/verifyEmail?email=".$registred_email);
        }else{
            request()->session()->flash('error_message', 'Failed to resend code');
            return redirect("/login");
        }
    }
}
