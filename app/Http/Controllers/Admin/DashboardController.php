<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['total_registered_user'] = DB::table('users')->count();
        $data['total_verified_user'] = DB::table('users')->where('isVerified', '=', true)->count();
        $data['total_pending_user'] = DB::table('users')->where('isVerified', '=', false)->count();
        $data['total_email_verified_user'] = DB::table('users')->where('isEmailVerified', '=', true)->count();

        return view('Admin.home', $data);
    }
}
