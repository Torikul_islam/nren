<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Mail\SendVerificationEmailToUser;
use Illuminate\Http\Request;
use Response;
use Hash;
use DB;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{

    /**
     * Display a listing of the User.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $data = User::where('isVerified', 0)
            ->orderBy('created_at', 'desc')
            ->get();

        return view('Admin.users.index', compact('data'));
    }


    /**
     * Display the specified User.
     *
     * @param int $id
     *
     * @return Response
     */

    public function ToggleVerify(Request $request)
    {
        $id = $request->input("user_id");


        $user = DB::table('users')->find($id);

        if ($user->isVerified == false) {
            $updatedate = User::where('id', '=', $user->id)
                ->update([
                    'isVerified'  => true
                ]);

            if (strpos(env("APP_URL"), 'localhost') == true) {
                Mail::to($user->email)->send(new SendVerificationEmailToUser());
            }
        }
        return back();
    }
}
