<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class AccessDeniedController extends Controller
{
    public function __construct()
    {
    $this->middleware('auth');
    }

    public function index()
    {
        if(Auth::user()->role=="admin"){
           $string="admin.layouts.app";
        }else if(Auth::user()->role=="user"){
            $string="User.layouts.app";
        }else if( Auth::user()->role=="guest"){
            $string="Guest.guestlayout.app";
        }
    $data["layout_string"]=$string;
    return view('accessDenied',$data);
    }
}
