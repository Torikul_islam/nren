<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\coverage;
use App\Models\common;
use App\Models\lifetime;
use App\Models\connected_institution;
use App\Models\maximum_bw;
use App\Models\aggregate_up_bw;
use App\Models\ownership;
use App\Models\omg;
use App\Models\promotionandvisiblity;
use App\Models\collaboration;
use App\Models\financitial_stablity;
use App\Models\human_resouces;
use App\Models\human_expert;
use App\Models\offer_service;
use App\Models\all_ren;
use Illuminate\Support\Str;
use App\Mail\sendUserScore;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use PDF;
use Response;
class CommonController extends Controller
{
    public function CoverageStore(Request $request)
    {

         try{
          $request->validate([
                'ren_type_id'                          => 'required',
                'coverage_total_university'            => 'required|numeric',
                'coverage_university'                  => 'required|numeric',
                'Lifetime'                             => 'required',
                'connected_institute_university'       => 'nullable | numeric',
                'connected_institute_research'         => 'nullable | numeric',
                'connected_institute_govt'             => 'nullable | numeric',
                'connected_institute_school'           => 'nullable | numeric',
                'connected_institute_other'            => 'nullable | numeric',
                'Maximum_BW_U'                         => 'required',
                'Maximum_BW_RI'                        => 'required',
                'Commodity'                            => 'required|numeric',
                'Res_Edu'                              => 'required|numeric',
                'RandS'                                => 'required',
                'Trans_Network'                        => 'required',
                'CandV'                                => 'required',
                'VideoColla'                           => 'required',
                'BackboneFibre'                        => 'required',
                'LMF'                                  => 'required',
                'CivilPI'                              => 'required',
                'Organogram'                           => 'required',
                'Policy_Makers'                        => 'required',
                'Recruitment_Regulations'              => 'required',
                'Promotion_Policy'                     => 'required',
                'Annual_Appraisal'                     => 'required',
                'Gratuity'                             => 'required',
                'Provident_Fund'                       => 'required',
                'Welfare_Fund'                         => 'required',
                'Group_Insurance'                      => 'required',
                'Pension_Benefits'                     => 'required',
                'Permanent'                            => 'nullable|numeric',
                'Contractual'                          => 'nullable|numeric',
                'Outsourced'                           => 'nullable|numeric',
                'Turnover_Rate'                        => 'required|numeric',
                'Filled_Position'                      => 'required',
                'PV_effort_REN'                        => 'required',
                'Awareness_programs'                   => 'numeric',
                'budget_promotional'                   => 'required',
                'Awareness_Programs_conducted'         => 'numeric',
                'Development_personnel'                => 'required',
                'collaboration_NREN'                   => 'required',
                'collaboration_UR'                     => 'required',
                'collaboration_vendor'                 => 'required',
                'collaboration_policy_RM'              => 'required',
                'collaboration_policy_HEC'             => 'required',
                'financial_type'                       => 'required',
                'FS_Revenue_Coverage'                  => 'required',
                'FS_compliance_lts'                    => 'required',
                'financial_constraint'                 => 'required',
                'LTC'                                  => 'required',
                'Highly_Struggling'                    => 'required',
                'SystemOperationRandS'                 => 'required',
                'SystemOperationTN'                    => 'required',
                'SystemOperationCV'                    => 'required',
                'SystemOperationCollaboration'         => 'required',
                'SystemOperationAS'                    => 'required',
                'SystemOperationOther'                 => 'required',
                'human_resouces_technical'             => 'nullable|numeric',
                'human_resouces_Nontechnical'          => 'nullable|numeric',
                'human_expert_admin'                   => 'nullable |numeric',
                'human_intermediate_admin'             => 'nullable |numeric',
                'human_beginner_admin'                 => 'nullable |numeric',
                'human_expert_tech'                    => 'nullable |numeric',
                'human_intermediate_tech'              => 'nullable |numeric',
                'human_beginner_tech'                  => 'nullable |numeric',

            ]);

          $ren_type_id = $request->get('ren_type_id');

          $Lifetime = $request->get('Lifetime');
          $data["Lifetime"]=$Lifetime;


          //finalcitial stablity start here

          $financial_type = $request->get('financial_type');
          $data["financial_type"]=$financial_type;
          $FS_Revenue_Coverage = $request->get('FS_Revenue_Coverage');
          $data["FS_Revenue_Coverage"]=$FS_Revenue_Coverage;
          $FS_compliance_lts = $request->get('FS_compliance_lts');
          $data["FS_compliance_lts"]=$FS_compliance_lts;

          $financial_constraint = $request->get('financial_constraint');
          $data["financial_constraint"]=$financial_constraint;
          $LTC = $request->get('LTC');
          $data["LTC"]=$LTC;


          $FS_compliance_lts_status = 0;
          if ($FS_compliance_lts =='YES') {
            $FS_compliance_lts_status = 'YES';
          }
          if ($FS_compliance_lts =='Not Applicable') {
            $FS_compliance_lts_status = 'NO';
          }

          $both_OPex_and_CAPex ='YES';
          $only_OPex ='YES';
          $only_CAPex ='YES';
          $Neither_OPex_nor_CAPex ='YES';
          $Full_OPEX_and_Part_of_CAPEX = 'YES';
          $Full_CAPEX_and_Part_of_OPEX = 'YES';

          if ($FS_Revenue_Coverage =='Full CAPEX and Full OPEX') {

            $both_OPex_and_CAPex ='YES';
            $only_OPex ='NO';
            $only_CAPex ='NO';
            $Neither_OPex_nor_CAPex ='NO';
            $Full_OPEX_and_Part_of_CAPEX = 'NO';
            $Full_CAPEX_and_Part_of_OPEX = 'NO';
          }
          if ($FS_Revenue_Coverage =='Full OPEX and Part of CAPEX') {
             $Full_OPEX_and_Part_of_CAPEX = 'YES';
              $both_OPex_and_CAPex ='NO';
              $only_OPex ='NO';
              $only_CAPex ='NO';
              $Neither_OPex_nor_CAPex ='NO';
              $Full_CAPEX_and_Part_of_OPEX = 'NO';
          }
          if ($FS_Revenue_Coverage =='Full CAPEX and Part of OPEX') {
             $Full_CAPEX_and_Part_of_OPEX = 'YES';
              $Full_OPEX_and_Part_of_CAPEX = 'NO';
              $both_OPex_and_CAPex ='NO';
              $only_OPex ='NO';
              $only_CAPex ='NO';
              $Neither_OPex_nor_CAPex ='NO';
          }
          if ($FS_Revenue_Coverage =='Only OPex') {

            $only_OPex  ='YES';
              $only_CAPex ='NO';
              $Neither_OPex_nor_CAPex ='NO';
              $Full_CAPEX_and_Part_of_OPEX = 'NO';
              $Full_OPEX_and_Part_of_CAPEX = 'NO';
              $both_OPex_and_CAPex ='NO';
          }
          if ($FS_Revenue_Coverage =='Only CAPex') {

            $only_CAPex ='YES';
              $only_OPex  ='NO';
              $Neither_OPex_nor_CAPex ='NO';
              $Full_CAPEX_and_Part_of_OPEX = 'NO';
              $Full_OPEX_and_Part_of_CAPEX = 'NO';
              $both_OPex_and_CAPex ='NO';
          }
          if ($FS_Revenue_Coverage =='Neither CAPEX nor OPEX') {

            $Neither_OPex_nor_CAPex ='YES';
              $Full_CAPEX_and_Part_of_OPEX = 'NO';
              $Full_OPEX_and_Part_of_CAPEX = 'NO';
              $both_OPex_and_CAPex ='NO';
              $only_CAPex ='NO';
              $only_OPex  ='NO';

          }
           if ($FS_Revenue_Coverage =='Not Applicable') {

              $Neither_OPex_nor_CAPex ='NO';
              $Full_CAPEX_and_Part_of_OPEX = 'NO';
              $Full_OPEX_and_Part_of_CAPEX = 'NO';
              $both_OPex_and_CAPex ='NO';
              $only_CAPex ='NO';
              $only_OPex  ='NO';
          }


          $financitial_stablity_parameter = 0;

          $highly_struggling = $request->get('Highly_Struggling');
          $data["highly_struggling"]=$highly_struggling;


          if ($financial_type =='Fully Self-financed') {

            if ($both_OPex_and_CAPex == 'YES') {
             $financitial_stablity_parameter = 'Highly Stable';
            }else{

              if (($only_CAPex =='YES' || $only_OPex =='YES') && $FS_compliance_lts_status =='YES') {
                $financitial_stablity_parameter = 'Stable';
              }
               if (($only_CAPex =='YES' || $only_OPex =='YES') && $FS_compliance_lts_status =='NO') {
                $financitial_stablity_parameter = 'Partially Stable';
              }
              if ($Neither_OPex_nor_CAPex =='YES' && $FS_compliance_lts_status =='YES') {
                $financitial_stablity_parameter = 'Partially Stable';
              }
              if ($Neither_OPex_nor_CAPex =='YES' && $FS_compliance_lts_status =='NO') {
                $financitial_stablity_parameter = 'Unstable';
              }
            }
          }


          $Withdrawal_of_Finacne = 'NO';
          if ($financial_type =='Hybrid Financing') {
            if ($both_OPex_and_CAPex == 'YES') {
             $financitial_stablity_parameter = 'Highly Stable';
            }else{


              

              if ($Full_OPEX_and_Part_of_CAPEX == 'YES' || $Full_CAPEX_and_Part_of_OPEX == 'YES') {
                
                 if ($Withdrawal_of_Finacne == 'YES') {
                    $financitial_stablity_parameter = 'Partially Stable';
                  }else{
                     if ($FS_compliance_lts_status =='YES') {
                      $financitial_stablity_parameter = 'Stable';
                    }else{
                       $financitial_stablity_parameter = 'Partially Stable';
                    }
                  }
              }else{

                if ($Neither_OPex_nor_CAPex =='YES') {
                  if ($Withdrawal_of_Finacne == 'YES') {
                     $financitial_stablity_parameter = 'Unstable';
                  }else{
                    $financitial_stablity_parameter = 'Partially Stable';
                  }
                }else{
                  $financitial_stablity_parameter = 'Unstable';
                }

              }         
            }
          }

  

          $finance_stablity_Lifetime_score = 0;
          if ($Lifetime == '1-3 Years') {
            $finance_stablity_Lifetime_score ='<3';
          }
          if ($Lifetime == '4-5 Years') {
            $finance_stablity_Lifetime_score ='3-10';
          }
           if ($Lifetime == '> 10 Years') {
            $finance_stablity_Lifetime_score ='>10';
          }
          if ($Lifetime == '6-10 Years') {
            $finance_stablity_Lifetime_score ='3-10';
          }


          
          if ($financial_type =='Fully Government Financed') {
                      
            if ($financial_constraint =='YES') {

            if ($finance_stablity_Lifetime_score == '<3') {
              $financitial_stablity_parameter = 'Unstable';
            }

            if ($finance_stablity_Lifetime_score == '3-10' && $highly_struggling == 'NO') {
             $financitial_stablity_parameter = 'Partially Stable';
            }

             if ($finance_stablity_Lifetime_score == '3-10' && $highly_struggling == 'YES') {
             $financitial_stablity_parameter = 'Unstable';
            }
             if ($finance_stablity_Lifetime_score == '>10' && $highly_struggling == 'NO') {
             $financitial_stablity_parameter = 'Stable';
            }
             if ($finance_stablity_Lifetime_score == '>10' && $highly_struggling == 'YES') {
             $financitial_stablity_parameter = 'Partially Stable';
            }
            
          }
          else{

            if ($LTC =='YES') {
              $financitial_stablity_parameter = 'Highly Stable';
            }else{

              if ($finance_stablity_Lifetime_score == '<3') {
              $financitial_stablity_parameter = 'Partially Stable';
              }
              if ($finance_stablity_Lifetime_score == '3-10') {
               $financitial_stablity_parameter = 'Stable';
              }

              if ($finance_stablity_Lifetime_score == '>10') {
               $financitial_stablity_parameter = 'Highly Stable';
              }

            }

          }

          }



          $financitial_stablity_grading = 0;
          if ($financitial_stablity_parameter == 'Unstable') {
           $financitial_stablity_grading = 0;
          }
          if ($financitial_stablity_parameter == 'Partially Stable') {
           $financitial_stablity_grading = 3;
          }
          if ($financitial_stablity_parameter == 'Stable') {
           $financitial_stablity_grading = 6;
          }
          if ($financitial_stablity_parameter == 'Highly Stable') {
           $financitial_stablity_grading = 10;
          }




          $financitial_stablity_table = financitial_stablity::updateOrCreate(
                ['ren_type_id'                              =>  $ren_type_id],
                ['financial_type'                          =>  $financial_type,
                'FS_Revenue_Coverage'                      =>  $FS_Revenue_Coverage,
                'FS_compliance_lts'                        =>  $FS_compliance_lts,
                'financial_constraint'                     =>  $financial_constraint,
                'finance_stablity_Lifetime'                =>  $Lifetime,
                'LTC'                                      =>  $LTC,
                'highly_struggling'                        =>  $highly_struggling,
                'financitial_stablity_parameter'           =>  $financitial_stablity_parameter,
                'financitial_stablity_grading'             =>  $financitial_stablity_grading],
            );

          //finalcitial stablity end here

               


//offered services start here 

          $network_service_SO = $request->get('network_service_SO');
          $identity_service_SO = $request->get('identity_service_SO');
          $security_service_SO = $request->get('security_service_SO');
          $multimedia_service_SO = $request->get('multimedia_service_SO');
          $collaboration_service_SO = $request->get('collaboration_service_SO');
          $storage_service_SO = $request->get('storage_service_SO');
          $professional_service_SO = $request->get('professional_service_SO');

          $data["network_service_SO"]=$network_service_SO;
          $data["identity_service_SO"]=$identity_service_SO;
          $data["security_service_SO"]=$security_service_SO;
          $data["multimedia_service_SO"]=$multimedia_service_SO;
          $data["collaboration_service_SO"]=$collaboration_service_SO;
          $data["storage_service_SO"]=$storage_service_SO;
          $data["professional_service_SO"]=$professional_service_SO;


          $network_service_SO_value =0;
          if ($network_service_SO === null) {
             $network_service_SO_value =0;
          }else{
            $network_service_SO_value = count($network_service_SO);
          }

          $identity_service_SO_value =0;
          if ($identity_service_SO === null) {
             $identity_service_SO_value =0;
          }else{
            $identity_service_SO_value = count($identity_service_SO);
          }

          $security_service_SO_value =0;
          if ($security_service_SO === null) {
             $security_service_SO_value =0;
          }else{
            $security_service_SO_value = count($security_service_SO);
          }

          $multimedia_service_SO_value =0;
          if ($multimedia_service_SO === null) {
             $multimedia_service_SO_value =0;
          }else{
            $multimedia_service_SO_value = count($multimedia_service_SO);
          }

          $collaboration_service_SO_value =0;
          if ($collaboration_service_SO === null) {
             $collaboration_service_SO_value =0;
          }else{
            $collaboration_service_SO_value = count($collaboration_service_SO);
          }



          $storage_service_SO_value =0;
          if ($storage_service_SO === null) {
             $storage_service_SO_value =0;
          }else{
            $storage_service_SO_value = count($storage_service_SO);
          }

          $professional_service_SO_value =0;
          if ($professional_service_SO === null) {
             $professional_service_SO_value =0;
          }else{
            $professional_service_SO_value = count($professional_service_SO);
          }

        
        $total_offered_services = $network_service_SO_value + $identity_service_SO_value + $security_service_SO_value + $multimedia_service_SO_value + $collaboration_service_SO_value + $storage_service_SO_value + $professional_service_SO_value;


        
        $offered_service_final_score = ($total_offered_services/710)*100;


          $offered_service_all_table = offer_service::updateOrCreate(
                ['ren_type_id'                              =>  $ren_type_id],
                ['network_service_SO'                       =>  json_encode($network_service_SO),
                'identity_service_SO'                      =>  json_encode($identity_service_SO),
                'security_service_SO'                      =>  json_encode($security_service_SO),
                'multimedia_service_SO'                    =>  json_encode($multimedia_service_SO),
                'collaboration_service_SO'                 =>  json_encode($collaboration_service_SO),
                'storage_service_SO'                       =>  json_encode($storage_service_SO),
                'professional_service_SO'                  =>  json_encode($professional_service_SO),
                'offered_service_final_score'              =>  round($offered_service_final_score,2)],
            );


//offered services end here 

//HUman epertise start here


          $human_expert_admin = $request->get('human_expert_admin');
          $human_intermediate_admin = $request->get('human_intermediate_admin');
          $human_beginner_admin = $request->get('human_beginner_admin');

          $data["human_expert_admin"]=$human_expert_admin;
          $data["human_intermediate_admin"]=$human_intermediate_admin;
          $data["human_beginner_admin"]=$human_beginner_admin;

          $human_expert_admin_decvalue       =  $human_expert_admin / 100.00;
          $human_intermediate_admin_decvalue =  $human_intermediate_admin / 100.00;
          $human_beginner_admin_decvalue     =  $human_beginner_admin / 100.00;

          $aggreagte_admin_score = (($human_expert_admin_decvalue*10) + ($human_intermediate_admin_decvalue*7.5) + ($human_beginner_admin_decvalue*5));

          

          $human_expert_tech = $request->get('human_expert_tech');
          $human_intermediate_tech = $request->get('human_intermediate_tech');
          $human_beginner_tech = $request->get('human_beginner_tech');

          $data["human_expert_tech"]=$human_expert_tech;
          $data["human_intermediate_tech"]=$human_intermediate_tech;
          $data["human_beginner_tech"]=$human_beginner_tech;


          $human_expert_tech_decvalue       =  $human_expert_tech / 100.00;
          $human_intermediate_tech_decvalue =  $human_intermediate_tech / 100.00;
          $human_beginner_tech_decvalue     =  $human_beginner_tech / 100.00;

          $aggreagte_tech_score = (($human_expert_tech_decvalue*10) + ($human_intermediate_tech_decvalue*7.5) + ($human_beginner_tech_decvalue*5));


          $human_expert_all_table = human_expert::updateOrCreate(
                ['ren_type_id'                                  =>  $ren_type_id],
                ['human_expert_admin'                           =>  $human_expert_admin,
                'human_intermediate_admin'                     =>  $human_intermediate_admin,
                'human_beginner_admin'                         =>  $human_beginner_admin,
                'aggreagte_admin_score'                        =>  round($aggreagte_admin_score,2),
                'human_expert_tech'                            =>  $human_expert_tech,
                'human_intermediate_tech'                      =>  $human_intermediate_tech,
                'human_beginner_tech'                          =>  $human_beginner_tech,
                'aggreagte_tech_score'                         =>  round($aggreagte_tech_score,2)],
            );

//HUman epertise end here 



          //collaboration start here

              $collaboration_NREN = $request->get('collaboration_NREN');
              $collaboration_UR = $request->get('collaboration_UR');
              $collaboration_vendor = $request->get('collaboration_vendor');
              $collaboration_policy_RM = $request->get('collaboration_policy_RM');
              $collaboration_policy_HEC = $request->get('collaboration_policy_HEC');


              $data["collaboration_NREN"]=$collaboration_NREN;
              $data["collaboration_UR"]=$collaboration_UR;
              $data["collaboration_vendor"]=$collaboration_vendor;
              $data["collaboration_policy_RM"]=$collaboration_policy_RM;
              $data["collaboration_policy_HEC"]=$collaboration_policy_HEC;

              $collaboration_NREN_score =0;
              if ($collaboration_NREN =='Totally Unsuccessful') {
                $collaboration_NREN_score =0;
              }
              if ($collaboration_NREN =='Unsuccessful') {
                $collaboration_NREN_score =1;
              }
               if ($collaboration_NREN =='Successful') {
                $collaboration_NREN_score =3;
              }
               if ($collaboration_NREN =='Very Successful') {
                $collaboration_NREN_score =4;
              }
               if ($collaboration_NREN =='Neutral') {
                $collaboration_NREN_score =2;
              }


              $collaboration_UR_score =0;
              if ($collaboration_UR =='Totally Unsuccessful') {
                $collaboration_UR_score =0;
              }
              if ($collaboration_UR =='Unsuccessful') {
                $collaboration_UR_score =1;
              }
               if ($collaboration_UR =='Successful') {
                $collaboration_UR_score =3;
              }
               if ($collaboration_UR =='Very Successful') {
                $collaboration_UR_score =4;
              }
               if ($collaboration_UR =='Neutral') {
                $collaboration_UR_score =2;
              }


              $collaboration_vendor_score =0;
              if ($collaboration_vendor =='Totally Unsuccessful') {
                $collaboration_vendor_score =0;
              }
              if ($collaboration_vendor =='Unsuccessful') {
                $collaboration_vendor_score =1;
              }
               if ($collaboration_vendor =='Successful') {
                $collaboration_vendor_score =3;
              }
               if ($collaboration_vendor =='Very Successful') {
                $collaboration_vendor_score =4;
              }
               if ($collaboration_vendor =='Neutral') {
                $collaboration_vendor_score =2;
              }


              $collaboration_policy_RM_score =0;
              if ($collaboration_policy_RM =='Totally Unsuccessful') {
                $collaboration_policy_RM_score =0;
              }
              if ($collaboration_policy_RM =='Unsuccessful') {
                $collaboration_policy_RM_score =1;
              }
               if ($collaboration_policy_RM =='Successful') {
                $collaboration_policy_RM_score =3;
              }
               if ($collaboration_policy_RM =='Very Successful') {
                $collaboration_policy_RM_score =4;
              }
               if ($collaboration_policy_RM =='Neutral') {
                $collaboration_policy_RM_score =2;
              }

              $collaboration_policy_HEC_score =0;
              if ($collaboration_policy_HEC =='Totally Unsuccessful') {
                $collaboration_policy_HEC_score =0;
              }
              if ($collaboration_policy_HEC =='Unsuccessful') {
                $collaboration_policy_HEC_score =1;
              }
               if ($collaboration_policy_HEC =='Successful') {
                $collaboration_policy_HEC_score =3;
              }
               if ($collaboration_policy_HEC =='Very Successful') {
                $collaboration_policy_HEC_score =4;
              }
               if ($collaboration_policy_HEC =='Neutral') {
                $collaboration_policy_HEC_score =2;
              }



              $aggregate_collaborate_score = (($collaboration_policy_HEC_score +  $collaboration_policy_RM_score + $collaboration_vendor_score + $collaboration_UR_score + $collaboration_NREN_score ) *10)/20;


          $collaboration_table = collaboration::updateOrCreate(
                ['ren_type_id'                                  =>  $ren_type_id],
                ['collaboration_NREN'                           =>  $collaboration_NREN,
                'collaboration_UR'                             =>  $collaboration_UR,
                'collaboration_vendor'                         =>  $collaboration_vendor,
                'collaboration_policy_RM'                      =>  $collaboration_policy_RM,
                'collaboration_policy_HEC'                     =>  $collaboration_policy_HEC,
                'aggregate_collaborate_score'                  =>  $aggregate_collaborate_score],
            );


 //collaboration end  here




//promotion and visiblity start here 
           $PV_effort_REN = $request->get('PV_effort_REN');
           $Awareness_programs = $request->get('Awareness_programs');
           $budget_promotional = $request->get('budget_promotional');
           $Awareness_Programs_conducted = $request->get('Awareness_Programs_conducted');
           $Development_personnel = $request->get('Development_personnel');

           $data["PV_effort_REN"]=$PV_effort_REN;
           $data["Awareness_programs"]=$Awareness_programs;
           $data["budget_promotional"]=$budget_promotional;
           $data["Awareness_Programs_conducted"]=$Awareness_Programs_conducted;
           $data["Development_personnel"]=$Development_personnel;

           if ($PV_effort_REN == 'YES') {
             
             $Awareness_programs_score = 0;
             if ($Awareness_programs == 0) {
               $vvv = 0;
             }
             if ($Awareness_programs == 1) {
              $vvv = 5;
             }
             if ($Awareness_programs == 2) {
              $vvv = 6;
             }
             if ($Awareness_programs == 3) {
              $vvv = 7;
             }
             if ($Awareness_programs > 3) {
              $vvv = 10;
             }
           }else{
            $Awareness_programs_score = 'NO';
            $vvv = 0;
           }


           $Awareness_programs_final_score = ($vvv * 0.25);

         

            $budget_promotional_score = 0;
           if ($budget_promotional == '0%') {
              $budget_promotional_score = 0;
           }
           if ($budget_promotional  =='>0%') {
             $budget_promotional_score = 4;
           }
           if ($budget_promotional =='>1%') {
             $budget_promotional_score = 6;
           }
           if ($budget_promotional =='>5%') {
             $budget_promotional_score = 8;
           }
           if($budget_promotional =='>10%'){
            $budget_promotional_score = 10;
           }

           $budget_promotional_final_score = ($budget_promotional_score * 0.2);





            $Awareness_Programs_conducted_score = 0;
           if ($Awareness_Programs_conducted <=5) {
             $Awareness_Programs_conducted_score = 0;
           }
           if ($Awareness_Programs_conducted >5 && $Awareness_Programs_conducted <=10) {
             $Awareness_Programs_conducted_score = 6;
           }
           if ($Awareness_Programs_conducted >10 && $Awareness_Programs_conducted <=20) {
             $Awareness_Programs_conducted_score = 8;
           }
           if($Awareness_Programs_conducted >20){
            $Awareness_Programs_conducted_score = 10;
           }


           $Awareness_Programs_conducted_final_score = ($Awareness_Programs_conducted_score * 0.25);



          $Development_personnel_score =0;
          if ($Development_personnel =='YES') {
            $Development_personnel_score =10;
          }else{
             $Development_personnel_score =0;
          }


          $Development_personnel_final_score = ($Development_personnel_score * 0.3);


           //final aggregate score hobe 
           $PV_aggregate_score = 0;
           if ( $Awareness_programs_score === 'NO') {
             $PV_aggregate_score = (($budget_promotional_final_score + $Awareness_Programs_conducted_final_score + $Development_personnel_final_score)/ 0.75);
           }else{


            $PV_aggregate_score = ($budget_promotional_final_score + $Awareness_Programs_conducted_final_score + $Awareness_programs_final_score + $Development_personnel_final_score);

           }

            $PromotionVisiblity_table = promotionandvisiblity::updateOrCreate(
                ['ren_type_id'                      =>  $ren_type_id],
                ['PV_effort_REN'                    =>  $PV_effort_REN,
                'Awareness_programs'               =>  $Awareness_programs,
                'budget_promotional'               =>  $budget_promotional,
                'Awareness_Programs_conducted'     =>  $Awareness_Programs_conducted,
                'Development_personnel'            =>  $Development_personnel,
                'PV_aggregate_score'               =>  $PV_aggregate_score],
            );

     

//promotion and visiblity end  here 


          $policies = $request->get('policies');
          $data["policies"]=$policies;
          
          if ($policies === null) {
           $p = 0;
          }else{
            $p = count($policies);
          }

          $policies_score = (($p/90)*100)*0.15;

         

          $Organogram               = $request->get('Organogram');
          $Policy_Makers            = $request->get('Policy_Makers');
          $Recruitment_Regulations  = $request->get('Recruitment_Regulations');
          $Promotion_Policy         = $request->get('Promotion_Policy');
          $Annual_Appraisal         = $request->get('Annual_Appraisal');
          $Gratuity                 = $request->get('Gratuity');
          $Provident_Fund           = $request->get('Provident_Fund');
          $Welfare_Fund             = $request->get('Welfare_Fund');
          $Group_Insurance          = $request->get('Group_Insurance');
          $Pension_Benefits          = $request->get('Pension_Benefits');

          $data["Organogram"]=$Organogram;
          $data["Policy_Makers"]=$Policy_Makers;
          $data["Recruitment_Regulations"]=$Recruitment_Regulations;
          $data["Promotion_Policy"]=$Promotion_Policy;
          $data["Annual_Appraisal"]=$Annual_Appraisal;
          $data["Provident_Fund"]=$Provident_Fund;
          $data["Welfare_Fund"]=$Welfare_Fund;
          $data["Group_Insurance"]=$Group_Insurance;
          $data["Pension_Benefits"]=$Pension_Benefits;
          $data["Gratuity"]=$Gratuity;

          $Organogram_V=0;
          if ($Organogram =='YES') {
            $Organogram_V=10;
          }else{
            $Organogram_V=0;
          }

          $Policy_Makers_V=0;
          if ($Policy_Makers =='YES') {
            $Policy_Makers_V=10;
          }else{
            $Policy_Makers_V=0;
          }

          $Recruitment_Regulations_V=0;
          if ($Recruitment_Regulations =='YES') {
            $Recruitment_Regulations_V=3;
          }else{
            $Recruitment_Regulations_V=0;
          }


          $Promotion_Policy_V=0;
          if ($Promotion_Policy =='YES') {
            $Promotion_Policy_V=3;
          }else{
            $Promotion_Policy_V=0;
          }

          $Annual_Appraisal_V=0;
          if ($Annual_Appraisal =='YES') {
            $Annual_Appraisal_V=5;
          }else{
            $Annual_Appraisal_V=0;
          }

          $Gratuity_V=0;
          if ($Gratuity =='YES') {
            $Gratuity_V=5;
          }else{
            $Gratuity_V=0;
          }

          $Provident_Fund_V=0;
          if ($Provident_Fund =='YES') {
            $Provident_Fund_V=5;
          }else{
            $Provident_Fund_V=0;
          }

          $Welfare_Fund_V=0;
          if ($Welfare_Fund =='YES') {
            $Welfare_Fund_V=1;
          }else{
            $Welfare_Fund_V=0;
          }

          $Group_Insurance_V=0;
          if ($Group_Insurance =='YES') {
            $Group_Insurance_V=1;
          }else{
            $Group_Insurance_V=0;
          }

          $Pension_Benefits_V=0;
          if ($Pension_Benefits =='YES') {
            $Pension_Benefits_V=10;
          }else{
            $Pension_Benefits_V=0;
          }


          $HR_Policy_Score = (((($Organogram_V + $Policy_Makers_V + $Recruitment_Regulations_V + $Promotion_Policy_V + $Annual_Appraisal_V + $Gratuity_V + $Provident_Fund_V + $Welfare_Fund_V + $Group_Insurance_V + $Pension_Benefits_V ) /(53*10))*100)*0.20);


         

           $Permanent    = $request->get('Permanent');
           $Contractual  = $request->get('Contractual');
           $Outsourced   = $request->get('Outsourced');

           $data["Permanent"]=$Permanent;
           $data["Contractual"]=$Contractual;
           $data["Outsourced"]=$Outsourced;

           $ratioPCO = 0;
           if (($Contractual+$Outsourced)>0) {
             $ratioPCO = $Permanent /($Contractual+$Outsourced);
           }else{
            $ratioPCO = $Permanent;
           }

           

           $structure_status = 0;
           if ($ratioPCO <=1) {
              $structure_status = 4;
           }elseif ($ratioPCO >1 && $ratioPCO <=5) {
             $structure_status = 7;
           }else{
            $structure_status = 10;
           }


           $structure_status_score = $structure_status * 0.2;



          $Turnover_Rate = $request->get('Turnover_Rate');
          $data["Turnover_Rate"]=$Turnover_Rate;

          $Turnover_Rate_score = 0;
           if ($Turnover_Rate < 5) {
              $Turnover_Rate_score = 10;
           }elseif ($Turnover_Rate >= 5 && $Turnover_Rate < 20) {
             $Turnover_Rate_score = 7;
           }else{
            $Turnover_Rate_score = 5;
           }

          $Turnover_Rate_final_score = $Turnover_Rate_score * 0.3;

          $Filled_Position = $request->get('Filled_Position');
          $data["Filled_Position"]=$Filled_Position;


          $Filled_Position_score = 0;
           if ($Filled_Position == '>80%') {
              $Filled_Position_score = 10;
           }elseif ($Filled_Position == '50%-80%') {
             $Filled_Position_score = 8;
           }elseif($Filled_Position == '<50%' ){
            $Filled_Position_score = 5;
           }
           else{
            $Filled_Position_score = 0;
           }


          $Filled_Position_final_score = $Filled_Position_score * 0.15;


          $omg_aggregate_score = $Filled_Position_final_score + $Turnover_Rate_final_score +  $structure_status_score + $HR_Policy_Score + $policies_score;



            $omg_data_table = omg::updateOrCreate(
                ['ren_type_id'               =>  $ren_type_id],
                ['policies'                  =>  json_encode($policies),
                'Organogram'                =>  $Organogram,
                'Policy_Makers'             =>  $Policy_Makers,
                'Recruitment_Regulations'   =>  $Recruitment_Regulations,
                'Promotion_Policy'          =>  $Promotion_Policy,
                'Annual_Appraisal'          =>  $Annual_Appraisal,
                'Gratuity'                  =>  $Gratuity,
                'Provident_Fund'            =>  $Provident_Fund,
                'Welfare_Fund'              =>  $Welfare_Fund,
                'Group_Insurance'           =>  $Group_Insurance,
                'Pension_Benefits'          =>  $Pension_Benefits,
                'Permanent'                 =>  $Permanent,
                'Contractual'               =>  $Contractual,
                'Outsourced'                =>  $Outsourced,
                'Turnover_Rate'             =>  $Turnover_Rate,
                'Filled_Position'           =>  $Filled_Position,
                'omg_aggregate_score'       =>  round($omg_aggregate_score,2)],
            );

            //Ei table a policy add korte hobe first a 



           //connected institutions samne ansi karon value poya lagbe 
         


          $coverage_total_university = $request->get('coverage_total_university');
          $coverage_university = $request->get('coverage_university');

          $data["coverage_total_university"]=$coverage_total_university;
          $data["coverage_university"]=$coverage_university;

          $score = ($coverage_university/$coverage_total_university)*10;


            $C_table_data = coverage::updateOrCreate(
                ['ren_type_id'                            =>  $ren_type_id],
                ['coverage_total_university'              =>  $coverage_total_university,
                'coverage_connected_university'          =>  $coverage_university,
                'score'                                  =>  round($score,2)],
            );

               //Lifetime calculation start here

               //lifetime upor a ase financitial stablity calculation er jonno

               $lifetimeValue = 0;
               if($Lifetime =='1-3 Years'){
                   $lifetimeValue = 2;
               }
               elseif ($Lifetime =='4-5 Years') {
                   $lifetimeValue = 4.5;
               }
               elseif ($Lifetime =='6-10 Years') {
                   $lifetimeValue = 8;
               }
               else{
                $lifetimeValue = 10;
            }


             $L_table_data = lifetime::updateOrCreate(
                ['ren_type_id'                =>  $ren_type_id],
                ['Lifetime'                   =>  $Lifetime,
                'lifetimeValue'              =>  $lifetimeValue],
            );


            //lifetime end here

          //connected instituate start here



          $connected_institute_university = $request->get('connected_institute_university');
          $connected_institute_research = $request->get('connected_institute_research');
          $connected_institute_govt = $request->get('connected_institute_govt');
          $connected_institute_school = $request->get('connected_institute_school');
          $connected_institute_other = $request->get('connected_institute_other');

          $data["connected_institute_university"]=$connected_institute_university;
          $data["connected_institute_research"]=$connected_institute_research;
          $data["connected_institute_govt"]=$connected_institute_govt;
          $data["connected_institute_school"]=$connected_institute_school;
          $data["connected_institute_other"]=$connected_institute_other;

          $count =0;
          if($connected_institute_university != Null && $connected_institute_university != 0){
            $count ++;
          } 
          if ($connected_institute_research != Null && $connected_institute_research != 0) {
              $count ++;
          }
          if ($connected_institute_govt != Null && $connected_institute_govt != 0) {
             $count ++;
          }
          if ($connected_institute_school != Null && $connected_institute_school != 0) {
              $count ++;
          }
          if ($connected_institute_other != Null && $connected_institute_other != 0) {
              $count ++;
          }



          $connected_institute_typescore = $count *2;


         //return $connected_institute_typescore;


          $connected_institute_coverage = $connected_institute_university + $connected_institute_research + $connected_institute_govt + $connected_institute_school + $connected_institute_other;



          $connected_institute_coverage_score =0;
          if ($connected_institute_coverage < 50) {
              $connected_institute_coverage_score = 2;
          }
          if ($connected_institute_coverage >= 50 && $connected_institute_coverage < 100) {
              $connected_institute_coverage_score = 4;
          }
          if ($connected_institute_coverage >= 100 && $connected_institute_coverage < 500) {
              $connected_institute_coverage_score = 6;
          }
          if ($connected_institute_coverage >= 500 && $connected_institute_coverage < 1000) {
              $connected_institute_coverage_score = 8;
          }
          if ($connected_institute_coverage >= 1000) {
              $connected_institute_coverage_score = 10;
          }
          

            $connected_institute_data = connected_institution::updateOrCreate(
                ['ren_type_id'                            =>  $ren_type_id],
                ['connected_institute_university'         =>  $connected_institute_university,
                'connected_institute_research'           =>  $connected_institute_research,
                'connected_institute_govt'               =>  $connected_institute_govt,
                'connected_institute_school'             =>  $connected_institute_school,
                'connected_institute_other'              =>  $connected_institute_other,
                'total_institute_type'                   =>  $count,
                'connected_institute_typescore'          =>  $connected_institute_typescore,
                'connected_institute_coverage'           =>  $connected_institute_coverage,
                'connected_institute_coverage_score'     =>  $connected_institute_coverage_score],

            );

            //connected institution end here


            //maximum bandwidth start here

          $Maximum_BW_U = $request->get('Maximum_BW_U');
          $data["Maximum_BW_U"]=$Maximum_BW_U;

          $Maximum_BW_U_score = 0;

          if ($Maximum_BW_U =='None' ||  $Maximum_BW_U =='< 100 Mbps') {
              $Maximum_BW_U_score = 2;
          }
          if ($Maximum_BW_U =='100 Mbps') {
              $Maximum_BW_U_score = 4;
          }
          if ($Maximum_BW_U =='1 Gbps') {
              $Maximum_BW_U_score = 6;
          }
          if ($Maximum_BW_U =='10 Gbps') {
              $Maximum_BW_U_score = 8;
          }
          if ($Maximum_BW_U =='100 Gbps') {
              $Maximum_BW_U_score = 10;
          }

          $Maximum_BW_RI = $request->get('Maximum_BW_RI');
          $data["Maximum_BW_RI"]=$Maximum_BW_RI;
          $Maximum_BW_RI_score = 0;

          if ($Maximum_BW_RI =='None' ) {
              $Maximum_BW_RI_score = 0;
          }
          if ($Maximum_BW_RI =='< 100 Mbps') {
              $Maximum_BW_RI_score = 2;
          }
          if ($Maximum_BW_RI =='100 Mbps') {
              $Maximum_BW_RI_score = 4;
          }
          if ($Maximum_BW_RI =='1 Gbps') {
              $Maximum_BW_RI_score = 6;
          }
          if ($Maximum_BW_RI =='10 Gbps') {
              $Maximum_BW_RI_score = 8;
          }
          if ($Maximum_BW_RI =='100 Gbps') {
              $Maximum_BW_RI_score = 10;
          }


        $maximum_bw_data = maximum_bw::updateOrCreate(
                ['ren_type_id'                 =>  $ren_type_id],
                ['Maximum_BW_U'                =>  $Maximum_BW_U,
                'Maximum_BW_U_score'          =>  $Maximum_BW_U_score,
                'Maximum_BW_RI'               =>  $Maximum_BW_RI,
                'Maximum_BW_RI_score'         =>  $Maximum_BW_RI_score],
            );

               //maimum bandwidth end here


               //aggregate upstram BW start here
          $Commodity = $request->get('Commodity');
          $Res_Edu = $request->get('Res_Edu');

          $data["Commodity"]=$Commodity;
          $data["Res_Edu"]=$Res_Edu;


           $Commodity_score =0;
          if ($Commodity == 0) {
              $Commodity_score = 0;
          }
          if ($Commodity > 0 && $Commodity <= 100) {
              $Commodity_score = 1;
          }
          if ($Commodity > 100 && $Commodity <= 1000) {
              $Commodity_score = 3;
          }
          if ($Commodity > 1000 && $Commodity <= 10000) {
              $Commodity_score = 7;
          }
          if ($Commodity > 10000) {
              $Commodity_score = 10;
          }


          $Res_Edu_score =0;
          if ($Res_Edu == 0) {
              $Res_Edu_score = 0;
          }
          if ($Res_Edu > 0 && $Res_Edu <= 50) {
              $Res_Edu_score = 1;
          }
          if ($Res_Edu > 50 && $Res_Edu <= 500) {
              $Res_Edu_score = 3;
          }
          if ($Res_Edu > 500 && $Res_Edu <= 1000) {
              $Res_Edu_score = 7;
          }
          if ($Res_Edu > 1000) {
              $Res_Edu_score = 10;
          }

          $aggregate_up_bw_data = aggregate_up_bw::updateOrCreate(
                ['ren_type_id'              =>  $ren_type_id],
                ['Commodity'                =>  $Commodity,
                'Commodity_score'          =>  $Commodity_score,
                'Res_Edu'                  =>  $Res_Edu,
                'Res_Edu_score'            =>  $Res_Edu_score],
            );         

               //aggreagte upstream end here


               //depth of ownership start here

          $RandS         = $request->get('RandS');
          $Trans_Network = $request->get('Trans_Network');
          $CandV         = $request->get('CandV');
          $VideoColla    = $request->get('VideoColla');
          $BackboneFibre = $request->get('BackboneFibre');
          $LMF           = $request->get('LMF');
          $CivilPI       = $request->get('CivilPI');


          $data["RandS"]=$RandS;
          $data["Trans_Network"]=$Trans_Network;
          $data["CandV"]=$CandV;
          $data["VideoColla"]=$VideoColla;
          $data["BackboneFibre"]=$BackboneFibre;
          $data["LMF"]=$LMF;
          $data["CivilPI"]=$CivilPI;

        

           $RandS_score =0;
          if ($RandS == 'Fully Owned') {
              $RandS_score = 4;
          }
          if ($RandS == 'Long Term Leased') {
              $RandS_score = 3;
          }
          if ($RandS == 'Hybrid') {
              $RandS_score = 2;
          }
          if ($RandS == 'Rented') {
              $RandS_score = 1;
          }
          if ($RandS == 'Not Applicable') {
              $RandS_score = 0;
          }

          $Trans_Network_score =0;
          if ($Trans_Network == 'Fully Owned') {
              $Trans_Network_score = 4;
          }
          if ($Trans_Network == 'Long Term Leased') {
              $Trans_Network_score = 3;
          }
          if ($Trans_Network == 'Hybrid') {
              $Trans_Network_score = 2;
          }
          if ($Trans_Network == 'Rented') {
              $Trans_Network_score = 1;
          }
          if ($Trans_Network == 'Not Applicable') {
              $Trans_Network_score = 0;
          }

          $CandV_score =0;
          if ($CandV == 'Fully Owned') {
              $CandV_score = 4;
          }
          if ($CandV == 'Long Term Leased') {
              $CandV_score = 3;
          }
          if ($CandV == 'Hybrid') {
              $CandV_score = 2;
          }
          if ($CandV == 'Rented') {
              $CandV_score = 1;
          }
          if ($CandV == 'Not Applicable') {
              $CandV_score = 0;
          }

          $VideoColla_score =0;
          if ($VideoColla == 'Fully Owned') {
              $VideoColla_score = 4;
          }
          if ($VideoColla == 'Long Term Leased') {
              $VideoColla_score = 3;
          }
          if ($VideoColla == 'Hybrid') {
              $VideoColla_score = 2;
          }
          if ($VideoColla == 'Rented') {
              $VideoColla_score = 1;
          }
          if ($VideoColla == 'Not Applicable') {
              $VideoColla_score = 0;
          }


          $BackboneFibre_score =0;
          if ($BackboneFibre == 'Fully Owned') {
              $BackboneFibre_score = 4;
          }
          if ($BackboneFibre == 'Long Term Leased') {
              $BackboneFibre_score = 3;
          }
          if ($BackboneFibre == 'Hybrid') {
              $BackboneFibre_score = 2;
          }
          if ($BackboneFibre == 'Rented') {
              $BackboneFibre_score = 1;
          }
          if ($BackboneFibre == 'Not Applicable') {
              $BackboneFibre_score = 0;
          }

          $LMF_score =0;
          if ($LMF == 'Fully Owned') {
              $LMF_score = 4;
          }
          if ($LMF == 'Long Term Leased') {
              $LMF_score = 3;
          }
          if ($LMF == 'Hybrid') {
              $LMF_score = 2;
          }
          if ($LMF == 'Rented') {
              $LMF_score = 1;
          }
          if ($LMF == 'Not Applicable') {
              $LMF_score = 0;
          }

          $CivilPI_score =0;
          if ($CivilPI == 'Fully Owned') {
              $CivilPI_score = 4;
          }
          if ($CivilPI == 'Long Term Leased') {
              $CivilPI_score = 3;
          }
          if ($CivilPI == 'Hybrid') {
              $CivilPI_score = 2;
          }
          if ($CivilPI == 'Rented') {
              $CivilPI_score = 1;
          }
          if ($CivilPI == 'Not Applicable') {
              $CivilPI_score = 0;
          }



          $ownership_score = ((($RandS_score * 0.7) + ($Trans_Network_score * 0.7) + ($CandV_score * 0.7) + ($VideoColla_score * 0.5) + ($BackboneFibre_score * 1) + ($LMF_score * 0.8) + ($CivilPI_score * 0.4)) / (10 * 19.2))*100; 



            $ownership_data = ownership::updateOrCreate(
                ['ren_type_id'                 =>  $ren_type_id],
                ['RandS'                       =>  $RandS,
                'Trans_Network'               =>  $Trans_Network,
                'CandV'                       =>  $CandV,
                'VideoColla'                  =>  $VideoColla,
                'BackboneFibre'               =>  $BackboneFibre,
                'LMF'                         =>  $LMF,
                'CivilPI'                     =>  $CivilPI,
                'ownership_score'             =>  round($ownership_score,2)],
            );

               //depth of ownership end here

               //human resources start from here

              $SystemOperationRandS = $request->get('SystemOperationRandS');
              $SystemOperationTN = $request->get('SystemOperationTN');
              $SystemOperationCV = $request->get('SystemOperationCV');
              $SystemOperationCollaboration = $request->get('SystemOperationCollaboration');
              $SystemOperationAS = $request->get('SystemOperationAS');
              $SystemOperationOther = $request->get('SystemOperationOther');
              $human_resouces_technical = $request->get('human_resouces_technical');
              $human_resouces_Nontechnical = $request->get('human_resouces_Nontechnical');

              $data["SystemOperationRandS"]=$SystemOperationRandS;
              $data["SystemOperationTN"]=$SystemOperationTN;
              $data["SystemOperationCV"]=$SystemOperationCV;
              $data["SystemOperationCollaboration"]=$SystemOperationCollaboration;
              $data["SystemOperationAS"]=$SystemOperationAS;
              $data["SystemOperationOther"]=$SystemOperationOther;
              $data["human_resouces_technical"]=$human_resouces_technical;
              $data["human_resouces_Nontechnical"]=$human_resouces_Nontechnical;


              $Fully_self=0;
              if ($SystemOperationRandS =='Fully Self') {
                $Fully_self= $Fully_self + 1;
              }
              if ($SystemOperationTN =='Fully Self') {
                $Fully_self= $Fully_self + 1;
              }
              if ($SystemOperationCV =='Fully Self') {
                $Fully_self= $Fully_self + 1;
              }
              if ($SystemOperationCollaboration =='Fully Self') {
                $Fully_self= $Fully_self + 1;
              }
              if ($SystemOperationAS =='Fully Self') {
                $Fully_self= $Fully_self + 1;
              }
              if ($SystemOperationOther =='Fully Self') {
                $Fully_self= $Fully_self + 1;
              }


              $Fully_Outsourced=0;
              if ($SystemOperationRandS =='Fully Outsourced') {
                $Fully_Outsourced= $Fully_Outsourced + 1;
              }
              if ($SystemOperationTN =='Fully Outsourced') {
                $Fully_Outsourced= $Fully_Outsourced + 1;
              }
              if ($SystemOperationCV =='Fully Outsourced') {
                $Fully_Outsourced= $Fully_Outsourced + 1;
              }
              if ($SystemOperationCollaboration =='Fully Outsourced') {
                $Fully_Outsourced= $Fully_Outsourced + 1;
              }
              if ($SystemOperationAS =='Fully Outsourced') {
                $Fully_Outsourced= $Fully_Outsourced + 1;
              }
              if ($SystemOperationOther =='Fully Outsourced') {
                $Fully_Outsourced= $Fully_Outsourced + 1;
              }


              $Hybrid=0;
              if ($SystemOperationRandS =='Hybrid') {
                $Hybrid= $Hybrid + 1;
              }
              if ($SystemOperationTN =='Hybrid') {
                $Hybrid= $Hybrid + 1;
              }
              if ($SystemOperationCV =='Hybrid') {
                $Hybrid= $Hybrid + 1;
              }
              if ($SystemOperationCollaboration =='Hybrid') {
                $Hybrid= $Hybrid + 1;
              }
              if ($SystemOperationAS =='Hybrid') {
                $Hybrid= $Hybrid + 1;
              }
              if ($SystemOperationOther =='Hybrid') {
                $Hybrid= $Hybrid + 1;
              }


              $Not_Applicable=0;
              if ($SystemOperationRandS =='Not Applicable') {
                $Not_Applicable= $Not_Applicable + 1;
              }
              if ($SystemOperationTN =='Not Applicable') {
                $Not_Applicable= $Not_Applicable + 1;
              }
              if ($SystemOperationCV =='Not Applicable') {
                $Not_Applicable= $Not_Applicable + 1;
              }
              if ($SystemOperationCollaboration =='Not Applicable') {
                $Not_Applicable= $Not_Applicable + 1;
              }
              if ($SystemOperationAS =='Not Applicable') {
                $Not_Applicable= $Not_Applicable + 1;
              }
              if ($SystemOperationOther =='Not Applicable') {
                $Not_Applicable= $Not_Applicable + 1;
              }


              $testing_array = compact('Fully_self', 'Fully_Outsourced', 'Hybrid');
              arsort($testing_array);
              $maximum_value = key($testing_array);

              //return $maximum_value;


              $dominent_type_operation =0;
              if (${$maximum_value} == $Fully_self) {
                $dominent_type_operation ='Dominantly-Self';
              }
              if (${$maximum_value} == $Hybrid) {
                $dominent_type_operation ='Dominantly-Hybrid';
              }
              if (${$maximum_value} == $Fully_Outsourced) {
                $dominent_type_operation ='Dominantly-Hybrid';
              }

        

          $human_resource_total_connected_institution = $connected_institute_university + $connected_institute_research + $connected_institute_govt + $connected_institute_school + $connected_institute_other ;


            ///for nontechnical
             if ($human_resouces_Nontechnical == 0) {
                $humanresouce_connected_nontechnical_institution = $human_resource_total_connected_institution;
             }else{
              $humanresouce_connected_nontechnical_institution = $human_resource_total_connected_institution / $human_resouces_Nontechnical;
             }

              
              $human_resource_status_nontechnical =0;
              if ($dominent_type_operation =='Dominantly-Self') {

                
                if ($humanresouce_connected_nontechnical_institution == null) {
                  $human_resource_status_nontechnical ='not-Adequately Staffed';
                }
                if ($humanresouce_connected_nontechnical_institution < 5) {
                  $human_resource_status_nontechnical ='Over-stuffed';
                }
                if ($humanresouce_connected_nontechnical_institution >= 5 && $humanresouce_connected_nontechnical_institution < 15) {
                  $human_resource_status_nontechnical ='Adequately Staffed';
                }
                if ($humanresouce_connected_nontechnical_institution >= 15) {
                  $human_resource_status_nontechnical ='not-Adequately Staffed';
                }
                
              }else{

                 if ($humanresouce_connected_nontechnical_institution == null) {
                  $human_resource_status_nontechnical ='not-Adequately Staffed';
                }
                if ($humanresouce_connected_nontechnical_institution < 5) {
                  $human_resource_status_nontechnical ='Over-stuffed';
                }
                if ($humanresouce_connected_nontechnical_institution >= 5 && $humanresouce_connected_nontechnical_institution < 15) {
                  $human_resource_status_nontechnical ='Adequately Staffed';
                }
                if ($humanresouce_connected_nontechnical_institution >= 15) {
                  $human_resource_status_nontechnical ='not-Adequately Staffed';
                }

              }


              $human_resouce_nontechnical_final_score = 0;
              if ($human_resource_status_nontechnical =='not-Adequately Staffed') {
               $human_resouce_nontechnical_final_score = 5;
              }
              if ($human_resource_status_nontechnical =='Adequately Staffed') {
               $human_resouce_nontechnical_final_score = 10;
              }

             if ($human_resource_status_nontechnical =='Over-stuffed') {
             $human_resouce_nontechnical_final_score = 7;
            }


             //for technical
            $humanresouce_connected_technical_institution = $human_resource_total_connected_institution / $human_resouces_technical;

            $human_resource_status_technical =0;
              if ($dominent_type_operation =='Dominantly-Self') {

                
                if ($humanresouce_connected_technical_institution <2) {
                  $human_resource_status_technical ='Over-stuffed';
                }
                if ($humanresouce_connected_technical_institution >= 2 && $humanresouce_connected_technical_institution < 10) {
                  $human_resource_status_technical ='Adequately Staffed';
                }
                if ($humanresouce_connected_technical_institution >= 10) {
                  $human_resource_status_technical ='not-Adequately Staffed';
                }
                
              }else{

                if ($humanresouce_connected_technical_institution < 4) {
                  $human_resource_status_technical ='Over-stuffed';
                }
                if ($humanresouce_connected_technical_institution >= 4 && $humanresouce_connected_technical_institution < 20) {
                  $human_resource_status_technical ='Adequately Staffed';
                }
                if ($humanresouce_connected_technical_institution >= 20) {
                  $human_resource_status_technical ='not-Adequately Staffed';
                }

              }


              $human_resouce_technical_final_score = 0;
              if ($human_resource_status_technical =='not-Adequately Staffed') {
               $human_resouce_technical_final_score = 5;
              }
              if ($human_resource_status_technical =='Adequately Staffed') {
               $human_resouce_technical_final_score = 10;
              }

             if ($human_resource_status_technical =='Over-stuffed') {
             $human_resouce_technical_final_score = 7;
            }




            $human_resouces_data_table = human_resouces::updateOrCreate(
                ['ren_type_id'                         =>  $ren_type_id],
                ['SystemOperationRandS'                =>  $SystemOperationRandS,
                'SystemOperationTN'                    =>  $SystemOperationTN,
                'SystemOperationCV'                    =>  $SystemOperationCV,
                'SystemOperationCollaboration'         =>  $SystemOperationCollaboration,
                'SystemOperationAS'                    =>  $SystemOperationAS,
                'SystemOperationOther'                 =>  $SystemOperationOther,
                'human_resouces_technical'             =>  $human_resouces_technical,
                'human_resouce_technical_final_score'  =>  $human_resouce_technical_final_score,
                'human_resouces_Nontechnical'          =>  $human_resouces_Nontechnical,
                'human_resouce_nontechnical_final_score'  =>  $human_resouce_nontechnical_final_score],
            );

          //human resources end  from here




      $maturity_level = ((($score * 1.5) + ($lifetimeValue * 1.0) + ( $connected_institute_typescore * 1.0) + ($connected_institute_coverage_score * 1.0) + ($Maximum_BW_U_score * 2.0) + ($Maximum_BW_RI_score* 2.0) + ($Commodity_score * 2.0) + ($Res_Edu_score * 2.0) + ($ownership_score * 1.5) + ($offered_service_final_score * 3.0)+($financitial_stablity_grading * 2.0)+($human_resouce_technical_final_score * 1.5) + ($human_resouce_nontechnical_final_score * 1.0) + ($omg_aggregate_score * 1) + ($PV_aggregate_score * 1.5) + ($aggregate_collaborate_score * 1.0) + ($aggreagte_tech_score * 1.5) + ($aggreagte_admin_score * 1.0))/270)*100;


              $Critical_Deviation = $request->get('Critical_Deviation');
              $data["Critical_Deviation"]=$Critical_Deviation;

                    if($Critical_Deviation === null){
                        $Critical_Deviation = array('Torikul');
                      }

                    $CR_Score = 0;
                    foreach ($Critical_Deviation as $cd) {
                      
                      if ($cd =='Non-connectivity with Research Institute/Universities') {
                        $CR_Score = $CR_Score + 5;
                      }
                       if ($cd =='Non-connectivity with Educational Institute') {
                        $CR_Score = $CR_Score + 3;
                      }
                       if ($cd =='Non-connectivity with Global Research Network') {
                        $CR_Score = $CR_Score + 3;
                      }
                       if ($cd =='Non-ownership of R&S Device') {
                        $CR_Score = $CR_Score + 2;
                      }
                      if ($cd =='No Internal Network Connectivity') {
                        $CR_Score = $CR_Score + 2;
                      }
                    
                  }
              
                  //demerites minus korte hobe
                 $maturity_level = $maturity_level - $CR_Score;

                 $placement_tier = 0;
                 if ($maturity_level >= 80) {
                   $placement_tier = 'Tier-1';
                 }
                 if ($maturity_level >= 70 && $maturity_level <80 ) {
                   $placement_tier = 'Tier-2';
                 }
                 if ($maturity_level >= 55 && $maturity_level <70) {
                   $placement_tier = 'Tier-3';
                 }
                 if ($maturity_level >= 40 && $maturity_level <55 ) {
                   $placement_tier = 'Tier-4';
                 }
                 if ($maturity_level < 40) {
                   $placement_tier = 'Tier-5';
                 }
                 

              $common_data = common::updateOrCreate(
                ['ren_type_id'                          =>  $ren_type_id],
                ['coverage_id'                          =>  $C_table_data->id,
                'lifetime_id'                           =>  $L_table_data->id,
                'connected_institution_id'              =>  $connected_institute_data->id,
                'maximumBW_id'                          =>  $maximum_bw_data->id,
                'aggregate_UPBW_id'                     =>  $aggregate_up_bw_data->id,
                'ownership_id'                          =>  $ownership_data->id,
                'omg_id'                                =>  $omg_data_table->id,
                'PV_id'                                 =>  $PromotionVisiblity_table->id,
                'collaboration_id'                      =>  $collaboration_table->id,
                'FS_id'                                 =>  $financitial_stablity_table->id,
                'HR_id'                                 =>  $human_resouces_data_table->id,
                'HE_id'                                 =>  $human_expert_all_table->id,
                'SO_id'                                 =>  $offered_service_all_table->id,
                'Demerits'                              =>  json_encode($Critical_Deviation),
                'maturity_level'                        =>  round($maturity_level,2),
                'placement_tier'                        =>  $placement_tier],
            );

                 $data["score"] = $score;
                 $data["lifetimeValue"] = $lifetimeValue;
                 $data["connected_institute_typescore"] = $connected_institute_typescore;
                 $data["connected_institute_coverage_score"] = $connected_institute_coverage_score;
                 $data["Maximum_BW_U_score"] = $Maximum_BW_U_score;
                 $data["Maximum_BW_RI_score"] = $Maximum_BW_RI_score;
                 $data["Commodity_score"] = $Commodity_score;
                 $data["Res_Edu_score"] = $Res_Edu_score;
                 $data["ownership_score"] = $ownership_score;
                 $data["offered_service_final_score"] = $offered_service_final_score;
                 $data["financitial_stablity_grading"] = $financitial_stablity_grading;
                 $data["human_resouce_technical_final_score"] = $human_resouce_technical_final_score;
                 $data["human_resouce_nontechnical_final_score"] = $human_resouce_nontechnical_final_score;
                 $data["omg_aggregate_score"] = $omg_aggregate_score;
                 $data["PV_aggregate_score"] = $PV_aggregate_score;
                 $data["aggregate_collaborate_score"] = $aggregate_collaborate_score;
                 $data["aggreagte_tech_score"] = $aggreagte_tech_score;
                 $data["aggreagte_admin_score"] = $aggreagte_admin_score;
                 $data["maturity_level"] = $maturity_level;
                 $data["placement_tier"] = $placement_tier;
                 $data["Organization"] = Auth::user()->name;

                 $alterSendScoreUser = $request->get('alterSendScoreUser');
                 if ($alterSendScoreUser =="YES") {
                    $email = $request->get('email');
                    $data["email"] = $email;

                    $pdf = PDF::loadView('email.test', $data);
                    $pdf2 = PDF::loadView('email.variable', $data);

                    Mail::send('email.sendGuestScore', $data, function($message)use($data, $pdf,$pdf2) {
                        $message->to($data["email"], $data["email"])
                                ->subject('NREN Maturity Score Document from NREN')
                                ->attachData($pdf->output(), "MaturityScore.pdf")
                                ->attachData($pdf2->output(), "SubmitForm.pdf");
                    });

                 }          
         return redirect()->route('maturity');

    }catch (\Exception $exception) {
            //dd($exception->getMessage());
            return back()->withError('Some Error Occured!  '.$exception->getMessage());
            
        }
    }
}
