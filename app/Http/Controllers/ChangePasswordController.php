<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class ChangePasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(Auth::user()->role=="admin"){
            $string="Admin.layouts.app";
        }else if(Auth::user()->role=="user"){
            $string="User.layouts.app";
        }
        $data["layout_string"]=$string;
        return view('changePassword',$data);
    }
    
    public function store(Request $request)
    {
        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);
        User::find(auth()->user()->id)->update(['password' => Hash::make($request->new_password)]);
        Auth::logout();
        $request->session()->flash('success_message', 'Password Changed Successfully. Login with new password');
        return view('auth.login');
    }
}
