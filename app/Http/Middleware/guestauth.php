<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\Auth;

class guestauth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && (Auth::user()->role == "guest")  && (Auth::user()->isVerified == true)) {
            return $next($request);
            }
        request()->session()->flash('error_message', 'Only Guest is authorized to access this');
        return redirect('/access-denied');
    }
}
