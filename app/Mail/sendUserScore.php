<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class sendUserScore extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $registred_email;
    public function __construct($maturity_level,$placement_tier,$getORGname)
    {
        $this->maturity_level=$maturity_level;
        $this->placement_tier=$placement_tier;
        $this->getORGname=$getORGname;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $app_name = env("APP_NAME");
        $data['app_url']=env("APP_URL");
        $data['maturity_level']=$this->maturity_level;
        $data['placement_tier']=$this->placement_tier;
        $data['getORGname']=$this->getORGname;
        return $this->subject('NREN Maturity Score from '.$app_name)
                ->view('email.sendUserScore', $data);
    }
}
