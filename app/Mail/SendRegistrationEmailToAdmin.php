<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendRegistrationEmailToAdmin extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $registred_email;
    public function __construct($email)
    {
        $this->registred_email=$email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $app_name = env("APP_NAME");
        $data['app_url']=env("APP_URL");
        $data['registred_email']=$this->registred_email;
        return $this->subject('New user registered on '.$app_name)
                ->view('email.RegistrationEmailToAdmin', $data);
    }
}
