<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class common extends Model
{
    use HasFactory;

    protected $guarded = [];


/*    public function getRouteKeyName(){
        return 'ren_unique_id';
    }*/


      public function getRENDATA()
    {
        return $this->belongsTo(User::class,'ren_type_id');
    }

    public function getCoverageScore()
    {
        return $this->belongsTo(coverage::class,'coverage_id');
    }

    public function getLifetimevalue()
    {
        return $this->belongsTo(lifetime::class,'lifetime_id');
    }

    public function getConnectedInstitutionlue()
    {
        return $this->belongsTo(connected_institution::class,'connected_institution_id');
    }

        public function getMaximumBW()
    {
        return $this->belongsTo(maximum_bw::class,'maximumBW_id');
    }

         public function getAggregateBW()
    {
        return $this->belongsTo(aggregate_up_bw::class,'aggregate_UPBW_id');
    }

          public function getOwwnerShipData()
    {
        return $this->belongsTo(ownership::class,'ownership_id');
    }

     public function getOMGData()
    {
        return $this->belongsTo(omg::class,'omg_id');
    }

     public function getPVData()
    {
        return $this->belongsTo(promotionandvisiblity::class,'PV_id');
    }

     public function getcollaborationData()
    {
        return $this->belongsTo(collaboration::class,'collaboration_id');
    }
     public function getFSData()
    {
        return $this->belongsTo(financitial_stablity::class,'FS_id');
    }
    public function getHRData()
    {
        return $this->belongsTo(human_resouces::class,'HR_id');
    }

    public function getHEData()
    {
        return $this->belongsTo(human_expert::class,'HE_id');
    }

     public function getServiceOfferData()
    {
        return $this->belongsTo(offer_service::class,'SO_id');
    }
}
