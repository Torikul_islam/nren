<?php

namespace App\Exports;

use App\Models\common;
use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use DB;
use Carbon\Carbon;

class dataExport implements FromCollection, WithHeadings, WithStyles
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        //return common::all();

        return $data = common::select(
            "users.name",
            "coverages.coverage_total_university",
            "coverages.coverage_connected_university",
            "lifetimes.Lifetime",
            "connected_institutions.connected_institute_university",
            "connected_institutions.connected_institute_research",
            "connected_institutions.connected_institute_govt",
            "connected_institutions.connected_institute_school",
            "connected_institutions.connected_institute_other",
            "maximum_bws.Maximum_BW_U",
            "maximum_bws.Maximum_BW_RI",
            "aggregate_up_bws.Commodity",
            "aggregate_up_bws.Res_Edu",
            "ownerships.RandS",
            "ownerships.Trans_Network",
            "ownerships.CandV",
            "ownerships.VideoColla",
            "ownerships.BackboneFibre",
            "ownerships.LMF",
            "ownerships.CivilPI",
            "omgs.policies",
            "omgs.Organogram",
            "omgs.Policy_Makers",
            "omgs.Recruitment_Regulations",
            "omgs.Promotion_Policy",
            "omgs.Annual_Appraisal",
            "omgs.Gratuity",
            "omgs.Provident_Fund",
            "omgs.Welfare_Fund",
            "omgs.Group_Insurance",
            "omgs.Pension_Benefits",
            "omgs.Permanent",
            "omgs.Contractual",
            "omgs.Outsourced",
            "omgs.Turnover_Rate",
            "omgs.Filled_Position",
            "promotionandvisiblities.PV_effort_REN",
            "promotionandvisiblities.Awareness_programs",
            "promotionandvisiblities.budget_promotional",
            "promotionandvisiblities.Awareness_Programs_conducted",
            "promotionandvisiblities.Development_personnel",
            "collaborations.collaboration_NREN",
            "collaborations.collaboration_UR",
            "collaborations.collaboration_vendor",
            "collaborations.collaboration_policy_RM",
            "collaborations.collaboration_policy_HEC",
            "human_resouces.SystemOperationRandS",
            "human_resouces.SystemOperationTN",
            "human_resouces.SystemOperationCV",
            "human_resouces.SystemOperationCollaboration",
            "human_resouces.SystemOperationAS",
            "human_resouces.SystemOperationOther",
            "human_resouces.human_resouces_technical",
            "human_resouces.human_resouces_Nontechnical",
            "financitial_stablities.financial_type",
            "financitial_stablities.FS_Revenue_Coverage",
            "financitial_stablities.FS_compliance_lts",
            "financitial_stablities.financial_constraint",
            "financitial_stablities.finance_stablity_Lifetime",
            "financitial_stablities.LTC",
            "financitial_stablities.highly_struggling",
            "financitial_stablities.financitial_stablity_parameter",
            "human_experts.human_expert_admin",
            "human_experts.human_intermediate_admin",
            "human_experts.human_beginner_admin",
            "human_experts.human_expert_tech",
            "human_experts.human_intermediate_tech",
            "human_experts.human_beginner_tech",
            "offer_services.network_service_SO",
            "offer_services.identity_service_SO",
            "offer_services.security_service_SO",
            "offer_services.multimedia_service_SO",
            "offer_services.collaboration_service_SO",
            "offer_services.storage_service_SO",
            "offer_services.professional_service_SO"
        )
            ->leftJoin("aggregate_up_bws", "aggregate_up_bws.id", "=", "commons.aggregate_UPBW_id")
            ->leftJoin("collaborations", "collaborations.id", "=", "commons.collaboration_id")
            ->leftJoin("connected_institutions", "connected_institutions.id", "=", "commons.connected_institution_id")
            ->leftJoin("coverages", "coverages.id", "=", "commons.coverage_id")
            ->leftJoin("financitial_stablities", "financitial_stablities.id", "=", "commons.FS_id")
            ->leftJoin("human_experts", "human_experts.id", "=", "commons.HE_id")
            ->leftJoin("human_resouces", "human_resouces.id", "=", "commons.HR_id")
            ->leftJoin("lifetimes", "lifetimes.id", "=", "commons.lifetime_id")
            ->leftJoin("maximum_bws", "maximum_bws.id", "=", "commons.maximumBW_id")
            ->leftJoin("offer_services", "offer_services.id", "=", "commons.SO_id")
            ->leftJoin("omgs", "omgs.id", "=", "commons.omg_id")
            ->leftJoin("ownerships", "ownerships.id", "=", "commons.ownership_id")
            ->leftJoin("promotionandvisiblities", "promotionandvisiblities.id", "=", "commons.PV_id")
            ->leftJoin("users", "users.id", "=", "commons.ren_type_id")
            ->get();
    }
    public function headings(): array
    {
        return [
            "Name of the Organization",
            "How Many Universities Are There In Total In Your Country ?",
            "How Many Universities Were Connected To Your Network ?",
            "How Long Have You Been In Business ?",
            "How Many Institutions/Members Were Connected To Your Network??/Universities",
            "How Many Institutions/Members Were Connected To Your Network??/Research Institutes",
            "How Many Institutions/Members Were Connected To Your Network??/Government Organizations",
            "How Many Institutions/Members Were Connected To Your Network??/Schools And Colleges",
            "How Many Institutions/Members Were Connected To Your Network??/Others",
            "The Maximum Bandwith That Is Deliverable Among The Universities ?",
            "The Maximum Bandwith That Is Deliverable Among The Research Instituates ?",
            "Your Total Upstream Traffic Capacity, Mbps/Commodity",
            "Your Total Upstream Traffic Capacity, Mbps/Research And Education",
            "What Is The Ownership Model Of The Key Components Of Your Network?/Routing And Switching",
            "What Is The Ownership Model Of The Key Components Of Your Network?/Transmission Network",
            "What Is The Ownership Model Of The Key Components Of Your Network?/Compute And Virtualization",
            "What Is The Ownership Model Of The Key Components Of Your Network?/Video Collaboration",
            "What Is The Ownership Model Of The Key Components Of Your Network?/Backbone Fibre",
            "What Is The Ownership Model Of The Key Components Of Your Network?/Last Mile Fibre",
            "What Is The Ownership Model Of The Key Components Of Your Network?/Civil And Power Infrastructure At PoPs",
            "Please Select The Policies That Are Available In Your Organization?",
            "Please Mention The Availability Of The Following In Your Organization/Organogram",
            "Please Mention The Availability Of The Following In Your Organization/Policy Makers",
            "Please Mention The Availability Of The Following In Your Organization/Recruitment Regulations",
            "Please Mention The Availability Of The Following In Your Organization/Promotion Policy",
            "Please Mention The Availability Of The Following In Your Organization/Annual Appraisal",
            "Please Mention The Availability Of The Following In Your Organization/Gratuity",
            "Please Mention The Availability Of The Following In Your Organization/Provident Fund",
            "Please Mention The Availability Of The Following In Your Organization/Welfare Fund",
            "Please Mention The Availability Of The Following In Your Organization/Group Insurance",
            "Please Mention The Availability Of The Following In Your Organization/Pension Benefits",
            "How Many Employees Were Working In Your Organization On January 31, 2021 Under The Following Distinct Categories?/Permanent/Full Time",
            "How Many Employees Were Working In Your Organization On January 31, 2021 Under The Following Distinct Categories?/Contractual/ Part-Time",
            "How Many Employees Were Working In Your Organization On January 31, 2021 Under The Following Distinct Categories?/Outsourced/ Honorary",
            " What Is The Turnover Rate [In %] Of The Employees In Last 5 Years?",
            "How Many Positions Of Your Organogram Are Filled At The Moment [In %]??",
            "Do You Think That More Efforts Are Required By The Regulatory Bodies To Ensure Smooth Functioning Of The NREN?",
            "Awareness Programs Attended By Regulatory Bodies In Last 3 Years?",
            "What Is The Percent (%) Of Budget Allocated For Promotional Activities With Respect To Your Total Budget?",
            "How Many Awareness Programs [Seminar/Workshop/Training] Have Been Conducted In Last 3 Years?",
            "Promotion/Market Development Personnel Presence?",
            "Rate Your Collaboration With NRENs, Users, Vendors And Policy Makers/NRENs",
            "Rate Your Collaboration With NRENs, Users, Vendors And Policy Makers/Universities/Research Institutions",
            "Rate Your Collaboration With NRENs, Users, Vendors And Policy Makers/Vendors",
            "Rate Your Collaboration With NRENs, Users, Vendors And Policy Makers/Policy Makers",
            "Rate Your Collaboration With NRENs, Users, Vendors And Policy Makers/Policy Makers (Higher Education Commission)",
            "How Do You Perform Your System Operations?/Routing And Switching",
            "How Do You Perform Your System Operations?/Transmission Network",
            "How Do You Perform Your System Operations?/Compute And Virtualization",
            "How Do You Perform Your System Operations?/Video Collaboration",
            "How Do You Perform Your System Operations?/Application Services",
            "How Do You Perform Your System Operations?/Others",
            "How Many Employees Were Working In Your Organization On January 31, 2021 Under The Following Distinct Categories?/Technical",
            "How Many Employees Were Working In Your Organization On January 31, 2021 Under The Following Distinct Categories?/Non-Technical",
            "Type Of Financing For Your NREN??",
            "Revenue Coverage",
            "Compliance With LTS?",
            "Are There Any Financial Constraints Of Funding By The Government Or Regulatory Bodies And A Push To Be Financially Self-Sustainable?",
            "LTC: Long Term Commitment From Government ?",
            "Do You Consider Yourself As An NREN Who Is Highly Struggling For Financial Sustainablity ??",
            "Expertise Of NREN Employees In Percent (%) Engaged In Administrative And Management/Experienced",
            "Expertise Of NREN Employees In Percent (%) Engaged In Administrative And Management/Intermediate",
            "Expertise Of NREN Employees In Percent (%) Engaged In Administrative And Management/Beginners",
            "Aproximate % Level Of Expertise Among NREN Engineers/Expert ",
            "Aproximate % Level Of Expertise Among NREN Engineers/Intermediate",
            "Aproximate % Level Of Expertise Among NREN Engineers/Beginners",
            "Which Network Services Are You Currently Facilitating To REN Community ??",
            "Which Identity Services Have You Already Deployed ??",
            "Which Security Services Are You Offering ??",
            "Do You Cover Any Of The Following Multimedia Services To Your REN Community ??",
            "Which Collaboration Services Are You Currently Promoting ??",
            "Do You Offer The Following Storage And Hosting Services ??",
            "Do You Have Any Professional Services ??"
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }
}
